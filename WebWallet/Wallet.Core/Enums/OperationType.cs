﻿namespace Wallet.Core.Enums
{
    public enum OperationType
    {
        None,
        Transfer,
        Transaction,
    }
}
