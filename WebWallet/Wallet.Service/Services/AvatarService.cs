﻿using Avatarator;
using System.Net;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net.Mime;
using Wallet.Helpers.Еxceptions;
using Wallet.Core;
using Wallet.Core.Models;
using Wallet.Service.Interfaces;

namespace Wallet.Services
{
	public class AvatarService : IAvatarService
    {
		private readonly Cloudinary cloudinary;
		public AvatarService(Cloudinary cloudinary)
		{
			this.cloudinary = cloudinary;
		}		

        public string UploadImage(IFormFile imageFile)
		{
			string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images");
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}

			//get file extension
			String publicId = Guid.NewGuid().ToString();
			FileInfo fileInfo = new FileInfo(imageFile.FileName);
			string fileName = publicId + fileInfo.Extension;

			string fileNameWithPath = Path.Combine(path, fileName);
			using (var stream = new FileStream(fileNameWithPath, FileMode.Create))
			{
				imageFile.CopyTo(stream);
			}

			// Upload image to host service ...
			var uploadParams = new ImageUploadParams()
			{
				File = new FileDescription(fileNameWithPath),
				PublicId = publicId
			};
			var uploadResult = cloudinary.Upload(uploadParams);
			if (uploadResult.StatusCode == HttpStatusCode.OK)
			{
				FileInfo fileToDelete = new FileInfo(fileNameWithPath);
				fileToDelete.Delete();
				return Environment.GetEnvironmentVariable("CLOUDINARY_HOST_URL") + publicId;
			}

			FileInfo file = new FileInfo(fileNameWithPath);
			file.Delete();
			return null;
		}

        public DeletionResult DeleteImage(string OldAvatar)
		{
            var publicId = OldAvatar.Split(new[] { '/' }).Last();

            var deletionParams = new DeletionParams(publicId);
            var deletionResult = cloudinary.Destroy(deletionParams);

            return deletionResult;
        }

        // Generates a FormFile with the initials of the user.
        public static IFormFile GeneratePlaceholderAvatar(string firstName, string lastName)
		{
			var avatarString = string.Format("{0}{1}", firstName[0], lastName[0]).ToUpper();

			var config = new AvataratorConfig();
			var generator = new AbbreviationGenerator(config);
			var image = generator.Generate(avatarString, 150, 150);

			// Load RGB array into MemoryStream
			MemoryStream rgbMemoryStream = new MemoryStream(image);
			// Create bitmap object from rgbMemoryStream
			Bitmap bitmap = new Bitmap(rgbMemoryStream);
			// Convert rgbMemoryStream to pngMemoryStream
			MemoryStream pngMemoryStream = new MemoryStream();
			bitmap.Save(pngMemoryStream, ImageFormat.Png);
			// Convert pngMemoryStream to FormFile
			return CreateTemporaryFile(avatarString, "image/png", pngMemoryStream);
		}

		private static IFormFile CreateTemporaryFile(string fileName, string contentType, Stream stream)
		{
			var formFile = new FormFile(
				baseStream: stream,
				baseStreamOffset: 0,
				length: stream.Length,
				name: "Data",
				fileName: fileName)
			{
				Headers = new HeaderDictionary()
			};

			formFile.ContentType = contentType;
			return formFile;
		}

		public static void ValidateImage(IFormFile ImageFile)
		{
			if (ImageFile.ContentType != MediaTypeNames.Image.Jpeg
					&& ImageFile.ContentType != "image/png")
			{
				throw new InvalidImageException(ErrorMessages.ImageFormatErrorMessage);
			}

			if (ImageFile.Length > 2 * 1024 * 1024)
			{
				throw new InvalidImageException(ErrorMessages.ImageSizeErrorMessage);
			}
		}
	}
}
