﻿namespace Wallet.Core.Dtos.Currency
{
    public class CurrencyConversionResultDto
    {
        public string ReceivedCurrency { get; set; }

        public string SentCurrency { get; set; }

        public decimal ReceivedAmount { get; set; }
        public decimal SentAmount { get; set; }

        public decimal ExchangeRate => Math.Round(SentAmount / ReceivedAmount, 4);
    }
}
