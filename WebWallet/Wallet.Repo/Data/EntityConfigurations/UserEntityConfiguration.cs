﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Wallet.Core.Models;

namespace Wallet.Repo.Data.EntityConfigurations
{
    public class UserEntityConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasMany(u => u.BankCards)
            .WithOne(b => b.User)
            .HasForeignKey(b => b.UserId)
            .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(u => u.OutgoingTransactions)
            .WithOne(t => t.SenderUser)
            .HasForeignKey(t => t.SenderUserId)
            .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(u => u.IncomingTransactions)
            .WithOne(t => t.RecepientUser)
            .HasForeignKey(t => t.RecepientUserId)
            .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(u => u.Transfers)
            .WithOne(t => t.User)
            .HasForeignKey(t => t.UserId)
            .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(u => u.Accounts)
                .WithOne(a => a.User)
                .HasForeignKey(a=> a.UserId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
