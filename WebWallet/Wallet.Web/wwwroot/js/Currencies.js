﻿$(document).ready(function () {

    var currencyDropdown = $("#currencyDropdown");
    var selectedCurrencyField = $("#selectedCurrency");

    currencyDropdown.on("change", function () {
        var selectedCurrency = $(this).val();
        selectedCurrencyField.val(selectedCurrency);
        $(this).closest('form').submit();
    });

    function validateForm() {
        var isValid =
            validateName($('#Name').val()) &&
            validateCurrency($('#CurrencyId').val())

        $('#createButton').prop('disabled', !isValid);
    }

    $('#Name').on('input', function () {
        var Name = $(this).val();
        var isValid = validateName(Name);

        if (isValid) {
            $(this).removeClass('is-invalid').addClass('is-valid');
            $('#NameError').text('');
        } else {
            $(this).removeClass('is-valid').addClass('is-invalid');
            $('#NameError').text('The Name field must start with a capital letter and contain only alphabetical letters.');
        }

        validateForm();
    });

    function validateName(Name) {
        var namePattern = /^[A-Z][a-zA-Z\s]*$/;
        return namePattern.test(Name);
    }


    $('#CurrencyId').on('input', function () {
        var currencyId = $(this).val();
        var isValid = validateCurrency(currencyId);

        if (isValid) {
            $(this).removeClass('is-invalid').addClass('is-valid');
            $('#CurrencyIdError').text('');
        } else {
            $(this).removeClass('is-valid').addClass('is-invalid');
            $('#CurrencyIdError').text('Invalid Currency ID. The currency ID must be exactly 3 letters long and consist of capital letters A-Z only.');
        }

        validateForm();
    });

    function validateCurrency(currencyId) {
        var currencyIdPattern = /^[A-Z]{3}$/;
        return currencyIdPattern.test(currencyId);
    }
    
});