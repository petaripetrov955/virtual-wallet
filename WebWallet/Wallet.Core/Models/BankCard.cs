﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;
using Wallet.Core.Enums;

namespace Wallet.Core.Models
{
    public class BankCard
    {
        public const string InvalidPinErrorMessage = "Please enter a 4 to 6-digit numeric PIN for verification.";
        public const string PinPattern = @"^[0-9]{4,6}$";
        public const string InvalidCvvErrorMessage = "Please enter a 3 or 4-digit numeric CVV for card verification.";
        public const string CvvPattern = @"^[0-9]{3,4}$";
        public const string InvalidHolderNameErrorMessage = "Please enter a valid first and last name in the format 'First Last' in uppercase letters.";
        public const string HolderPattern = @"^[A-Z]+ [A-Z]+$";
        public const string InvalidCardNumberErrorMessage = "The length of the digits must be between 14 and 16.";
        public const string CardNumberPattern = @"^\d{14,16}$";
        public BankCard()
        {
            this.CreatedAt = DateTime.UtcNow;
            this.IsActive = true;
            this.Transfers = new List<Transfer>();
        }

        public int Id { get; set; }

        [Required]
        [RegularExpression(Account.Name_Regex_Pattern, ErrorMessage = ErrorMessages.InvalidAccountNameErrorMessage)]
        public string Name { get; set; }

        [Required]
        [RegularExpression(CardNumberPattern, ErrorMessage = InvalidCardNumberErrorMessage)]
        public string CardNumber { get; set; }
        public string CardNumberEnding { get; set; }

        [Required]
        public DateTime ValidThrough { get; set; }

        [Required]
        [RegularExpression(HolderPattern, ErrorMessage = InvalidHolderNameErrorMessage)]
        [MaxLength(28)]
        public string HolderName { get; set; }

        [Required]
        [RegularExpression(PinPattern, ErrorMessage = InvalidPinErrorMessage)]
        public string PIN { get; set; }

        [Required]
        [RegularExpression(CvvPattern, ErrorMessage = InvalidCvvErrorMessage)]
        public string CVV { get; set; }

        [Required]
        [EnumDataType(typeof(CardType))]
        public CardType CardType { get; set; }

        public DateTime CreatedAt { get; }
        public bool IsActive { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        [Required]
        [MaxLength(3)]
        public string CurrencyId { get; set; }
        public Currency Currency { get; set; }
        public ICollection<Transfer> Transfers { get; set; }
    }
}
