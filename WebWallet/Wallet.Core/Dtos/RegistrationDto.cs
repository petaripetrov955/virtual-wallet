﻿using System.ComponentModel.DataAnnotations;

namespace Wallet.Core.Dtos
{
	public class RegistrationDto
	{
		[MinLength(2, ErrorMessage = ErrorMessages.MinLengthErrorMessage)]
		[MaxLength(32, ErrorMessage = ErrorMessages.MaxLengthErrorMessage)]
		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string FirstName { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string LastName { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string Email { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string PhoneNumber { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string UserName { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string Password { get; set; }	
		public string? AvatarURL { get; set; }
	}
}
