﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Globalization;
using System.Net;
using System.Text.RegularExpressions;
using Wallet.Core;
using Wallet.Core.Dtos.Currency;
using Wallet.Core.Models;
using Wallet.Helpers.Еxceptions;
using Wallet.Repo.Interfaces;
using Wallet.Service.Interfaces;

namespace Wallet.Service.Services
{
    public class CurrencyService : ICurrencyService
    {
        private const string RequestsLimitReachedErrorMessage = "Too many requests. API limits reached.";
        private const string DuplicatedCurrencyErrorMessage = "Currency {0} '{1}' already exists in the system.";

        private const string ExternalApiBaseUrl = "https://api.currencybeacon.com/v1";

        private readonly IConfiguration _configuration;
        private readonly HttpClient _httpClient;
        private readonly ICurrencyRepository _currencyRepository;
        public CurrencyService(ICurrencyRepository currencyRepository, HttpClient httpClient, IConfiguration configuration)
        {
            _currencyRepository = currencyRepository;
            _httpClient = httpClient;
            _configuration = configuration;
        }

        public async Task<BaseCurrencyWithRatesDto> GetAllWithRatesAsync(string currencyBase)
        {
            await this.GetByIdAsync(currencyBase);
            ICollection<Currency> currencies = await GetAllAsync();

            string currenciesCodes = String.Join(",", currencies.Select(c => c.Id));
            string apiUrl = $"{ExternalApiBaseUrl}/latest?base={currencyBase}&symbols={currenciesCodes}&api_key={this.GetApiKey()}";

            BaseCurrencyWithRatesDto result = await GetResultFromExternalApiAsync<BaseCurrencyWithRatesDto>(apiUrl);
            result.Rates = result.Rates.ToDictionary(x => x.Key, x => Math.Round(x.Value, 4));

            return result;
        }

        public async Task<ICollection<Currency>> GetAllAsync()
        {
            ICollection<Currency> currencies = await this._currencyRepository.GetAllAsync();

            if (currencies.Count == 0)
            {
                throw new InvalidOperationException(String.Format(ErrorMessages.EmptyCollectionErrorMessage,nameof(Currency)));
            }

            return currencies;
        }

        public async Task<BaseCurrencyWithRatesDto> GetByIdWithRate(string baseCurrency, string currencyWithRate)
        {
            await this.GetByIdAsync(baseCurrency);
            await this.GetByIdAsync(currencyWithRate);

            string apiUrl = $"{ExternalApiBaseUrl}/latest?base={baseCurrency}&symbols={currencyWithRate}&api_key={this.GetApiKey()}";

            return await GetResultFromExternalApiAsync<BaseCurrencyWithRatesDto>(apiUrl);
        }

        public async Task<Currency> GetByIdAsync(string currencyId)
        {
            ValidateCurrencyId(currencyId);
            Currency currency = await this._currencyRepository.GetByIdAsync(currencyId);
            CheckIfCurrencyIsNull(currency);

            return currency;
        }

        public async Task<Currency> GetByNameAsync(string currencyName)
        {
            ValidateCurrencyName(currencyName);
            Currency currency = await this._currencyRepository.GetByNameAsync(currencyName);
            CheckIfCurrencyIsNull(currency);

            return currency;
        }
        public async Task<ExternalCurrencyConversionResponseDto> ConvertAsync(string receivingCurrency, string sendingCurrency, decimal AmountInReceivingCurrency)
        {
            await GetByIdAsync(receivingCurrency);
            await GetByIdAsync(sendingCurrency);

            string amount = AmountInReceivingCurrency.ToString(CultureInfo.CreateSpecificCulture("en-GB"));
            var apiUrl = $"{ExternalApiBaseUrl}/convert?from={receivingCurrency}&to={sendingCurrency}&amount={amount}&api_key={this.GetApiKey()}";

            return await GetResultFromExternalApiAsync<ExternalCurrencyConversionResponseDto>(apiUrl);
        }

        public async Task<Currency> CreateAsync(string currencyId, string name)
        {
            ValidateCurrencyName(name);
            ValidateCurrencyId(currencyId);
            Currency currency = await this._currencyRepository.GetByIdAsync(currencyId);
            if (currency != null)
            {
                throw new DuplicateEntityException(String.Format(DuplicatedCurrencyErrorMessage, "id",currencyId));
            }

            currency = await this._currencyRepository.GetByNameAsync(name);

            if (currency != null)
            {
                throw new DuplicateEntityException(String.Format(DuplicatedCurrencyErrorMessage, "name",name));
            }

            return await this._currencyRepository.CreateAsync(currencyId, name);
        }

        private void ValidateCurrencyId(string currencyId)
        {
            bool isCurrencyIdNullOrEmpty = String.IsNullOrWhiteSpace(currencyId);
            if (isCurrencyIdNullOrEmpty)
            {
                throw new ArgumentException(String.Format(ErrorMessages.EmptyStringErrorMessage, "ID"));
            }

            Regex currencyIdRegex = new Regex(Currency.CurrencyPattern);
            Match result = currencyIdRegex.Match(currencyId);
            if (!result.Success)
            {
                throw new ArgumentException(ErrorMessages.InvalidCurrencyIdErrorMessage);
            }
        }
        private void ValidateCurrencyName(string name)
        {
            bool isCurrencyNameNullOrEmpty = String.IsNullOrWhiteSpace(name);
            if (isCurrencyNameNullOrEmpty)
            {
                throw new ArgumentException(String.Format(ErrorMessages.EmptyStringErrorMessage, "Name"));
            }
        }

        private void CheckIfCurrencyIsNull(Currency currency)
        {
            if (currency is null)
            {
                throw new EntityNotFoundException();
            }
        }

        private async Task<T> GetResultFromExternalApiAsync<T>(string apiUrl)
        {
            try
            {
                using (HttpResponseMessage response = await _httpClient.GetAsync(apiUrl))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string responseContent = await response.Content.ReadAsStringAsync();
                        T convertionResult = JsonConvert.DeserializeObject<T>(responseContent);

                        return convertionResult;
                    }
                    else if (response.StatusCode == (HttpStatusCode)429)
                    {
                        throw new CurrencyBeaconApiException(RequestsLimitReachedErrorMessage);
                    }
                    else
                    {
                        throw new CurrencyBeaconApiException(ErrorMessages.ExternalCurrencyApiErrorMessage);
                    }
                }
            }
            catch (Exception ex) when (ex is InvalidOperationException || ex is HttpRequestException || ex is TaskCanceledException)
            {
                throw new CurrencyBeaconApiException(ErrorMessages.ExternalCurrencyApiErrorMessage);
            }
        }

        private string GetApiKey()
        {
            return this._configuration["CurrencyApiKey"];
        }
    }
}
