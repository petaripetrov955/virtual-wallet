﻿using Wallet.Core.Enums;

namespace Wallet.Web.ViewModels.BankCards
{
    public class CardDisplayViewModel
    {
        private const string VisaLogoURL = "http://localhost:5000/images/cards/visa.png";
        private const string VisaElectronURL = "http://localhost:5000/images/cards/electron.png";
        private const string MasterCardURL = "http://localhost:5000/images/cards/mastercard.png";
        private const string MaestroURL = "http://localhost:5000/images/cards/maestro.png";

        public int Id { get; set; }
        public string ValidThrough { get; set; }
        public string HolderName { get; set; }
        public CardType CardType { get; set; }
        public string CardNumber { get; set; }
        public string CardLogo => this.SetCardLogo();

        private string SetCardLogo()
        {
            if (CardType == CardType.Visa)
            {
                return VisaLogoURL;
            }
            else if (CardType == CardType.VisaElectron)
            {
                return VisaElectronURL;
            }
            else if (CardType == CardType.Maestro)
            {
                return MaestroURL;
            }
            else
            {
                return MasterCardURL;
            }
        }
    }
}
