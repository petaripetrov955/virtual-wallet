﻿using Wallet.Core.Dtos;
using Wallet.Core.Models;
using Wallet.Web.ViewModels;

namespace Wallet.Web.Mappers
{
    public class UserMapper
    {
        public User CreateMap(RegistrationDto dto)
        {
            return new User
            {
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Email = dto.Email,
				UserName = dto.UserName,
				PhoneNumber = dto.PhoneNumber,
				AvatarURL = dto.AvatarURL ?? "",
                CreatedAt = DateTime.Now
            };
        }
        public User UpdateMap(UserUpdateDto dto)
        {
            return new User
            {
                FirstName = dto.FirstName ?? default,
                LastName = dto.LastName ?? default,
                Email = dto.Email ?? default,
                UserName = dto.UserName,
                PhoneNumber = dto.PhoneNumber,
                AvatarURL = dto.AvatarURL ?? ""
            };
        }

        public UserUpdateDto UpdateDtoMap(User user)
        {
            return new UserUpdateDto
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                UserName = user.UserName,
                PhoneNumber = user.PhoneNumber,
                AvatarURL = user.AvatarURL
            };
        }

		public User MapUserToViewModel(UserViewModel viewModel)
		{
			return new User()
			{
				FirstName = viewModel.FirstName,
				LastName = viewModel.LastName,
				Email = viewModel.Email,
				UserName = viewModel.UserName,
				PhoneNumber = viewModel.PhoneNumber ?? "",
				AvatarURL = viewModel.AvatarURL ?? ""
			};
		}

		public User MapUserToViewModel(LoginViewModel viewModel)
		{
			return new User()
			{
				UserName = viewModel.UserName,
				Password = viewModel.Password
			};
		}

		public User MapUserToViewModel(RegisterViewModel viewModel)
		{
			return new User()
			{
				FirstName = viewModel.FirstName,
				LastName = viewModel.LastName,
				Email = viewModel.Email,
				UserName = viewModel.UserName,
				PhoneNumber = viewModel.PhoneNumber,
				AvatarURL = viewModel.AvatarURL ?? ""
			};
		}
	}
}
