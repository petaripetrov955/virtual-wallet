﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using Wallet.Core;
using Wallet.Core.Enums;
using Wallet.Core.Models;
using Wallet.Helpers;
using static Wallet.Core.Constants;

namespace Wallet.Tests
{
    public class TestHelper
    {
        private static Role adminRole = new Role(Constants.Roles.AdminRoleId, Constants.Roles.AdminRoleName);
        private static Role userRole = new Role(Constants.Roles.UserRoleId, Constants.Roles.UserRoleName);

        
        public static User GetTestAdmin()
        {
            return new User
            {
                Id = 1,
                FirstName = "Admin",
                LastName = "Test",
                Email = "admin.test@email.bg",
                UserName = "Test User 1",
                Password = "123",
                IsBlocked = false,
                IsDeleted = false,
                PhoneNumber = "1234567890",
                AvatarURL = "imagelink",
                EmailConfirmed = true
            };
        }

        public static User GetTestUser()
        {
            return new User
            {
                Id = 2,
                FirstName = "Test",
                LastName = "User",
                Email = "user@example.org",
                UserName = "user",
                Password = "123",
                IsBlocked = false,
                IsDeleted = false,
                PhoneNumber = "1234567890",
                AvatarURL = "b",
                EmailConfirmed = true,
                Accounts = GetUserTestAccounts()
            };
        }

        public static User GetAnotherTestUser()
        {
            return new User
            {
                Id = 3,
                FirstName = "George",
                LastName = "Georgiev",
                Email = "g.georgiev@email.bg",
                UserName = "georgi",
                Password = "123",
                IsBlocked = false,
                IsDeleted = false,
                PhoneNumber = "0000000000",
                AvatarURL = "b",
                EmailConfirmed = true,
                Accounts = new List<Account> { GetAnotherTestUserAccount() }
            };
        }

        public static User GetBlockedUser()
        {
            return new User
            {
                Id = 5,
                FirstName = "Alexander",
                LastName = "Alexandrov",
                Email = "a.alexandrov@email.bg",
                UserName = "alex",
                Password = "123",
                IsBlocked = true,
                IsDeleted = false,
                PhoneNumber = "3456789012",
                AvatarURL = "b",
                EmailConfirmed = true
            };
        }

        public static List<User> GetTestUsers()
        {
            return new List<User>
            {
                new User
                {
                    Id = 2,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "user@example.org",
                    UserName = "user",
                    Password = "123",
                    IsBlocked = false,
                    IsDeleted = false,
                    PhoneNumber = "1234567890",
                    AvatarURL = "b",
                    EmailConfirmed = true
                },
                new User
                {
                    Id = 3,
                    FirstName = "George",
                    LastName = "Georgiev",
                    Email = "g.georgiev@email.bg",
                    UserName = "georgi",
                    Password = "123",
                    IsBlocked = false,
                    IsDeleted = false,
                    PhoneNumber = "0000000000",
                    AvatarURL = "b",
                    EmailConfirmed = true,
                    Accounts = new List<Account> {GetAnotherTestUserAccount() }
                },
                new User
                {
                    Id = 4,
                    FirstName= "Dimiter",
                    LastName= "Dimitrov",
                    Email= "d.dimitrov@email.bg",
                    UserName = "dimiter",
                    Password = "123",
                    IsBlocked = false,
                    IsDeleted = false,
                    PhoneNumber = "2345678901",
                    AvatarURL = "c",
                    EmailConfirmed = true
                }
            };
        }

        public static Role GetAdminRole()
        {
            return adminRole;
        }

        public static ICollection<Role> GetRoles()
        {
            return new List<Role>()
            {
                adminRole,
                userRole
            };
        }
        public static ICollection<IdentityUserRole<int>> GetUserRoles()
        {
            var adminRole1 = new IdentityUserRole<int>()
            {
                RoleId = adminRole.Id,
                UserId = GetTestAdmin().Id
            };
            var userRole2 = new IdentityUserRole<int>()
            {
                RoleId = userRole.Id,
                UserId = GetTestUser().Id
            };
            var userRole3 = new IdentityUserRole<int>()
            {
                RoleId = userRole.Id,
                UserId = 3
            };
            var userRole4 = new IdentityUserRole<int>()
            {
                RoleId = userRole.Id,
                UserId = 4
            };
            var list = new List<IdentityUserRole<int>>()
            {
                adminRole1,userRole2,userRole3,userRole4
            };
            return list;
        }

        public static List<BankCard> GetTestUserBankCards()
        {
            return new List<BankCard>
            {
                new BankCard
                {
                    Id = 1,
                    CardNumber = HashingUtility.HashData("5543707370373089"),
                    CardNumberEnding ="3089",
                    CardType = CardType.Mastercard,
                    ValidThrough = new DateTime(2024,1,30),
                    CurrencyId = "BGN",
                    PIN = HashingUtility.HashData("1918"),
                    CVV = HashingUtility.HashData("424"),
                    UserId = 3,
                    Name = "BGN Master Card",
                    HolderName = "TEST USER"
                },
                new BankCard
                {
                    Id = 2,
                    CardNumber = HashingUtility.HashData("5543707370373089"),
                    CardNumberEnding ="3089",
                    CardType = CardType.Mastercard,
                    ValidThrough = new DateTime(2024,1,30),
                    CurrencyId = "BGN",
                    PIN = HashingUtility.HashData("1918"),
                    CVV = HashingUtility.HashData("424"),
                    UserId = 4,
                    Name = "BGN Master Card",
                    HolderName = "TEST USER"
                }
            };
        }

        public static List<BankCard> GetTestBankCards()
        {
            return new List<BankCard>
            {
                new BankCard
                {
                    Id = 3,
                    CardNumber = HashingUtility.HashData("5543707370373089"),
                    CardNumberEnding ="3089",
                    CardType = CardType.Mastercard,
                    ValidThrough = new DateTime(2024,1,30),
                    CurrencyId = "BGN",
                    PIN = HashingUtility.HashData("1918"),
                    CVV = HashingUtility.HashData("424"),
                    UserId = 3,
                    Name = "BGN Master Card",
                    HolderName = "GEORGE GEORGIEV"
                },
                new BankCard
                {
                    Id = 4,
                    CardNumber = HashingUtility.HashData("5543707370373089"),
                    CardNumberEnding ="3089",
                    CardType = CardType.Mastercard,
                    ValidThrough = new DateTime(2024,1,30),
                    CurrencyId = "BGN",
                    PIN = HashingUtility.HashData("1918"),
                    CVV = HashingUtility.HashData("424"),
                    UserId = 4,
                    Name = "BGN Master Card",
                    HolderName = "DIMITER DIMITROV"
                }
            };
        }

        public static BankCard GetInactiveBankCard()
        {
            return new BankCard
            {
                Id = 5,
                CardNumber = HashingUtility.HashData("5543707370373089"),
                CardNumberEnding = "3089",
                CardType = CardType.Mastercard,
                ValidThrough = new DateTime(2024, 1, 30),
                CurrencyId = "BGN",
                PIN = HashingUtility.HashData("1918"),
                CVV = HashingUtility.HashData("424"),
                UserId = 1,
                Name = "BGN Master Card",
                HolderName = "TEST USER",
                IsActive = false
            };
        }

        public static BankCard GetTestBankCard()
        {
            return new BankCard
            {
                Id = 4,
                CardNumber = HashingUtility.HashData("5543707370373089"),
                CardNumberEnding = "3089",
                CardType = CardType.Mastercard,
                ValidThrough = new DateTime(2024, 1, 30),
                CurrencyId = "BGN",
                PIN = HashingUtility.HashData("1918"),
                CVV = HashingUtility.HashData("424"),
                UserId = 3,
                Name = "BGN Master Card",
                HolderName = "GEORGE GEORGIEV"
            };
        }

        public static Currency GetTestCurrency()
        {
            return new Currency { Id = "BGN", Name = "Bulgarian Lev" };
        }

        public static List<Currency> GetTestCurrencies()
        {
            return new List<Currency>
            {
                new Currency { Id = "USD", Name = "United States Dollar" },
                new Currency { Id = "EUR", Name = "Euro" }
            };
        }

        public static Account GetTestUserAccount()
        {
            return new Account
            {
                Id = 1,
                Name = "TestAccount1",
                UserId = 2,
                CurrencyId = "BGN",
                Balance = 10,
                IsDeleted = false
            };
        }

        public static Account GetAnotherTestUserAccount()
        {
            return new Account
            {
                Id = 2,
                Name = "TestAccount1",
                UserId = 3,
                CurrencyId = "BGN",
                Balance = 0,
                IsDeleted = false
            };
        }

        public static List<Account> GetUserTestAccounts()
        {
            return new List<Account>
            {
                GetTestUserAccount(),
                new Account
                {
                    Id = 3,
                    Name = "TestAccount",
                    UserId = 2,
                    CurrencyId = "BGN",
                    Balance = 10,
                    IsDeleted = false
                }
            };
        }
    }
}
