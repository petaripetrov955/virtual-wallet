﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Wallet.Core.Models;

namespace Wallet.Repo.Data.EntityConfigurations
{
    internal class BankCardEntityConfiguration : IEntityTypeConfiguration<BankCard>
    {
        public void Configure(EntityTypeBuilder<BankCard> builder)
        {

            builder.HasIndex(bk => bk.CardNumber)
                    .IsUnique();

            builder.HasMany(bk => bk.Transfers)
                    .WithOne(tr => tr.BankCard)
                    .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(bk => bk.Currency)
                    .WithMany(c => c.BankCards)
                    .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
