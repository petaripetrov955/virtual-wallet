﻿using Microsoft.AspNetCore.Identity;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;

namespace Wallet.Repo.Interfaces
{
    public interface IUsersRepository
    {
        Task<List<User>> GetUsersAsync();

        Task<List<User>> GetUsersWithAllTransactionsAsync();

        Task<int> CountAllAsync();

        Task<User> GetByIdAsync(int id);

        Task<User> GetByIdWithBankCardsAsync(int id);
        Task<User> GetByUsernameWithBankCardsAsync(string username);
        Task<User> GetByUsernameWithAccountsAsync(string username);

        Task<User> GetByIdWithAccountsAsync(int id);

        Task<User> GetByIdWithAllTransactionsAsync(int id);

        Task<User> GetByUserNameAsync(string userName);

        Task<User> GetByPhoneNumberAsync(string phoneNumber);

        Task<IdentityResult> AddToRoleAsync(User user, string role);

        Task<IList<string>> GetRolesAsync(User user);

        Task<bool> IsInRoleAsync(User user, string role);

        Task<IdentityResult> RegisterAsync(User user, string password);

        Task<string> GenerateEmailConfirmationTokenAsync(User user);

        Task<string> GenerateChangeEmailTokenAsync(User user, string newEmail);

        Task<IdentityResult> ConfirmEmailAsync(User user, string token);

        Task<IdentityResult> ChangeEmailAsync(User user, string newEmail, string token);

        Task<User> FindByEmailAsync(string email);

        Task<IdentityResult> UpdateAsync(int id, User user);

        Task<IdentityResult> ChangePasswordAsync(int id, string currentPassword, string newPassword);

        Task<IdentityResult> DeleteAsync(int id);

        Task<IdentityResult> BlockAsync(User user);

        Task<IdentityResult> UnblockAsync(User user);

        Task<PaginatedList<User>> SearchByAsync(UserQueryParameters filterParameters);
    }
}
