﻿using Microsoft.EntityFrameworkCore;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Repo.Data;
using Wallet.Repo.Interfaces;
using Wallet.Core.Enums;
using Wallet.Core;

namespace Wallet.Repo.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly ApplicationContext _context;
        public AccountRepository(ApplicationContext context)
        {
            _context = context;
        }

        public async Task<ICollection<Account>> GetAllAsync()
        {
            return await this._context.Accounts.ToListAsync();
        }

        public async Task<Account> CreateAsync(Account accountToAdd)
        {
            await this._context.Accounts.AddAsync(accountToAdd);
            await this._context.SaveChangesAsync();

            return accountToAdd;
        }

        public async Task<Account> DeleteAsync(Account accountToDelete)
        {
            accountToDelete.IsDeleted = true;
            await _context.SaveChangesAsync();

            return accountToDelete;
        }

        public async Task<Account> UpdateAsync(Account accountToUpdate, string nameUpdate)
        {
            if (nameUpdate != null)
            {
                accountToUpdate.Name = nameUpdate;
            }

            await this._context.SaveChangesAsync();

            return accountToUpdate;
        }

        public async Task<Account> GetAsync(int id)
        {
            return await this._context.Accounts
                                               .Include(ac => ac.IncomingTransactions)
                                               .Include(ac => ac.OutgoingTransactions)
                                               .Include(ac => ac.Transfers)
                                               .Include(ac => ac.Currency)
                                               .Include(ac => ac.User)
                                               .FirstOrDefaultAsync(ac => ac.Id == id && !ac.IsDeleted);
        }

        public async Task<PaginatedList<Account>> SearchByAsync(AccountQueryParameters filterParameters, bool isAdmin, User user)
        {
            IEnumerable<Account> result = null;
            if (isAdmin)
            {
                result = await this._context.Accounts.Where(a => !a.IsDeleted).ToListAsync();
            }
            else
            {
                result = await this._context.Accounts.Where(a => !a.IsDeleted && a.UserId == user.Id).ToListAsync();
            }

            if (!string.IsNullOrEmpty(filterParameters.Keyword))
            {
                var result1 = FilterByAccountName(result, filterParameters.Keyword);
                var result2 = FilterByCurrencyId(result, filterParameters.Keyword);
                result = result1.Union(result2);
            }
            else
            {
                result = FilterByAccountName(result, filterParameters.Name);
                result = FilterByCurrencyId(result, filterParameters.CurrencyId);
            }

            result = SortBy(result, filterParameters.SortBy);
            result = SortOrder(result, filterParameters.SortOrder);
            int itemsCount = result.Count();
            int totalPages = itemsCount / filterParameters.PageSize;
            totalPages = itemsCount % filterParameters.PageSize == 0 ? totalPages : ++totalPages;
            result = Paginate(result, filterParameters.PageNumber, filterParameters.PageSize);

            return new PaginatedList<Account>(result.ToList(), totalPages, filterParameters.PageNumber, itemsCount);
        }

        public static IEnumerable<Account> Paginate(IEnumerable<Account> result, int pageNumber, int pageSize)
        {
            if (pageSize == -1)
            {
                return result;
            }
            
            return result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);
        }

        private static IEnumerable<Account> FilterByAccountName(IEnumerable<Account> accounts, string accountName)
        {
            if (!string.IsNullOrEmpty(accountName))
            {
                return accounts.Where(account => account.Name.Contains(accountName, StringComparison.InvariantCultureIgnoreCase));
            }

            return accounts;
        }

        private static IEnumerable<Account> FilterByCurrencyId(IEnumerable<Account> accounts, string currencyId)
        {
            if (!string.IsNullOrEmpty(currencyId))
            {
                return accounts.Where(account => account.CurrencyId.Contains(currencyId, StringComparison.InvariantCultureIgnoreCase));
            }

            return accounts;
        }

        private static IEnumerable<Account> SortBy(IEnumerable<Account> accounts, string sortCriteria)
        {
            object parsementResult;
            Enum.TryParse(typeof(SortingBy), sortCriteria, out parsementResult);
            SortingBy sortBy = parsementResult == null ? SortingBy.none : (SortingBy)parsementResult;

            switch (sortBy)
            {
                case SortingBy.balance:
                    return accounts.OrderBy(account => account.Balance);
                case SortingBy.currencyId:
                    return accounts.OrderBy(account => account.CurrencyId);
                default:
                    return accounts.OrderBy(account => account.Name);
            }
        }

        private static IEnumerable<Account> SortOrder(IEnumerable<Account> accounts, string sortOrder)
        {
            return Constants.SortingOrder.Descending == sortOrder ? accounts.Reverse() : accounts;
        }
    }
}
