﻿using Wallet.Core;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Helpers.Еxceptions;
using Wallet.Repo.Interfaces;
using Wallet.Service.Interfaces;

namespace Wallet.Service.Services
{
    public class TransfersService : ITransfersService
    {
        
        private const string NotMatchingCurrenciesMessage = "Choose an Account and Bank Card with Matching Currencies";
        private const string NotEnoughFundsMessage = "The amount in your account is less than {0} {1}. Missing {2} {3}";

        private readonly ITransfersRepository _transfersRepository;
        private readonly IUsersService _usersService;
        private readonly IAccountService _accountService;
        private readonly IBankCardService _bankCardService;

        public TransfersService(ITransfersRepository transfersRepository, IUsersService usersService, IAccountService accountService, IBankCardService bankCardService)
        {
            this._transfersRepository = transfersRepository;
            _usersService = usersService;
            _accountService = accountService;
            _bankCardService = bankCardService;
        }

        public async Task<List<Transfer>> GetAllTransfersAsync()
        {
            return await this._transfersRepository.GetAllTransfersAsync();
        }

        public async Task<List<Transfer>> GetTransfersByUserIdAsync(int userId)
        {
            return await this._transfersRepository.GetTransfersByUserIdAsync(userId);
        }

        public async Task<Transfer> GetTransferByIdAsync(int id, User user)
        {
            Transfer transfer = await this._transfersRepository.GetTransferByIdAsync(id);
            bool isAdmin = await this._usersService.IsInRoleAsync(user, Constants.Roles.AdminRoleName);

            if (!isAdmin && user.Id != transfer.UserId)
            {
                throw new UnauthorizedOperationException(String.Format(ErrorMessages.UserIsNotOwnerErrorMessage, nameof(Transfer), nameof(Transfer)));
            }

            return transfer;
        }

        public async Task<Transfer> CreateTransferAsync(Transfer transferToCreate, User user)
        {
            this._usersService.ValidateUserStatus(user);
            BankCard bankCard = await _bankCardService.GetByIdAsync(transferToCreate.BankCardId, user);
            Account account = await _accountService.GetByIdAsync(transferToCreate.AccountId, user);

            if (!bankCard.IsActive || account.IsDeleted)
            {
                throw new EntityNotFoundException(ErrorMessages.InactiveBankCardOrAccountMessage);
            }

            if (bankCard.CurrencyId != account.CurrencyId)
            {
                throw new ArgumentException(NotMatchingCurrenciesMessage);
            }

            if (!transferToCreate.IsIncoming && !IsBalanceSufficientForTransfer(transferToCreate, account))
            {
                string errorMessage = String.Format(NotEnoughFundsMessage, transferToCreate.Amount, transferToCreate.CurrencyId, transferToCreate.Amount - account.Balance, transferToCreate.CurrencyId);
                
                throw new InvalidOperationException(errorMessage);
            }

            AdjustAccountBalance(transferToCreate, account);
            Transfer createdTransfer = await _transfersRepository.CreateTransferAsync(transferToCreate);

            return createdTransfer;
        }

        public bool IsBalanceSufficientForTransfer(Transfer transfer, Account account)
        {
            if (transfer.Amount > account.Balance)
            {
                return false;
            }

            return true;
        }

        public void AdjustAccountBalance(Transfer transfer, Account account)
        {
            if (!transfer.IsIncoming)
            {
                account.Balance -= transfer.Amount;
            }
            else
            {
                account.Balance += transfer.Amount;
            }
        }

        public async Task<List<Transfer>> FilterByAsync(User user, bool IsAdmin, TransferQueryParameters filterParams = null)
        {
            return await _transfersRepository.FilterByAsync(filterParams, user, IsAdmin);
        }
    }
}
