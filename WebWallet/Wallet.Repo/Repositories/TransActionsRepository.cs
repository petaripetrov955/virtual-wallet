﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Repo.Data;
using Wallet.Repo.Interfaces;
using Wallet.Web.QueryParameters;
using Wallet.Core.Models;
using Microsoft.EntityFrameworkCore;
using Wallet.Helpers.Еxceptions;
using Wallet.Core;
using Wallet.Core.QueryParameters;

namespace Wallet.Repo.Repositories
{
    public class TransactionsRepository : ITransactionsRepository
    {
        private readonly ApplicationContext _dbContext;

        public TransactionsRepository(ApplicationContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<List<Transaction>> GetAllTransactionsAsync()
        {
            return await this._dbContext.Transactions
                                .Include(tr => tr.SenderUser)
                                .Include(tr => tr.SenderAccount)
                                .Include(tr => tr.OriginCurrency)
                                .Include(tr => tr.RecepientUser)
                                .Include(tr => tr.RecepientAccount)
                                .Include(tr => tr.TargetCurrency)
                                .OrderByDescending(tr => tr.CreatedAt)
                                .ToListAsync();                                                            
        }

        public async Task<List<Transaction>> GetTransactionsBySenderOrRecepientUserIdAsync(int userId)
        {            
            return await this._dbContext.Transactions
                                .Where(tr => ((tr.SenderUserId == userId) || (tr.RecepientUserId == userId)))
                                .Include(tr => tr.SenderUser)
                                .Include(tr => tr.SenderAccount)
                                .Include(tr => tr.OriginCurrency)
                                .Include(tr => tr.RecepientUser)
                                .Include(tr => tr.RecepientAccount)
                                .Include(tr => tr.TargetCurrency)
                                .OrderByDescending(tr => tr.CreatedAt)
                                .ToListAsync();
        }


        public async Task<Transaction> GetTransactionByIdAsync(int id)
        {
            return await this._dbContext.Transactions
                                .Include(tr => tr.SenderUser)
                                .Include(tr => tr.SenderAccount)
                                .Include(tr => tr.OriginCurrency)
                                .Include(tr => tr.RecepientUser)
                                .Include(tr => tr.RecepientAccount)
                                .Include(tr => tr.TargetCurrency)
                                .FirstOrDefaultAsync(ta => ta.Id == id)
                                ?? throw new EntityNotFoundException(ErrorMessages.TransactionNotExistsErrorMessage + $"'{id}'");
        }

        public async Task<Transaction> CreateTransactionAsync(Transaction transactionToCreate)
        {
            _dbContext.Transactions.Add(transactionToCreate);
            _dbContext.SaveChanges();
            transactionToCreate = await this.GetTransactionByIdAsync(transactionToCreate.Id);
            return transactionToCreate;            
        }

        public async Task<List<Transaction>> FilterByAsync(TransactionQueryParameters filterParams, User user, bool IsAdmin)
        {
            IEnumerable<Transaction> result;
            if (IsAdmin) 
            {
                result = await this.GetAllTransactionsAsync();
            }
            else
            {
                result = await this.GetTransactionsBySenderOrRecepientUserIdAsync(user.Id);
            }

            if (filterParams is null)
            {
                return result.ToList();
            }

            if (!string.IsNullOrEmpty(filterParams.Keyword))
            {
                var result1 = FilterBySenderUserName(result, filterParams.Keyword);
                var result2 = FilterByRecepientUserName(result, filterParams.Keyword);
                var result3 = FilterBySenderAccountName(result, filterParams.Keyword);
                var result4 = FilterByRecepientAccountName(result, filterParams.Keyword);
                var result5 = FilterByDescription(result, filterParams.Keyword);
                var temp = result1.Union(result2);
                var temp2 = temp.Union(result3);
                var temp3 = temp2.Union(result4);
                var temp4 = temp3.Union(result5);
                result = temp4.Union(result5).ToList();
                result = SortBy(result, filterParams.SortBy);
                result = SortOrder(result, filterParams.SortOrder);                
            }
            else
            {
                result = FilterBySenderUserName(result, filterParams.SenderUserName);
                result = FilterBySenderUserId(result, filterParams.SenderUserId);
                result = FilterByRecepientUserName(result, filterParams.RecepientUserName);
                result = FilterByRecepientUserId(result, filterParams.RecepientUserId);
                result = FilterBySenderAccountName(result, filterParams.SenderAccountName);
                result = FilterBySenderAccountId(result, filterParams.SenderAccountId);
                result = FilterByRecepientAccountName(result, filterParams.RecepientUserName);
                result = FilterByRecepientAccountId(result, filterParams.RecepientAccountId);                
                result = FilterByTargetCurrencyName(result, filterParams.TargetCurrencyName);
                result = FilterByTargetCurrencyId(result, filterParams.TargetCurrencyId);
                result = FilterByOriginCurrencyName(result, filterParams.OriginCurrencyName);
                result = FilterByOriginCurrencyId(result, filterParams.OriginCurrencyId);
                result = FilterByDescription(result, filterParams.Description);
                result = FilterByInternal(result, filterParams.IsInternal);
                result = FilterBySenderOrRecepientAccountId(result, filterParams.SenderOrRecepientAccountId);
                result = SortBy(result, filterParams.SortBy);
                result = SortOrder(result, filterParams.SortOrder);                
            }

            return result.ToList();
        }       

        private static IEnumerable<Transaction> FilterBySenderUserName(IEnumerable<Transaction> transactions, string senderUsername)
        {
            if (!string.IsNullOrEmpty(senderUsername))
            {
                return transactions.Where(trans => trans.SenderUser.UserName.Contains(senderUsername, StringComparison.InvariantCultureIgnoreCase));
            }

            return transactions;
        }

        private static IEnumerable<Transaction> FilterBySenderUserId(IEnumerable<Transaction> transactions, int? senderUserId)
        {
            if (senderUserId is not null)
            {
                return transactions.Where(trans => trans.SenderUserId.Equals(senderUserId));
            }

            return transactions;
        }

        private static IEnumerable<Transaction> FilterByRecepientUserName(IEnumerable<Transaction> transactions, string recepientUsername)
        {
            if (!string.IsNullOrEmpty(recepientUsername))
            {
                return transactions.Where(trans => trans.RecepientUser.UserName.Contains(recepientUsername, StringComparison.InvariantCultureIgnoreCase));
            }

            return transactions;
        }

        private static IEnumerable<Transaction> FilterByRecepientUserId(IEnumerable<Transaction> transactions, int? recepientUserId)
        {
            if (recepientUserId is not null)
            {
                return transactions.Where(trans => trans.RecepientUserId.Equals(recepientUserId));
            }

            return transactions;
        }

        private static IEnumerable<Transaction> FilterBySenderAccountName(IEnumerable<Transaction> transactions, string senderAccountName)
        {
            if (!string.IsNullOrEmpty(senderAccountName))
            {
                return transactions.Where(trans => trans.SenderAccount.Name.Contains(senderAccountName, StringComparison.InvariantCultureIgnoreCase));
            }

            return transactions;
        }

        private static IEnumerable<Transaction> FilterBySenderAccountId(IEnumerable<Transaction> transactions, int? senderAccountId)
        {
            if (senderAccountId is not null)
            {
                return transactions.Where(trans => trans.SenderAccountId.Equals(senderAccountId));
            }

            return transactions;
        }

        private static IEnumerable<Transaction> FilterByRecepientAccountName(IEnumerable<Transaction> transactions, string recepientAccountName)
        {
            if (!string.IsNullOrEmpty(recepientAccountName))
            {
                return transactions.Where(trans => trans.RecepientAccount.Name.Contains(recepientAccountName, StringComparison.InvariantCultureIgnoreCase));
            }

            return transactions;
        }

        private static IEnumerable<Transaction> FilterByRecepientAccountId(IEnumerable<Transaction> transactions, int? recepientAccountId)
        {
            if (recepientAccountId is not null)
            {
                return transactions.Where(trans => trans.RecepientAccountId.Equals(recepientAccountId));
            }

            return transactions;
        }

        

        private static IEnumerable<Transaction> FilterByTargetCurrencyName(IEnumerable<Transaction> transactions, string targetCurrencyName)
        {
            if (!string.IsNullOrEmpty(targetCurrencyName))
            {
                return transactions.Where(trans => trans.TargetCurrency.Name.Contains(targetCurrencyName, StringComparison.InvariantCultureIgnoreCase));
            }

            return transactions;
        }

        private static IEnumerable<Transaction> FilterByTargetCurrencyId(IEnumerable<Transaction> transactions, string targetCurrencyId)
        {
            if (!string.IsNullOrEmpty(targetCurrencyId))
            {
                return transactions.Where(trans => trans.TargetCurrencyId.Equals(targetCurrencyId));
            }

            return transactions;
        }

        private static IEnumerable<Transaction> FilterByOriginCurrencyName(IEnumerable<Transaction> transactions, string originCurrencyName)
        {
            if (!string.IsNullOrEmpty(originCurrencyName))
            {
                return transactions.Where(trans => trans.OriginCurrency.Name.Contains(originCurrencyName, StringComparison.InvariantCultureIgnoreCase));
            }

            return transactions;
        }

        private static IEnumerable<Transaction> FilterByOriginCurrencyId(IEnumerable<Transaction> transactions, string originCurrencyId)
        {
            if (!string.IsNullOrEmpty(originCurrencyId))
            {
                return transactions.Where(trans => trans.OriginCurrencyId.Equals(originCurrencyId));
            }

            return transactions;
        }

        private static IEnumerable<Transaction> FilterByDescription(IEnumerable<Transaction> transactions, string description)
        {
            if (!string.IsNullOrEmpty(description))
            {
                return transactions.Where(trans => trans.Description.Contains(description, StringComparison.InvariantCultureIgnoreCase));
            }
            return transactions;
        }

        private static IEnumerable<Transaction> FilterByInternal(IEnumerable<Transaction> transactions, bool? isinternal)
        {
            if (isinternal is true)
            {
                return transactions.Where(trans => trans.IsInternal);
            }
            return transactions;
        }

        private static IEnumerable<Transaction> FilterBySenderOrRecepientAccountId(IEnumerable<Transaction> transactions, int? id)
        {
            if (id is not null)
            {
                return transactions.Where(trans => trans.SenderAccountId.Equals(id) || trans.RecepientAccountId.Equals(id));
            }
            return transactions;
        }

        private static IEnumerable<Transaction> SortBy(IEnumerable<Transaction> transactions, string sortby)
        {
            switch (sortby)
            {
                case "originamount":
                    return transactions.OrderBy(trans => trans.OriginAmount);
                case "targetamount":
                    return transactions.OrderBy(trans => trans.TargetAmount);
                case "recepientusername":
                    return transactions.OrderBy(trans => trans.RecepientUser.UserName);                
                default:
                    return transactions.OrderByDescending(trans => trans.CreatedAt);
            }
        }

        private static IEnumerable<Transaction> SortOrder(IEnumerable<Transaction> transactions, string sortOrder)
        {
            return (sortOrder == "desc") ? transactions.Reverse() : transactions;
        }
    }
}
