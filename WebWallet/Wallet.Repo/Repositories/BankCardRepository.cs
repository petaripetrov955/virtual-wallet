﻿using Microsoft.EntityFrameworkCore;
using Wallet.Core.Dtos.BankCard;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Repo.Data;
using Wallet.Repo.Interfaces;

namespace Wallet.Repo.Repositories
{
    public class BankCardRepository : IBankCardRepository
    {
        private readonly ApplicationContext _context;

        public BankCardRepository(ApplicationContext context)
        {
            this._context = context;
        }

        public async Task<ICollection<BankCard>> GetAllAsync(BankCardQueryParameters parameters)
        {
            ICollection<BankCard> cards = this._context
                                                       .BankCards
                                                       .Where(bc => bc.IsActive)
                                                       .ToHashSet();

            if (parameters.Holder != null)
            {
                cards = cards.Where(c => c.HolderName == parameters.Holder).ToHashSet();
            }

            if (parameters.UserId != null)
            {
                return cards.Where(c => c.UserId == parameters.UserId).ToHashSet();
            }

            return cards;
        }

        public async Task<BankCard> GetByIdAsync(int id)
        {
            return await this._context.BankCards
                                                .Include(bc => bc.User)
                                                .Include(bc => bc.Currency)
                                                .Include(bc => bc.Transfers)
                                                .FirstOrDefaultAsync(bc => bc.Id == id);
        }

        public async Task<BankCard> CreateAsync(BankCard cardToAdd)
        {
            await this._context.BankCards.AddAsync(cardToAdd);
            await this._context.SaveChangesAsync();

            return cardToAdd;
        }

        public async Task<BankCard> DeleteAsync(BankCard cardToDelete)
        {
            cardToDelete.IsActive = false;
            await this._context.SaveChangesAsync();

            return cardToDelete;
        }

        public async Task<BankCard> UpdateAsync(BankCard card, BankCardUpdateDto cardUpdates)
        {
            if (cardUpdates.Name != null)
            {
                card.Name = cardUpdates.Name;
            }

            if (cardUpdates.PIN != null)
            {
                card.PIN = cardUpdates.PIN;
            }

            await this._context.SaveChangesAsync();

            return card;
        }
    }
}
