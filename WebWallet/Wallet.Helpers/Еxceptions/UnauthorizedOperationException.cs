﻿
namespace Wallet.Helpers.Еxceptions
{
	public class UnauthorizedOperationException : ApplicationException
	{
		public UnauthorizedOperationException(string message)
			: base(message)
		{
		}
	}
}
