﻿using System.Text.RegularExpressions;
using Wallet.Core;
using Wallet.Core.Dtos.Account;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Helpers.Еxceptions;
using Wallet.Repo.Interfaces;
using Wallet.Service.Interfaces;

namespace Wallet.Service.Services
{
    public class AccountService : IAccountService
    {
        private const string Account_Not_Found_Error_Message = "Sorry, the requested account could not be found. Please double-check the  account details and try again. If you believe this is an error, please contact customer support for assistance.";

        private readonly IAccountRepository _accountRepository;
        private readonly ICurrencyService _currencyService;
        private readonly IUsersService _userService;

        public AccountService(IAccountRepository accountRepository, ICurrencyService currencyService, IUsersService userService)
        {
            _accountRepository = accountRepository;
            _currencyService = currencyService;
            _userService = userService;
        }

        public async Task<Account> GetByIdAsync(int accountId, User user)
        {
            Account account = await this._accountRepository.GetAsync(accountId);

            if (account is null || account.IsDeleted)
            {
                throw new EntityNotFoundException(Account_Not_Found_Error_Message);
            }

            bool isAdmin = await this._userService.IsInRoleAsync(user, Constants.Roles.AdminRoleName);
            if (isAdmin)
            {
                return account;
            }

            CheckIfAccountOwner(user, account);

            return account;
        }

        public async Task<Account> GetByIdAsync(int accountId)
        {
            Account account = await this._accountRepository.GetAsync(accountId);

            return account ?? throw new EntityNotFoundException();
        }

        public void CheckIfAccountOwner(User user, Account account)
        {
            if (user.Id != account.UserId)
            {
                throw new UnauthorizedOperationException(String.Format(ErrorMessages.UserIsNotOwnerErrorMessage, nameof(Account), nameof(Account)));
            };
        }

        public async Task<Account> CreateAsync(Account accountToCreate, User user)
        {
            try
            {
                this._userService.ValidateUserStatus(user);
                ICollection<Currency> currencies = await this._currencyService.GetAllAsync();
                if (!currencies.Any(c => c.Id == accountToCreate.CurrencyId))
                {
                    throw new ArgumentException(String.Format(ErrorMessages.NotSupportedCurrencyErrorMessage, accountToCreate.CurrencyId));
                }

                if (user.Accounts.Any(ac => ac.CurrencyId == accountToCreate.CurrencyId))
                {
                    throw new DuplicateEntityException(ErrorMessages.AccountWithTheSameCurrencyAlreadySetErrorMessage);
                }

                user.Accounts.Add(accountToCreate);

                return await this._accountRepository.CreateAsync(accountToCreate);
            }
            catch (InvalidOperationException)
            {
                throw new ArgumentException(String.Format(ErrorMessages.NotSupportedCurrencyErrorMessage, accountToCreate.CurrencyId));
            }
        }

        public async Task<Account> DeleteAsync(int accountId, User user)
        {
            this._userService.ValidateUserStatus(user);
            Account accountToDelete = await this.GetByIdAsync(accountId, user);
            if (accountToDelete.Balance > Account.Close_Required_Amount)
            {
                throw new NonZeroBalanceException();
            }

            return await this._accountRepository.DeleteAsync(accountToDelete);
        }

        public async Task<Account> UpdateAsync(AccountUpdateDto accountUpdates, User user)
        {
            this._userService.ValidateUserStatus(user);
            Account accountToUpdate = await this.GetByIdAsync(accountUpdates.Id, user);
            if (accountToUpdate.Name == accountUpdates.Name)
            {
                throw new ArgumentException(ErrorMessages.DuplicatedAccountNameErrorMessage);
            }

            Regex nameRegex = new Regex(Account.Name_Regex_Pattern);
            Match result = nameRegex.Match(accountUpdates.Name);
            if (!result.Success)
            {
                throw new ArgumentException(ErrorMessages.InvalidAccountNameErrorMessage);
            }

            return await this._accountRepository.UpdateAsync(accountToUpdate, accountUpdates.Name);
        }

        public async Task<PaginatedList<Account>> SearchByAsync(AccountQueryParameters filterParameters, User user)
        {
            bool isAdmin = await this._userService.IsInRoleAsync(user, Constants.Roles.AdminRoleName);

            return await this._accountRepository.SearchByAsync(filterParameters, isAdmin, user);
        }
    }
}
