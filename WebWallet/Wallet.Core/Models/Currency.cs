﻿using System.ComponentModel.DataAnnotations;

namespace Wallet.Core.Models
{
    public class Currency
    {
        public const string CurrencyPattern = @"^[A-Z]{3}$";

        public Currency()
        {
            this.IncomingTransactions = new List<Transaction>();
            this.OutgoingTransactions = new List<Transaction>();
            this.BankCards = new List<BankCard>();
            this.Transfers = new List<Transfer>();
            this.Accounts = new List<Account>();
        }

        [Required]
        [RegularExpression(CurrencyPattern, ErrorMessage = ErrorMessages.InvalidCurrencyIdErrorMessage)]
        [MaxLength(3)]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        public bool IsDeleted { get; set; }
        public ICollection<Transaction> IncomingTransactions { get; set; }
        public ICollection<Transaction> OutgoingTransactions { get; set; }
        public ICollection<Transfer> Transfers { get; set; }
        public ICollection<Account> Accounts { get; set; }
        public ICollection<BankCard> BankCards { get; set; }
    }
}
