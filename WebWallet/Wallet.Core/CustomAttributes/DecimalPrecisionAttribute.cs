﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wallet.Core.CustomAttributes
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class DecimalPrecisionAttribute : ValidationAttribute
    {
        private readonly int precision;

        public DecimalPrecisionAttribute(int precision)
        {
            this.precision = precision;
        }

        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }

            if (value is decimal decimalValue)
            {
                int decimalPlaces = GetDecimalPlaces(decimalValue);
                if (decimalPlaces <= precision)
                {
                    return ValidationResult.Success;
                }
            }

            return new ValidationResult("Amount must have precision of 0, 1, or 2.");
        }

        private int GetDecimalPlaces(decimal value)
        {
            // Extracts the number of decimal places in the given decimal value
            var bits = decimal.GetBits(value);
            int scale = (bits[3] >> 16) & 0x000000FF;
            return scale;
        }
    }

}
