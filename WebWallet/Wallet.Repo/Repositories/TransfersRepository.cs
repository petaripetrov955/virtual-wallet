﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Core;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Helpers.Еxceptions;
using Wallet.Repo.Data;
using Wallet.Repo.Interfaces;

namespace Wallet.Repo.Repositories
{
    public class TransfersRepository : ITransfersRepository
    {
        private readonly ApplicationContext _context;

        public TransfersRepository(ApplicationContext dbContext)
        {
            this._context = dbContext;
        }

        

        public async Task<List<Transfer>> GetAllTransfersAsync()
        {
            return await this._context.Transfers
                                .Include(tr => tr.BankCard)
                                .Include(tr => tr.Currency)
                                .Include(tr => tr.User)
                                .Include(tr => tr.Account)
                                .OrderByDescending(tr => tr.CreatedAt)
                                .ToListAsync();
        }

        public async Task<List<Transfer>> GetTransfersByUserIdAsync(int userId)
        {
            return await this._context.Transfers
                                .Where(tr => tr.UserId == userId)                                
                                .Include(tr => tr.BankCard)
                                .Include(tr => tr.Currency)
                                .Include(tr => tr.User)
                                .Include(tr => tr.Account)
                                .OrderByDescending(tr => tr.CreatedAt)
                                .ToListAsync();
        }

        public async Task<Transfer> GetTransferByIdAsync(int id)
        {
            return await this._context.Transfers
                                .Include(tr => tr.BankCard)
                                .Include(tr => tr.Currency)
                                .Include(tr => tr.User)
                                .Include(tr => tr.Account)
                                .FirstOrDefaultAsync(tr => tr.Id == id)
                                ?? throw new EntityNotFoundException(ErrorMessages.TransferNotExistsErrorMessage + $"'{id}'");
        }

        public async Task<Transfer> CreateTransferAsync(Transfer transferToCreate)
        {
            await _context.Transfers.AddAsync(transferToCreate);
            await _context.SaveChangesAsync();
            transferToCreate = await this.GetTransferByIdAsync(transferToCreate.Id);
           
            return transferToCreate;            
        }

        public async Task<List<Transfer>> FilterByAsync(TransferQueryParameters filterParams, User user, bool IsAdmin)
        {
            IEnumerable<Transfer> result;
            if (IsAdmin)
            {
                result = await this.GetAllTransfersAsync();
            }
            else
            {
                result = await this.GetTransfersByUserIdAsync(user.Id);
            }

            if (filterParams is null)
            {
                return result.ToList();
            }
                        
            if (!string.IsNullOrEmpty(filterParams.Keyword))
            {
                var result1 = FilterByUserName(result, filterParams.Keyword);                
                var result2 = FilterByAccountName(result, filterParams.Keyword);
                var result3 = FilterByBankCardName(result, filterParams.Keyword);
                var result4 = FilterByDescription(result, filterParams.Keyword);                
                var temp = result1.Union(result2);
                var temp2 = temp.Union(result3);
                result = temp2.Union(result4).ToList();
                result = SortBy(result, filterParams.SortBy);
                result = SortOrder(result, filterParams.SortOrder);
                
            }
            else
            {
                result = FilterByUserName(result, filterParams.UserName);
                result = FilterByUserId(result, filterParams.UserId);
                result = FilterByAccountName(result, filterParams.AccountName);
                result = FilterByAccountId(result, filterParams.AccountId);
                result = FilterByBankCardName(result, filterParams.BankCardName);
                result = FilterByBankCardId(result, filterParams.BankCardId);
                result = FilterByCurrencyName(result, filterParams.CurrencyName);
                result = FilterByCurrencyId(result, filterParams.CurrencyId);
                result = FilterByDescription(result, filterParams.Description);
                result = FilterByIncoming(result, filterParams.IsIncoming);
                result = SortBy(result, filterParams.SortBy);
                result = SortOrder(result, filterParams.SortOrder);
               
            }

            return result.ToList();
        }
        

        private static IEnumerable<Transfer> FilterByUserName(IEnumerable<Transfer> transfers, string username)
        {
            if (!string.IsNullOrEmpty(username))
            {
                return transfers.Where(trans => trans.User.UserName.Contains(username, StringComparison.InvariantCultureIgnoreCase));
            }

            return transfers;
        }

        private static IEnumerable<Transfer> FilterByUserId(IEnumerable<Transfer> transfers, int? userId)
        {
            if (userId is not null)
            {
                return transfers.Where(trans => trans.UserId.Equals(userId));
            }

            return transfers;
        }

        private static IEnumerable<Transfer> FilterByAccountName(IEnumerable<Transfer> transfers, string accountName)
        {
            if (!string.IsNullOrEmpty(accountName))
            {
                return transfers.Where(trans => trans.Account.Name.Contains(accountName, StringComparison.InvariantCultureIgnoreCase));
            }

            return transfers;
        }

        private static IEnumerable<Transfer> FilterByAccountId(IEnumerable<Transfer> transfers, int? accountId)
        {
            if (accountId is not null)
            {
                return transfers.Where(trans => trans.AccountId.Equals(accountId));
            }

            return transfers;
        }

        private static IEnumerable<Transfer> FilterByBankCardName(IEnumerable<Transfer> transfers, string bankCardName)
        {
            if (!string.IsNullOrEmpty(bankCardName))
            {
                return transfers.Where(trans => trans.BankCard.Name.Contains(bankCardName, StringComparison.InvariantCultureIgnoreCase));
            }

            return transfers;
        }

        private static IEnumerable<Transfer> FilterByBankCardId(IEnumerable<Transfer> transfers, int? bankCardId)
        {
            if (bankCardId is not null)
            {
                return transfers.Where(trans => trans.BankCardId.Equals(bankCardId));
            }

            return transfers;
        }

        private static IEnumerable<Transfer> FilterByCurrencyName(IEnumerable<Transfer> transfers, string currencyName)
        {
            if (!string.IsNullOrEmpty(currencyName))
            {
                return transfers.Where(trans => trans.Currency.Name.Contains(currencyName, StringComparison.InvariantCultureIgnoreCase));
            }

            return transfers;
        }

        private static IEnumerable<Transfer> FilterByCurrencyId(IEnumerable<Transfer> transfers, string currencyId)
        {
            if (!string.IsNullOrEmpty(currencyId))
            {
                return transfers.Where(trans => trans.CurrencyId.Equals(currencyId));
            }

            return transfers;
        }

        private static IEnumerable<Transfer> FilterByDescription(IEnumerable<Transfer> transfers, string description)
        {
            if (!string.IsNullOrEmpty(description))
            {
                return transfers.Where(trans => trans.Description.Contains(description, StringComparison.InvariantCultureIgnoreCase));
            }
            return transfers;
        }

        private static IEnumerable<Transfer> FilterByIncoming(IEnumerable<Transfer> transfers, bool? incoming)
        {
            if (incoming is true)
            {
                return transfers.Where(trans => trans.IsIncoming);
            }
            return transfers;
        }

        private static IEnumerable<Transfer> SortBy(IEnumerable<Transfer> transfers, string sortby)
        {
            switch (sortby)
            {
                case "amount":
                    return transfers.OrderBy(trans => trans.Amount);
                case "incoming":
                    return transfers.OrderBy(trans => trans.IsIncoming);
                case "account":
                    return transfers.OrderBy(trans => trans.Account.Name);
                case "bankcard":
                    return transfers.OrderBy(trans => trans.BankCard.Name);
                case "currency":
                    return transfers.OrderBy(trans => trans.CurrencyId);
                default:
                    return transfers.OrderByDescending(trans => trans.CreatedAt);
            }
        }

        private static IEnumerable<Transfer> SortOrder(IEnumerable<Transfer> transfers, string sortOrder)
        {
            return (sortOrder == "desc") ? transfers.Reverse() : transfers;
        }
    }
}
