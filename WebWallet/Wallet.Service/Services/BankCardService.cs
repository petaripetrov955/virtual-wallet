﻿using System.Text.RegularExpressions;
using Wallet.Core;
using Wallet.Core.Dtos.BankCard;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Helpers;
using Wallet.Helpers.Еxceptions;
using Wallet.Repo.Interfaces;
using Wallet.Service.Interfaces;

namespace Wallet.Service.Services
{
    public class BankCardService : IBankCardService
    {
        public const string CardAlreadyExistsErrorMessage = "Card with the same number already exists. Please review your card details and ensure that the number entered matches the number on your card.";

        private const string UpdateCancelledErrorMessage = "The update request has been canceled. The {0} provided is already assigned to the card.";
        private const string NotActiveCardErrorMessage = "Card with ID {0} not found or is not active.";

        private readonly IBankCardRepository _bankCardRepository;
        private readonly ICurrencyService _currencyService;
        private readonly IUsersService _usersService;

        public BankCardService(IBankCardRepository bankCardRepository, ICurrencyService currencyService, IUsersService usersService)
        {
            _bankCardRepository = bankCardRepository;
            _currencyService = currencyService;
            _usersService = usersService;
        }


        public async Task<ICollection<BankCard>> GetAllAsync(BankCardQueryParameters parameters, User user)
        {
            IList<string> userRoles = await this._usersService.GetRolesAsync(user);
            ICollection<BankCard> cards;

            if (userRoles.Any(r => r == "Admin"))
            {
                cards = await this._bankCardRepository.GetAllAsync(parameters);
            }
            else
            {
                if (parameters.Holder != null)
                {
                    string userFullName =string.Join(" ", user.FirstName.ToUpper(), user.LastName.ToUpper());
                    bool isMatch = parameters.Holder == userFullName;
                    if (!isMatch)
                    {
                        throw new UnauthorizedOperationException(String.Format(ErrorMessages.UserIsNotOwnerErrorMessage, nameof(BankCard), nameof(BankCard)));
                    }
                }

                parameters.UserId = user.Id;
                cards = await this._bankCardRepository.GetAllAsync(parameters);
            }

            if (cards.Count == 0)
            {
                throw new InvalidOperationException(String.Format(ErrorMessages.EmptyCollectionErrorMessage, nameof(BankCard)));
            }

            return cards;
        }

        public async Task<BankCard> GetByIdAsync(int id, User user)
        {
            BankCard card = await this._bankCardRepository.GetByIdAsync(id);
            if (card is null || !card.IsActive)
            {
                throw new EntityNotFoundException(String.Format(NotActiveCardErrorMessage, id));
            }
            else if (card.UserId != user.Id)
            {
                throw new UnauthorizedOperationException(String.Format(ErrorMessages.UserIsNotOwnerErrorMessage, nameof(BankCard), nameof(BankCard)));
            }

            return card;
        }

        public async Task<BankCard> CreateAsync(BankCard cardToCreate, User user)
        {
            this._usersService.ValidateUserStatus(user);
            cardToCreate.CardNumber = HashingUtility.HashData(cardToCreate.CardNumber);
            cardToCreate.PIN = HashingUtility.HashData(cardToCreate.PIN);
            cardToCreate.CVV = HashingUtility.HashData(cardToCreate.CVV);

            ICollection<Currency> currencies = await this._currencyService.GetAllAsync();
            if (!currencies.Any(c => c.Id == cardToCreate.CurrencyId))
            {
                throw new ArgumentException(String.Format(ErrorMessages.NotSupportedCurrencyErrorMessage, cardToCreate.CurrencyId));
            }

            BankCard createdCard = await this._bankCardRepository.CreateAsync(cardToCreate);
            user.BankCards.Add(createdCard);

            return createdCard;
        }

        public async Task<BankCard> DeleteAsync(int id, User user)
        {
            this._usersService.ValidateUserStatus(user);
            BankCard removedCard = await this._bankCardRepository.DeleteAsync(await GetByIdAsync(id, user));

            return removedCard;
        }

        public async Task<BankCard> UpdateAsync(BankCardUpdateDto incomingUpdates, User user)
        {
            this._usersService.ValidateUserStatus(user);
            BankCard cardToUpdate = await this.GetByIdAsync(incomingUpdates.Id, user);
            if (incomingUpdates.PIN != null)
            {
                Regex pinRegex = new Regex(BankCard.PinPattern);
                Match result = pinRegex.Match(incomingUpdates.PIN);
                if (!result.Success)
                {
                    throw new ArgumentException(BankCard.InvalidPinErrorMessage);
                }

                incomingUpdates.PIN = HashingUtility.HashData(incomingUpdates.PIN);
                if (cardToUpdate.PIN == incomingUpdates.PIN)
                {
                    throw new ArgumentException(String.Format(UpdateCancelledErrorMessage, "PIN"));
                }
            }

            if (incomingUpdates.Name != null)
            {
                if (cardToUpdate.Name == incomingUpdates.Name)
                {
                    throw new ArgumentException(String.Format(UpdateCancelledErrorMessage, "name"));
                }

                Regex nameRegex = new Regex(Account.Name_Regex_Pattern);
                Match result = nameRegex.Match(incomingUpdates.Name);
                if (!result.Success)
                {
                    throw new ArgumentException(ErrorMessages.InvalidAccountNameErrorMessage);
                }
            }

            return await this._bankCardRepository.UpdateAsync(cardToUpdate, incomingUpdates);
        }
    }
}
