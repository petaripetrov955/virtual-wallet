﻿
using Microsoft.EntityFrameworkCore;
using Wallet.Core.Enums;
using Wallet.Core.Models;
using Wallet.Helpers;

namespace Wallet.Repo.Data.Seed
{
    static class AccountModelBuilderExtension
    {
        public static void SeedAccounts(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>().HasData
                 (
                new List<Account>
                {
                    new Account
                    {
                        Id = 1, 
                        Name = "My BGN Wallet",
                        UserId = 7,                        
                        CurrencyId = "BGN",
                        Balance = 1000
                    },

                    new Account
                    {
                        Id = 2,
                        Name = "My USD Wallet",
                        UserId = 7,
                        CurrencyId = "USD",
                        Balance = 1000
                    },

                    new Account
                    {
                        Id = 3,
                        Name = "For Journeys",
                        UserId = 3,
                        CurrencyId = "EUR",
                        Balance = 1000
                    },

                    new Account
                    {
                        Id = 4,
                        Name = "USD smetka",
                        UserId = 3,
                        CurrencyId = "USD",
                        Balance = 1000
                    },

                    new Account
                    {
                        Id = 5,
                        Name = "tainata BGN smetka",
                        UserId = 3,
                        CurrencyId = "BGN",
                        Balance = 1000
                    },

                    new Account
                    {
                        Id = 6,
                        Name = "na admina evroto",
                        UserId = 1,
                        CurrencyId = "EUR",
                        Balance = 1000
                    },

                    new Account
                    {
                        Id = 7,
                        Name = "Iva's AUD Account",
                        UserId = 7,
                        CurrencyId = "AUD",
                        Balance = 1000
                    }
               });
        }
    }
}
