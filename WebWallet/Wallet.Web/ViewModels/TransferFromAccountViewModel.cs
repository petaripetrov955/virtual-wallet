﻿using System.ComponentModel.DataAnnotations;
using Wallet.Core.CustomAttributes;
using Wallet.Web.ViewModels.Interfaces;

namespace Wallet.Web.ViewModels
{
    public class TransferFromAccountViewModel : ICreateTransferModel
    {
        public int UserId { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public decimal AccountBalance { get; set; }

        public string CurrencyId { get; set; }

        public int BankCardId { get; set; }
        public string BankCardName { get; set; }

        [Range(0.01, 1000000.00, ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [DecimalPrecision(2)]
        public decimal Amount { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is required.")]
        [MaxLength(256, ErrorMessage = "The '{0}' field must be less than {1} characters.")]
        [MinLength(8, ErrorMessage = "The '{0}' field must be at least {1} characters.")]
        public string Description { get; set; }

        public bool HasBankCard { get; set; } = true;

        public bool IsIncoming { get; set; }

        public bool IsConfirmed { get; set; }

    }
}
