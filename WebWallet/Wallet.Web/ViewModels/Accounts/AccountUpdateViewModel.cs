﻿using System.ComponentModel.DataAnnotations;

namespace Wallet.Web.ViewModels.Accounts
{
    public class AccountUpdateViewModel
    {
        [Required]
        public int Id { get; set; }
        public string? Name { get; set; }
    }
}
