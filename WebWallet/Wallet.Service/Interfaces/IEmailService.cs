﻿
namespace Wallet.Service.Interfaces
{
    public interface IEmailService
    {
        Task<bool> SendEmailAsync(string userEmail, string confirmationLink);
    }
}
