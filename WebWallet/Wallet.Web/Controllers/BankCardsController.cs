﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Wallet.Core.Dtos.BankCard;
using Wallet.Core.Models;
using Wallet.Helpers.Еxceptions;
using Wallet.Service.Interfaces;
using Wallet.Web.Mappers;
using Wallet.Web.ViewModels.BankCards;

namespace Wallet.Web.Controllers
{
    [Authorize]
    public class BankCardsController : Controller
    {
        IBankCardService _bankCardService;
        IUsersService _usersService;
        BankCardMapper _bankCardMapper;
        public BankCardsController(IUsersService usersService, IBankCardService bankCardService, BankCardMapper bankCardMapper)
        {
            this._bankCardService = bankCardService;
            this._usersService = usersService;
            this._bankCardMapper = bankCardMapper;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            User user = await this._usersService.GetByUserNameAsync(User.Identity.Name);
            user = await this._usersService.GetByIdWithBankCardsAsync(user.Id);
            UserWithCardsViewModel viewModel = this._bankCardMapper.Map(user.BankCards);

            return View(viewModel);
        }


        [HttpPost]
        public async Task<IActionResult> Delete(int bankCardId)
        {
            try
            {
                User user = await this._usersService.GetByUserNameAsync(User.Identity.Name);
                user = await this._usersService.GetByIdWithBankCardsAsync(user.Id);
                await this._bankCardService.DeleteAsync(bankCardId, user);

                return RedirectToAction("Index");
            }
            catch (UnauthorizedOperationException ex)
            {
                return RedirectToAction("Index", "Error", new { id = 401, message = ex.Message });
            }
        }

        [HttpPost]
        [Route("BankCards/UpdateAsync")]
        public async Task<IActionResult> UpdateAsync(BankCardUpdateDto cardUpdates)
        {
            try
            {
                User user = await this._usersService.GetByUserNameAsync(User.Identity.Name);
                BankCard existingCard = await this._bankCardService.UpdateAsync(cardUpdates, user);

                return RedirectToAction("Index");
            }
            catch (ArgumentException ex)
            {
                return RedirectToAction("Index", "Error", new { id = 400, message = ex.Message });
            }
            catch (UnauthorizedOperationException ex)
            {
                return RedirectToAction("Index", "Error", new { id = 401, message = ex.Message });
            }
        }

        [HttpPost]
        [Route("BankCards/CreateAsync")]
        public async Task<IActionResult> CreateAsync(CreateCardViewModel cardModel)
        {
            try
            {
                User user = await this._usersService.GetByUserNameAsync(User.Identity.Name);
                BankCard cardToCreate = this._bankCardMapper.Map(cardModel, user.Id);
                await this._bankCardService.CreateAsync(cardToCreate, user);

                return RedirectToAction("Index");
            }
            catch (InvalidOperationException ex)
            {
                return RedirectToAction("Index", "Error", new { id = 400, message = ex.Message });
            }
            catch (ArgumentException ex)
            {
                return RedirectToAction("Index", "Error", new { id = 400, message = ex.Message });
            }
            catch (UnauthorizedOperationException ex)
            {
                return RedirectToAction("Index", "Error", new { id = 401, message = ex.Message });
            }
        }
    }
}
