﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wallet.Core.Models
{
    public class Transaction
    {
        public const decimal Same_Currency_Exchange_Rate = 1.00m;
        public const decimal Default_Target_Amount = 0.00m;
        public Transaction()
        {
            CreatedAt = DateTime.Now;
        }

        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }

        [Required(ErrorMessage = "SenderUserId is required. (Message from Transaction-Class)")]
        public int SenderUserId { get; set; }
        public User SenderUser { get; set; }

        [Required(ErrorMessage = "SenderWalletAccountId is required. (Message from Transaction-Class)")]
        public int SenderAccountId { get; set; }
        public Account SenderAccount { get; set; }

        [Required(ErrorMessage = "RecepientUserId is required. (Message from Transaction-Class)")]
        public int RecepientUserId { get; set; }
        public User RecepientUser { get; set; }

        [Required(ErrorMessage = "RecepientWalletAccountId is required. (Message from Transaction-Class)")]
        public int RecepientAccountId { get; set; }
        public Account RecepientAccount { get; set; }

        [Range(0.01, (double)decimal.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2} (Message from Transaction-Class)")]
        public decimal TargetAmount { get; set; }

        [Required(ErrorMessage = "TargetCurrencyId is required. (Message from Transaction-Class)")]
        public string TargetCurrencyId { get; set; }
        public Currency TargetCurrency { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description is required (Message from Transaction-Class)")]
        [MaxLength(256, ErrorMessage = "The '{0}' field must be less than {1} characters. (Message from Transaction-Class)")]
        [MinLength(8, ErrorMessage = "The '{0}' field must be at least {1} character. (Message from Transaction-Class)")]
        public string Description { get; set; }

        //public string? Receipt { get; set; }


        [Required(ErrorMessage = "OriginCurrencyId is required. (Message from Transaction-Class)")]
        public string OriginCurrencyId { get; set; }
        public Currency OriginCurrency { get; set; }

        [Range(0.01, (double)decimal.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2} (Message from Transaction-Class)")]
        public decimal OriginAmount { get; set; }

        [Required(ErrorMessage = "{0} is required. (Message from Transaction-Class)")]
        public decimal ExchangeRate { get; set; }

        public bool IsInternal { get; set; }


    }
}
