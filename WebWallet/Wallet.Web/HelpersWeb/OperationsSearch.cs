﻿using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Core.Enums;
using Wallet.Web.ViewModels;

namespace Wallet.Web.HelpersWeb
{
    public static class OperationsSearch
    {
        private const string DescendingOrderLabel = "desc";
        public static async Task<PaginatedList<OperationsViewModel>> FilterBy(List<OperationsViewModel> operations, OperationQueryParameters filterParams = null)
        {

            var result = operations;
            var sortBy = filterParams.SortBy == null ? default : Enum.Parse<SortingBy>(filterParams.SortBy.ToLower());
            if (!string.IsNullOrEmpty(filterParams.Keyword))
            {
                var filteredByType = FilterByType(operations, filterParams.Keyword);
                var filteredByFrom = FilterByFrom(operations, filterParams.Keyword);
                var filteredByTo = FilterByTo(operations, filterParams.Keyword);
                var filteredByDescription = FilterByDescription(operations, filterParams.Keyword);
                var filteredByOriginAmount = FilterByOriginAmountAsString(operations, filterParams.Keyword);
                var filteredByTargetAmount = FilterByTargetAmountAsString(operations, filterParams.Keyword);
                result = filteredByType.Union(filteredByFrom).Union(filteredByTo).Union(filteredByDescription).Union(filteredByOriginAmount).Union(filteredByTargetAmount).ToList();

                result = SortBy(result, sortBy);
                SortOrder(result, filterParams.SortOrder);
            }
            else
            {
                result = SortBy(result, sortBy);
                SortOrder(result, filterParams.SortOrder);
            }

            int totalNumber = result.Count;
            int totalPages = totalNumber / filterParams.PageSize;
            totalPages = result.Count() % filterParams.PageSize == 0 ? totalPages : ++totalPages;

            result = Paginate(result, filterParams.PageNumber, filterParams.PageSize);

            return new PaginatedList<OperationsViewModel>(result, totalPages, filterParams.PageNumber, totalNumber);
        }
        private static List<OperationsViewModel> Paginate(List<OperationsViewModel> result, int pageNumber, int pageSize)
        {
            if (pageSize == -1)
            {
                return result;
            }
            
            return result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize).ToList();
        }

        private static List<OperationsViewModel> FilterByType(List<OperationsViewModel> operations, string type)
        {
            if (!string.IsNullOrEmpty(type))
            {
                return operations.Where(op => op.Type.Contains(type, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
            
            return operations;
        }

        private static List<OperationsViewModel> FilterByFrom(List<OperationsViewModel> operations, string from)
        {
            if (!string.IsNullOrEmpty(from))
            {
                return operations.Where(op => op.From.Contains(from, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
          
            return operations;
        }

        private static List<OperationsViewModel> FilterByTo(List<OperationsViewModel> operations, string to)
        {
            if (!string.IsNullOrEmpty(to))
            {
                return operations.Where(op => op.To.Contains(to, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
           
            return operations;
        }

        private static List<OperationsViewModel> FilterByDescription(List<OperationsViewModel> operations, string description)
        {
            if (!string.IsNullOrEmpty(description))
            {
                return operations.Where(op => op.Description.Contains(description, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
            
            return operations;
        }

        private static List<OperationsViewModel> FilterByOriginAmountAsString(List<OperationsViewModel> operations, string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
            {
                return operations.Where(op => op.OriginAmount.Contains(keyword, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
            
            return operations;
        }

        private static List<OperationsViewModel> FilterByTargetAmountAsString(List<OperationsViewModel> operations, string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
            {
                return operations.Where(op => op.TargetAmount.Contains(keyword, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
           
            return operations;
        }

        private static List<OperationsViewModel> SortBy(List<OperationsViewModel> operations, SortingBy sortby)
        {
            switch (sortby)
            {
                case SortingBy.type:
                    return operations.OrderBy(op => op.Type).ToList();
                case SortingBy.from:
                    return operations.OrderBy(op => op.From).ToList();
                case SortingBy.to:
                    return operations.OrderBy(op => op.To).ToList();
                case SortingBy.origin:
                    return operations.OrderBy(op => op.OriginAmountAsNumber).ToList();
                case SortingBy.target:
                    return operations.OrderBy(op => op.TargetAmountAsNumber).ToList();
                default:
                    return operations.OrderBy(op => op.CreatedAt).ToList();
            }
        }

        private static void SortOrder(List<OperationsViewModel> operations, string sortOrder)
        {
            if (!string.IsNullOrEmpty(sortOrder) && sortOrder == DescendingOrderLabel)
            {
                operations.Reverse();
            }
        }
    }
}
