﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Core.CustomAttributes;

namespace Wallet.Core.Dtos
{
    public class TransactionInternalCreateDto
    {        
        [Range(1, int.MaxValue, ErrorMessage = "Value for {0} must be provided")]
        public int SenderAccountId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Value for {0} must be provided")]
        public int RecepientAccountId { get; set; }


        [Range(0.01, 1000000.00, ErrorMessage = "Value for {0} must be between {1} and {2}")]       
        [DecimalPrecision(2)]
        public decimal TargetAmount { get; set; }      
    }


}
