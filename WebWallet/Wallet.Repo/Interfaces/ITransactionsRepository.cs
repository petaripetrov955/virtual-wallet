﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Web.QueryParameters;

namespace Wallet.Repo.Interfaces
{
    public interface ITransactionsRepository
    {
        Task<Transaction> GetTransactionByIdAsync(int id);
        Task<List<Transaction>> GetAllTransactionsAsync();
        Task<List<Transaction>> GetTransactionsBySenderOrRecepientUserIdAsync(int userId);
        Task<Transaction> CreateTransactionAsync(Transaction transactionToCreate);
        Task<List<Transaction>> FilterByAsync(TransactionQueryParameters filterParams, User user, bool IsAdmin);


    }
}
