﻿using Wallet.Core.Models;
using Wallet.Core.QueryParameters;

namespace Wallet.Repo.Interfaces
{
    public interface IAccountRepository
    {
        Task<ICollection<Account>> GetAllAsync();
        Task<Account> CreateAsync(Account accountToAdd);
        Task<Account> DeleteAsync(Account accountToDelete);
        Task<Account> UpdateAsync(Account accountToUpdate, string nameUpdate);
        Task<Account> GetAsync(int id);
        Task<PaginatedList<Account>> SearchByAsync(AccountQueryParameters filterParameters, bool isAdmin, User user);
    }
}
