﻿namespace Wallet.Core.Enums
{
    public enum SortingBy
    {
        none,
        type,
        from,
        to,
        origin,
        target,
        balance,
        currencyId,
    }
}
