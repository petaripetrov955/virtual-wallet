﻿using System.ComponentModel.DataAnnotations;
using Wallet.Core;
using Wallet.Core.Enums;

using static Wallet.Core.Models.Account;
using static Wallet.Core.Models.BankCard;
using static Wallet.Core.Models.Currency;

namespace Wallet.Web.ViewModels.BankCards
{
    public class CreateCardViewModel
    {
        [Required]
        [RegularExpression(Name_Regex_Pattern, ErrorMessage = ErrorMessages.InvalidAccountNameErrorMessage)]
        public string Name { get; set; }

        [Required]
        [RegularExpression(CardNumberPattern, ErrorMessage = InvalidCardNumberErrorMessage)]
        public string CardNumber { get; set; }
        [Required]
        public DateTime ValidThrough { get; set; }

        [Required]
        [RegularExpression(HolderPattern, ErrorMessage = InvalidHolderNameErrorMessage)]
        [MaxLength(28)]
        public string HolderName { get; set; }

        [Required]
        [RegularExpression(PinPattern, ErrorMessage = InvalidPinErrorMessage)]
        public string PIN { get; set; }

        [Required]
        [RegularExpression(CvvPattern, ErrorMessage = InvalidCvvErrorMessage)]
        public string CVV { get; set; }

        [Required]
        [EnumDataType(typeof(CardType))]
        public CardType CardType { get; set; }

        [Required]
        [RegularExpression(CurrencyPattern, ErrorMessage = ErrorMessages.InvalidCurrencyIdErrorMessage)]
        [MaxLength(3)]
        public string CurrencyId { get; set; }
    }
}
