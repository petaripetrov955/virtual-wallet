﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;

namespace Wallet.Service.Interfaces
{
    public interface ITransfersService
    {
        Task<List<Transfer>> GetAllTransfersAsync();
        Task<Transfer> GetTransferByIdAsync(int id, User user);
        Task<List<Transfer>> GetTransfersByUserIdAsync(int userId);
        Task<Transfer> CreateTransferAsync(Transfer transferToCreate, User user);
        bool IsBalanceSufficientForTransfer(Transfer transfer, Account account);
        Task<List<Transfer>> FilterByAsync(User user, bool IsAdmin, TransferQueryParameters filterParams = null);
    }
}
