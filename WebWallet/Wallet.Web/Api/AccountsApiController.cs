﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Wallet.Core;
using Wallet.Core.Dtos.Account;
using Wallet.Core.Models;
using Wallet.Service.Interfaces;
using Wallet.Web.Filters;
using Wallet.Web.Mappers;

namespace Wallet.Web.Api
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = $"{Constants.Roles.AdminRoleName},{Constants.Roles.UserRoleName}")]
    [CustomExceptionRESTApiFilter]
    [Route("api/accounts")]
    [ApiController]
    public class AccountsApiController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IUsersService _usersService;
        private readonly AccountMapper _accountMapper;
        public AccountsApiController(IAccountService accountService, IUsersService usersService, AccountMapper accountMapper)
        {
            _accountService = accountService;
            _accountMapper = accountMapper;
            _usersService = usersService;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
                User user = await this._usersService.GetByUserNameAsync(User.Identity.Name);
                Account account = await this._accountService.GetByIdAsync(id, user);
                AccountOutputDto accountOutputDto = this._accountMapper.Map(account);

                return Ok(accountOutputDto);
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
                User user = await this._usersService.GetByUserNameAsync(User.Identity.Name);
                _= await this._accountService.DeleteAsync(id, user);

                return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] AccountCreateDto inputData)
        {
                User user = await this._usersService.GetByUsernameWithAccountsAsync(User.Identity.Name);
                inputData.UserId = user.Id;
                Account accountToCreate = this._accountMapper.Map(inputData);
                Account createdAccount = await this._accountService.CreateAsync(accountToCreate, user);
                AccountOutputDto accountOutputDto = this._accountMapper.Map(createdAccount);

                return Ok(accountOutputDto);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] AccountUpdateDto accountUpdates)
        {
                User user = await this._usersService.GetByUserNameAsync(User.Identity.Name);
                Account updatedAccount = await this._accountService.UpdateAsync(accountUpdates, user);
                AccountOutputDto accountOutputDto = this._accountMapper.Map(updatedAccount);

                return Ok(accountOutputDto);
        }
    }
}
