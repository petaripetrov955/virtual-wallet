﻿using Wallet.Core.Dtos.BankCard;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;

namespace Wallet.Repo.Interfaces
{
    public interface IBankCardRepository
    {
        Task<ICollection<BankCard>> GetAllAsync(BankCardQueryParameters parameters);
        Task<BankCard> GetByIdAsync(int id);
        Task<BankCard> CreateAsync(BankCard cardToAdd);
        Task<BankCard> DeleteAsync(BankCard cardToDelete);
        Task<BankCard> UpdateAsync(BankCard card, BankCardUpdateDto cardUpdates);
    }
}
