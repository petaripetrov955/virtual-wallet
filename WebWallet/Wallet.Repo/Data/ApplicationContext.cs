﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Wallet.Core.Models;
using Wallet.Repo.Data.EntityConfigurations;
using Wallet.Repo.Data.Seed;

namespace Wallet.Repo.Data
{
	public class ApplicationContext : IdentityDbContext<User, Role, int>
	{
		public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
		{
		}

		public DbSet<BankCard>  BankCards { get; set; }
		public DbSet<Transaction> Transactions { get; set; }
		public DbSet<Transfer> Transfers { get; set; }
		public DbSet<Currency> Currencies { get; set; }
		public DbSet<Account> Accounts { get; set; }	

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.ApplyConfiguration(new UserEntityConfiguration());
            modelBuilder.ApplyConfiguration(new CurrencyEntityConfiguration());
            modelBuilder.ApplyConfiguration(new BankCardEntityConfiguration());
            modelBuilder.ApplyConfiguration(new AccountEntityConfiguration());
			modelBuilder.ApplyConfiguration(new TransactionEntityConfiguration());


            modelBuilder.SeedRoles();
			modelBuilder.SeedUsers();
			modelBuilder.SeedCurrencies();
			modelBuilder.SeedBankCards();
			modelBuilder.SeedAccounts();
			modelBuilder.SeedTransfers();
			modelBuilder.SeedTransactions();
		}
	}
}
