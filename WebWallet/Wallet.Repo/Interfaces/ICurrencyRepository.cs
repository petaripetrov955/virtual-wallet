﻿using Wallet.Core.Models;

namespace Wallet.Repo.Interfaces
{
    public interface ICurrencyRepository
    {
        Task<Currency> GetByIdAsync(string currencyId);
        Task<ICollection<Currency>> GetAllAsync();
        Task<Currency> CreateAsync(string currencyId, string name);
        Task<Currency> GetByNameAsync(string currencyName);
    }
}
