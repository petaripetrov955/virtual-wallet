﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Wallet.Core;
using Wallet.Helpers.Еxceptions;

namespace Wallet.Web.Filters
{
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            bool HasHandledException = false;

            if (!HasHandledException)
            {
                if (context.Exception is EntityNotFoundException)
                {
                    context.Result = new RedirectToActionResult(Constants.Labels.Index, Constants.Labels.Error, new { id = 404, message = context.Exception.Message });
                }
                else if (context.Exception is UnauthorizedOperationException)
                {
                    context.Result = new RedirectToActionResult(Constants.Labels.Index, Constants.Labels.Error, new { id = 401, message = context.Exception.Message });
                }
                else if (context.Exception is CurrencyBeaconApiException)
                {
                    context.Result = new RedirectToActionResult(Constants.Labels.Index, Constants.Labels.Error, new { id = 503, message = context.Exception.Message });
                }
                else if (context.Exception is ArgumentException || 
                        context.Exception is InvalidOperationException || 
                        context.Exception is InvalidTransactionException || 
                        context.Exception is NonZeroBalanceException ||
                        context.Exception is DuplicateEntityException)
                {
                    context.Result = new RedirectToActionResult(Constants.Labels.Index, Constants.Labels.Error, new { id = 400, message = context.Exception.Message });
                }
                else if (context.Exception is EmailNotConfirmedException)
                {
                    context.Result = new RedirectToActionResult("ConfirmYourUser", "Operations", new { id = 400, message = context.Exception.Message });
                }

                HasHandledException = true;
            }
        }
    }
}
