﻿namespace Wallet.Core.Dtos.Currency
{
    //Due to discrepancies in the target amount in the external api, we switch the from and
    //to currencies in order to get the desired functionality. 
    public class ExternalCurrencyConversionResponseDto 
    {
        //Target
        public string From { get; set; }

        //Origin
        public string To { get; set; }

        public decimal Amount { get; set; }

        public decimal Value { get; set; }
    }
}
