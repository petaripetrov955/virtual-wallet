﻿namespace Wallet.Helpers.Еxceptions
{
    public class EntityNotFoundException : ApplicationException
    {
        private const string DefaultMessage = "Entity has not been found.";

        public EntityNotFoundException() : base(DefaultMessage)
        {
        }
        public EntityNotFoundException(string message) : base(message)
        {
        }
    }
}
