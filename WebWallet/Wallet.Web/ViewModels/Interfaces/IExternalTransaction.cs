﻿namespace Wallet.Web.ViewModels.Interfaces
{
    public interface IExternalTransaction : ITransaction
    {
        int RecepientUserId { get; set; }
        string TargetCurrencyId { get; set; }
        string OriginCurrencyId { get; set; }
    }
}
