﻿using Wallet.Core.Dtos.Currency;
using Wallet.Core.Models;

namespace Wallet.Service.Interfaces
{
    public interface ICurrencyService
    {
        Task<ExternalCurrencyConversionResponseDto> ConvertAsync(string receivingCurrency, string sendingCurrency, decimal targetAmount);
        Task<BaseCurrencyWithRatesDto> GetAllWithRatesAsync(string currencyBase);
        Task<ICollection<Currency>> GetAllAsync();
        Task<BaseCurrencyWithRatesDto> GetByIdWithRate(string baseCurrency, string currencyWithRate);
        Task<Currency> GetByIdAsync(string currencyId);
        Task<Currency> GetByNameAsync(string currencyName);
        Task<Currency> CreateAsync(string currencyId, string name);
    }
}
