﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;

namespace Wallet.Web.ViewModels
{
    public class OperationsViewModel
    {

        public int Id { get; set; }
        public string InternalType { get; set; }

        public string Type { get; set; }
        public string Date { get; set; }
        public string From { get; set; }
        public string OriginAmount { get; set; }
        public string To { get; set; }
        public string TargetAmount { get; set; }
        
        public double OriginAmountAsNumber{ get; set; }
        public double TargetAmountAsNumber { get; set; }
        public DateTime CreatedAt { get; set; }

        public string Description { get; set; }

        
    }
}
