﻿namespace Wallet.Helpers.Еxceptions
{
    public class EmailNotConfirmedException : ApplicationException
    {
        public const string Default_Message = "We can't proceed with the operation as your profile isn't confirmed. Please confirm your profile for full access to features.";

        public EmailNotConfirmedException() : base(Default_Message)
        {
        }

        public EmailNotConfirmedException(string message)
            : base(message)
        {
        }
    }
}
