﻿using Microsoft.EntityFrameworkCore;
using Wallet.Core.Models;
using Wallet.Repo.Data;
using Wallet.Repo.Interfaces;

namespace Wallet.Repo.Repositories
{
    public class CurrencyRepository : ICurrencyRepository
    {
        private readonly ApplicationContext _context;

        public CurrencyRepository(ApplicationContext context)
        {
            _context = context;
        }

        public async Task<Currency> GetByNameAsync(string currencyName)
        {
            Currency currency = await this._context.Currencies.FirstOrDefaultAsync(c => c.Name == currencyName && !c.IsDeleted);

            return currency;
        }
        public async Task<Currency> GetByIdAsync(string currencyId)
        {
            Currency currency = await this._context.Currencies.FirstOrDefaultAsync(c => c.Id == currencyId && !c.IsDeleted);

            return currency;
        }

        public async Task<ICollection<Currency>> GetAllAsync()
        {
            List<Currency> activeCurrencies = await this._context.Currencies.Where(cur => !cur.IsDeleted).ToListAsync();

            return activeCurrencies;
        }
        public async Task<Currency> CreateAsync(string currencyId, string currencyName)
        {
            Currency currency = new Currency { Id = currencyId, Name = currencyName };
            await Task.Run(() => this._context.Currencies.Add(currency));
            await this._context.SaveChangesAsync();

            return currency;
        }
    }
}
