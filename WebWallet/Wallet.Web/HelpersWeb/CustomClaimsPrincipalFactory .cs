﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using Wallet.Core.Models;

namespace Wallet.Web.HelpersWeb
{
    public class CustomClaimsPrincipalFactory : UserClaimsPrincipalFactory<User, Role>
    {
        public CustomClaimsPrincipalFactory(UserManager<User> userManager, RoleManager<Role> roleManager, IOptions<IdentityOptions> options) : base(userManager, roleManager, options)
        {
        }

        public async override Task<ClaimsPrincipal> CreateAsync(User user)
        {
            var principal = await base.CreateAsync(user);

            ((ClaimsIdentity)principal.Identity).AddClaims(new[]
                {
                    new Claim("AvatarURL", user.AvatarURL.ToString()),
                    new Claim("IsBlocked", user.IsBlocked.ToString()),
                    new Claim("EmailConfirmed", user.EmailConfirmed.ToString())
                });

            return principal;
        }
    }
}
