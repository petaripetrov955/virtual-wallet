﻿using System.ComponentModel.DataAnnotations;
using Wallet.Core;

namespace Wallet.Web.ViewModels
{
	public class RegisterViewModel : LoginViewModel
	{
		[MinLength(2, ErrorMessage = ErrorMessages.MinLengthErrorMessage)]
		[MaxLength(32, ErrorMessage = ErrorMessages.MaxLengthErrorMessage)]
		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string FirstName { get; set; }

        [MinLength(2, ErrorMessage = ErrorMessages.MinLengthErrorMessage)]
        [MaxLength(32, ErrorMessage = ErrorMessages.MaxLengthErrorMessage)]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string LastName { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string Email { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		[Compare("Password", ErrorMessage = ErrorMessages.ConfirmPasswordErrorMessage)]
		[RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$", ErrorMessage = ErrorMessages.PasswordValidationErrorMessage)]
		public string ConfirmPassword { get; set; }

		[StringLength(10, ErrorMessage = ErrorMessages.LengthErrorMessage)]
		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string PhoneNumber { get; set; }

        public IFormFile? ImageFile { get; set; }

		public string? AvatarURL { get; set; }
	}
}
