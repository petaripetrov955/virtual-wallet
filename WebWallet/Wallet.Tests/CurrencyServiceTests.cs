﻿using Microsoft.Extensions.Configuration;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System.Net;
using Wallet.Core.Dtos.Currency;
using Wallet.Core.Models;
using Wallet.Helpers.Еxceptions;
using Wallet.Repo.Interfaces;
using Wallet.Service.Services;

namespace Wallet.Tests
{
    [TestClass]
    public class CurrencyServiceTests
    {
        private Mock<ICurrencyRepository> currencyRepositoryMock;
        private Mock<HttpClient> httpClientMock;
        private Mock<IConfiguration> configurationMock;

        private CurrencyService testCurrencyService;

        [TestInitialize]
        public void InitTest()
        {
            currencyRepositoryMock = new Mock<ICurrencyRepository>();
            httpClientMock = new Mock<HttpClient>();
            configurationMock = new Mock<IConfiguration>();

            testCurrencyService = new CurrencyService(currencyRepositoryMock.Object, httpClientMock.Object, configurationMock.Object);
        }
      
        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public async Task GetAllWithRatesAsync_Should_ThrowEntityNotFound_When_InvalidBaseCurrency()
        {
            // Arrange
            string currencyBase = "AAA";

            currencyRepositoryMock
                .Setup(repo => repo.GetByIdAsync(currencyBase))
                .ReturnsAsync((Currency)null);

            configurationMock.Setup(configuration => configuration[It.IsAny<String>()]).Returns("asdf");

            // Act
            await testCurrencyService.GetAllWithRatesAsync(currencyBase);

            // The test should throw EntityNotFoundException
        }

        [TestMethod]
        public async Task GetByIdAsync_Should_ReturnCurrency_When_ValidId()
        {
            // Arrange
            string currencyId = "USD";
            Currency currency = new Currency { Id = currencyId };

            currencyRepositoryMock
                .Setup(repo => repo.GetByIdAsync(currencyId))
                .ReturnsAsync(currency);

            // Act
            Currency actualCurrency = await testCurrencyService.GetByIdAsync(currencyId);

            // Assert
            Assert.AreEqual(currency, actualCurrency);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public async Task GetByIdAsync_Should_ThrowEntityNotFound_When_InvalidId()
        {
            // Arrange
            string currencyId = "AAA";

            currencyRepositoryMock
                .Setup(repo => repo.GetByIdAsync(currencyId))
                .ReturnsAsync((Currency)null);

            // Act
            await testCurrencyService.GetByIdAsync(currencyId);
        }

        [TestMethod]
        public async Task GetByNameAsync_Should_ReturnCurrency_When_ValidName()
        {
            // Arrange
            string currencyName = "US Dollar";
            Currency currency = new Currency { Name = currencyName };

            currencyRepositoryMock
                .Setup(repo => repo.GetByNameAsync(currencyName))
                .ReturnsAsync(currency);

            // Act
            Currency actualCurrency = await testCurrencyService.GetByNameAsync(currencyName);

            // Assert
            Assert.AreEqual(currency, actualCurrency);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public async Task GetByNameAsync_Should_ThrowEntityNotFound_When_InvalidName()
        {
            // Arrange
            string currencyName = "INVALID";

            currencyRepositoryMock
                .Setup(repo => repo.GetByNameAsync(currencyName))
                .ReturnsAsync((Currency)null);

            // Act
            await testCurrencyService.GetByNameAsync(currencyName);
        }

        [TestMethod]
        public async Task CreateAsync_Should_ReturnNewCurrency_When_ValidInput()
        {
            // Arrange
            string currencyId = "EUR";
            string currencyName = "Euro";

            currencyRepositoryMock
                .Setup(repo => repo.GetByIdAsync(currencyId))
                .ReturnsAsync((Currency)null);

            currencyRepositoryMock
                .Setup(repo => repo.GetByNameAsync(currencyName))
                .ReturnsAsync((Currency)null);

            currencyRepositoryMock
                .Setup(repo => repo.CreateAsync(currencyId, currencyName))
                .ReturnsAsync(new Currency { Id = currencyId, Name = currencyName });

            // Act
            Currency createdCurrency = await testCurrencyService.CreateAsync(currencyId, currencyName);

            // Assert
            Assert.IsNotNull(createdCurrency);
            Assert.AreEqual(currencyId, createdCurrency.Id);
            Assert.AreEqual(currencyName, createdCurrency.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(DuplicateEntityException))]
        public async Task CreateAsync_Should_ThrowDuplicateEntity_When_CurrencyIdAlreadyExists()
        {
            // Arrange
            string currencyId = "USD";
            string currencyName = "Dollar";

            currencyRepositoryMock
                .Setup(repo => repo.GetByIdAsync(currencyId))
                .ReturnsAsync(new Currency { Id = currencyId });

            // Act
            await testCurrencyService.CreateAsync(currencyId, currencyName);
        }

        [TestMethod]
        [ExpectedException(typeof(DuplicateEntityException))]
        public async Task CreateAsync_Should_ThrowDuplicateEntity_When_CurrencyNameAlreadyExists()
        {
            // Arrange
            string currencyId = "EUR";
            string currencyName = "Euro";

            currencyRepositoryMock
                .Setup(repo => repo.GetByIdAsync(currencyId))
                .ReturnsAsync((Currency)null);

            currencyRepositoryMock
                .Setup(repo => repo.GetByNameAsync(currencyName))
                .ReturnsAsync(new Currency { Id = "OTHER" });

            // Act
            await testCurrencyService.CreateAsync(currencyId, currencyName);
        }
    }
}
