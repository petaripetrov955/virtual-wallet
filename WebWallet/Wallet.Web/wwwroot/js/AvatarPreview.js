﻿document.getElementById("myFile").onchange = function () {
    var preview = document.getElementById('avatar-preview');
    var file = document.getElementById('myFile').files[0];
    var reader = new FileReader();

    reader.onloadend = function () {
        preview.src = reader.result;
    }

    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }
}