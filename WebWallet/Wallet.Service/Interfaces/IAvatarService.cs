﻿using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;

namespace Wallet.Service.Interfaces
{
    public interface IAvatarService
    {
        string UploadImage(IFormFile imageFile);

        DeletionResult DeleteImage(string OldAvatar);
    }
}
