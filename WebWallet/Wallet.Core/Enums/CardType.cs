﻿namespace Wallet.Core.Enums
{
    public enum CardType
    {
        Mastercard,
        Maestro,
        Visa,
        VisaElectron,
    }
}
