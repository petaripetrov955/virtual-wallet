﻿using Microsoft.AspNetCore.Identity;

namespace Wallet.Core
{
	public static class Errors
	{
		public static readonly IdentityError UserNameAlreadyExists 
			= new IdentityError 
				{ 
					Code = "UserNameAlreadyExists", 
					Description = "User with same user name already exists." 
				};
		public static readonly IdentityError PhoneNumerAlreadyExists 
			= new IdentityError 
				{ 
					Code = "PhoneNumerAlreadyExists", 
					Description = "User with same phone number already exists." 
				};

	}
}
