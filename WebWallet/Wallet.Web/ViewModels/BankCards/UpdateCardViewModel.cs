﻿using System.ComponentModel.DataAnnotations;

namespace Wallet.Web.ViewModels.BankCards
{
    public class UpdateCardViewModel
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? PIN { get; set; }
    }
}
