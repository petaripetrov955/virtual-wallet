﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wallet.Core.Dtos
{
    public class TransactionDisplayDto
    {
        public int Id { get; set; } 
        public string CreatedAt { get; set; }
        public int SenderUserId { get; set; }
        public int SenderAccountId { get; set; }
        public int RecepientUserId { get; set; }
        public int RecepientAccountId { get; set; }
        public decimal OriginAmount { get; set; }
        public string OriginCurrencyId { get; set; }
        public decimal TargetAmount { get; set; }
        public string TargetCurrencyId { get; set; }
        public decimal ExchangeRate { get; set; }
        public string Description { get; set;}
        public bool IsInternal { get; set; }

    }
}
