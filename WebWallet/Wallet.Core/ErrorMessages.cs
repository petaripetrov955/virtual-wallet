﻿using Microsoft.AspNetCore.Identity;

namespace Wallet.Core
{
    public static class ErrorMessages
    {
        public const string EmptyStringErrorMessage = "The {0} field is required and must not be an empty string.";
        public const string MinLengthErrorMessage = "The {0} must be at least {1} characters long.";
        public const string LengthErrorMessage = "The {0} must be {1} characters long.";
        public const string MaxLengthErrorMessage = "The {0} must be no more than {1} characters long.";

        public const string LoginErrorMessage = "Invalid login attempt.";
        public const string UserLockedErrorMessage = "User account locked out.";
        public const string UserIdNotFoundErrorMessage = "User with id: {0} does not exist.";
        public const string UserNameNotFoundErrorMessage = "User with name: {0} does not exist.";
        public const string UserProfileErrorMessage = "You cannot view another user's profile.";
        public const string UserProfileEditErrorMessage = "You cannot edit another user's profile.";
        public const string DuplicateUserNameErrorMessage = "User with the same user name: {0} already exists.";
        public const string DuplicatePhoneNumberErrorMessage = "User with the same phone number: {0} already exists.";
        public const string ModifyUserErrorMessage = "Only admin can modify an user.";
        public const string BlockUserErrorMessage = "You cannot block yourself.";
        public const string UnblockUserErrorMessage = "You cannot unblock yourself.";
        public const string DeleteUserErrorMessage = "You cannot delete your profile.";
        public const string DeleteUserAccountErrorMessage = "Cannot delete user. The user's account balance should be zero.";

        public const string ConfirmPasswordErrorMessage = "The new password and confirmation password do not match.";
        public const string PasswordValidationErrorMessage = "Password must be at least 8 symbols and should contain capital letter, lowercase letter, digit and special symbol.";
        public const string PasswordChangeErrorMessage = "You cannot change another user's password.";

        public const string ImageFormatErrorMessage = "Only JPEG and PNG images are allowed.";
        public const string ImageSizeErrorMessage = "Images can be no larger than 4 MB!";
        public const string ImageUploadErrorMessage = "Failed to upload image.";


        public const string TransferNotExistsErrorMessage = "Transfer does not exist. Invalid id: ";
        public const string TransferCreateErrorMessage = "There was an unexpected problem on creating this operation.";
        public const string TransferNotValidMessage = "You do not have enough in your Account for this operation";

        public const string TransactionNotExistsErrorMessage = "Transaction does not exist. Invalid id: ";
        public const string TransactionCreateErrorMessage = "There was an unexpected problem on creating this operation.";
		public const string TransactionNotValidMessage = "You do not have enough in your Account for this operation";
		public const string TransactionUnauthorizedMessage = "You are not authorized for this operation!";
		
		public const string OperationBlockedUserMessage = "You are blocked and are not authorized for this operation!";
        public const string OperationSearchNotAuthorizedMessage = "You are not authorized to view the operations of another user!";
        public const string OperationsNoAuthenticatedMessage = "Authentication Error. Log in first!";


        public const string NotSupportedCurrencyErrorMessage = "Sorry, the currency code '{0}' is not supported in our system.";
        public const string AccountWithTheSameCurrencyAlreadySetErrorMessage = "An account with the same currency has been already setted up for the user.";
        public const string DuplicatedAccountNameErrorMessage = "The update request has been canceled. The name provided is already assigned to the account.";
        public const string InvalidAccountNameErrorMessage = "The 'Name' field must be at least 1 character long and contain only alphabetical letters.";

        public const string ExternalCurrencyApiErrorMessage = "Service Unavailable. CurrencyBeacon API services are currently unavailable.";
        public const string InvalidCurrencyIdErrorMessage = "Invalid Currency ID. The currency ID must be exactly 3 letters long and consist of capital letters A-Z only.";
        public const string EmptyCollectionErrorMessage = "Operation cannot be performed as the {0} collection is empty.";
        public const string UserIsNotOwnerErrorMessage = "You do not have the necessary authorization to access the {0}. Please ensure you are the owner of the {1} before attempting to proceed.";
        public const string BlockedUserErrorMessage = "Apologies, but we're unable to proceed with the operation at this time as one of the affected profiles is temporarily blocked.";
        
        public const string InvalidOperation = "{0} is not a valid operation.";
        public const string InactiveBankCardOrAccountMessage = "Inactive Bank Card or Account";
        public const string NotEnoughFundsMessage = "The amount in your account is less than {0} {1}. Missing {2} {3}";
        public const string AccountIdUserIdMismatchMessage = "There is a mismatch between account holder and user ID. Please check both sender and recipient.";
    }
}
