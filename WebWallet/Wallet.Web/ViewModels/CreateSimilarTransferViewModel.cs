﻿using System.ComponentModel.DataAnnotations;
using Wallet.Core.CustomAttributes;
using Wallet.Web.ViewModels.Interfaces;

namespace Wallet.Web.ViewModels
{
    public class CreateSimilarTransferViewModel : ICreateTransferModel
    {

        [Range(1, int.MaxValue, ErrorMessage = "{0} must be provided.")]
        public int AccountId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "{0} must be provided.")]
        public int UserId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "{0} must be provided.")]
        public int BankCardId { get; set; }

        [Range(0.01, 1000000.00, ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [DecimalPrecision(2)]
        public decimal Amount { get; set; }

        public bool IsIncoming { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is required.")]
        [MaxLength(256, ErrorMessage = "The '{0}' field must be less than {1} characters.")]
        [MinLength(8, ErrorMessage = "The '{0}' field must be at least {1} characters.")]
        public string Description { get; set; }

        public decimal CurrentBalance { get; set; }

        public string Type { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string CurrencyId { get; set; }

        public bool IsConfirmed { get; set; }
    }
}
