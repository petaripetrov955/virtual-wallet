﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wallet.Core.Dtos
{
    public class TransferDispalyDto
    {
        public int Id { get; set; }

        public string CreatedAt { get; set; }
        
        public int UserId { get; set; }
        
        public int AccountId { get; set; }
        
        public int BankCardId { get; set; }
        
        public decimal Amount { get; set; }

        public bool IsIncoming { get; set; }

        public string Description { get; set; }

        public string CurrencyId { get; set; }
        
        
    }
}
