﻿namespace Wallet.Web.ViewModels
{
    public class OperationDetailsViewModel
    {
        public int Id { get; set; }
        public string InternalType { get; set; }
        public int SenderUserId { get; set; }
        public int RecepeintUserId { get; set; }

        public string Type { get; set; }
        public string Date { get; set; }
        public string From { get; set; }
        public string OriginAmount { get; set; }
        public string To { get; set; }
        public string TargetAmount { get; set; }
        public double? ExchangeRate { get; set; }
        public string Description { get; set; }

        public string? OriginCurrency { get; set; }
        public string? TargetCurrency { get; set; }

        public bool IsCreated { get; set; }
        public bool IsIncoming { get; set; }
        public bool IsInternal { get; set; }
        public decimal CurrentBalance { get; set; }
        public decimal TargetCurrentBalance { get; set; }

        



    }
}
