﻿using System.ComponentModel.DataAnnotations;

namespace Wallet.Core.Dtos.Currency
{
    public class BaseCurrencyWithRatesDto
    {
        public BaseCurrencyWithRatesDto()
        {
            Rates = new Dictionary<string, decimal>();
        }

        [Required]
        [RegularExpression(Models.Currency.CurrencyPattern, ErrorMessage = ErrorMessages.InvalidCurrencyIdErrorMessage)]
        [MaxLength(3)]
        public string Base { get; set; }
        public Dictionary<string, decimal> Rates { get; set; }
    }
}
