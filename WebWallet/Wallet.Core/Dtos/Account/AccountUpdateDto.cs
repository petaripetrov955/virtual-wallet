﻿using System.ComponentModel.DataAnnotations;

namespace Wallet.Core.Dtos.Account
{
    public class AccountUpdateDto
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [RegularExpression(Models.Account.Name_Regex_Pattern, ErrorMessage = ErrorMessages.InvalidAccountNameErrorMessage)]
        public string Name { get; set; }
    }
}
