﻿using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Wallet.Core.Models;
using Wallet.Helpers;
using Wallet.Repo.Data;
using Wallet.Repo.Interfaces;
using Wallet.Repo.Repositories;
using Wallet.Service;
using Wallet.Service.Interfaces;
using Wallet.Service.Services;
using Wallet.Services;
using Wallet.Web.Mappers;

namespace Wallet.Web
{
    public static class ServicesConfiguration
    {
        public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration configuration) =>
       services.AddDbContext<ApplicationContext>(
           options =>
           {
               options.UseSqlServer(configuration.GetConnectionString("sqlConnection"),
               b => b.MigrationsAssembly("Wallet.Repo"));
               options.EnableSensitiveDataLogging();
           });

		public static void ConfigureIdentity(this IServiceCollection services)
        {
            var builder = services.AddIdentity<User, Role>(options =>
            {
                options.Password.RequiredLength = 8;
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireNonAlphanumeric = true;

                options.User.RequireUniqueEmail = true;
				options.Tokens.EmailConfirmationTokenProvider = "emailconfirmation";

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;
            })
            .AddEntityFrameworkStores<ApplicationContext>()
            .AddDefaultTokenProviders()
            .AddTokenProvider<EmailConfirmationTokenProvider<User>>("emailconfirmation");
        }

        public static void ConfigureAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            var jwtConfiguration = configuration.GetSection("jwtConfig");
            var secretKey = jwtConfiguration["key"];

			services
                .AddAuthentication()
                .AddCookie(options =>
                {
                    options.Cookie.Name = "WebAppIdentityCookie";
                    options.Cookie.HttpOnly = true;

                    options.ExpireTimeSpan = TimeSpan.FromMinutes(5);

                    options.LoginPath = "/Users/Login";
                    options.AccessDeniedPath = "/Error";
                    options.SlidingExpiration = true;

                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ClockSkew = TimeSpan.Zero,
                        ValidIssuer = jwtConfiguration["issuer"],
                        ValidAudience = jwtConfiguration["audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey))
                    };
                });

            services.Configure<SecurityStampValidatorOptions>(options =>
               options.ValidationInterval = TimeSpan.FromMinutes(1));
        }

        public static void ConfigureMapping(this IServiceCollection services)
        {
            services.AddScoped<UserMapper>();
            services.AddSingleton<BankCardMapper>();
            services.AddSingleton<AccountMapper>();
			services.AddSingleton<CurrencyMapper>();
            services.AddScoped<TransferMapper>();
            services.AddScoped<TransactionMapper>();
            services.AddScoped<OperationMapper>();
		}
        public static void RegisterDependencies(this IServiceCollection services)
        {
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<IBankCardRepository, BankCardRepository>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<ITransactionsRepository, TransactionsRepository>();
            services.AddScoped<ITransfersRepository, TransfersRepository>();
            services.AddScoped<ICurrencyRepository, CurrencyRepository>();

            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<IBankCardService, BankCardService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ITransactionsService, TransactionsService>();
            services.AddScoped<ITransfersService, TransfersService>();
            services.AddScoped<ICurrencyService, CurrencyService>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();

            services.AddScoped<HttpClient>();
            services.AddScoped<ApplicationContext>();
			services.AddScoped<IAvatarService, AvatarService>();
			services.AddScoped<IEmailService, EmailService>();
        }
    }
}