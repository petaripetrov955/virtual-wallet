﻿using Wallet.Core.Dtos.Account;
using Wallet.Core.Models;
using Wallet.Web.ViewModels.Accounts;

namespace Wallet.Web.Mappers
{
    public class AccountMapper
    {
        public Account Map(AccountCreateDto accountToCreate)
        {
            return new Account
            {
                Name = accountToCreate.Name,
                UserId = accountToCreate.UserId,
                CurrencyId = accountToCreate.CurrencyId,
            };
        }

        public AccountOutputDto Map(Account account)
        {
            return new AccountOutputDto
            {
                Name = account.Name,
                CurrencyId = account.CurrencyId,
                Balance = Math.Round(account.Balance, 2)
            };
        }

        public Account Map(CreateAccountViewModel viewModel)
        {
            return new Account
            {
                Name = viewModel.Name,
                CurrencyId = viewModel.CurrencyId,
            };
        }

        public AccountDisplayViewModel Map(Account account, AccountDisplayViewModel accountViewModel)
        {
            accountViewModel.Id = account.Id;
            accountViewModel.Name = account.Name;
            accountViewModel.CurrencyId = account.CurrencyId;
            accountViewModel.Balance = account.Balance;

            return accountViewModel;
        }
        public UserWithAccountsViewModel Map(ICollection<Account> accounts)
        {
            UserWithAccountsViewModel viewModel = new UserWithAccountsViewModel();

            foreach (var account in accounts)
            {
                AccountDisplayViewModel accountDisplayViewModel = new AccountDisplayViewModel();
                AccountDisplayViewModel mappedAccountViewModel = this.Map(account, accountDisplayViewModel);
                viewModel.Accounts.Add(mappedAccountViewModel);
            }

            return viewModel;
        }
    }
}
