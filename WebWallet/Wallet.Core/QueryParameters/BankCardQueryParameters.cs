﻿namespace Wallet.Core.QueryParameters
{
    public class BankCardQueryParameters
    {
        public string? Holder { get; set; }
        public int? UserId { get; set; }
    }
}
