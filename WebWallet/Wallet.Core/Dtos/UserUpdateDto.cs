﻿using System.ComponentModel.DataAnnotations;

namespace Wallet.Core.Dtos
{
	public class UserUpdateDto
	{
		[MinLength(2, ErrorMessage = ErrorMessages.MinLengthErrorMessage)]
		[MaxLength(32, ErrorMessage = ErrorMessages.MaxLengthErrorMessage)]
		public string? FirstName { get; set; }
		public string? LastName { get; set; }
		public string? Email { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string UserName { get; set; }
		public string? PhoneNumber { get; set; }
		public string? AvatarURL { get; set; }
	}
}
