﻿using Wallet.Core.Enums;

namespace Wallet.Web.ViewModels.BankCards
{
    public class UserWithCardsViewModel
    {
        public UserWithCardsViewModel()
        {
            this.Cards = new List<CardDisplayViewModel>();
        }
        public ICollection<CardDisplayViewModel> Cards { get; set; }
    }
}
