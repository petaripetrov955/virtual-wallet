﻿namespace Wallet.Web.ViewModels.Interfaces
{
    public interface ICreateTransferModel
    {
        int UserId { get; set; }
        int AccountId { get; set; }
        int BankCardId { get; set; }
        decimal Amount { get; set; }
        bool IsIncoming { get; set; }
        string Description { get; set; }
        string CurrencyId { get; set; }
    }
}
