﻿namespace Wallet.Helpers.Еxceptions
{
    public class NonZeroBalanceException : ApplicationException
    {
        private const string DefaultMessage = "Account balance should be zero in order to be deleted.";

        public NonZeroBalanceException() : base(DefaultMessage)
        {
        }
        public NonZeroBalanceException(string message) : base(message)
        {
        }
    }
}
