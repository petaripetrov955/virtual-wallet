﻿using Microsoft.EntityFrameworkCore;
using Wallet.Core.Enums;
using Wallet.Core.Models;
using Wallet.Helpers;

namespace Wallet.Repo.Data.Seed
{
    static class BankCardModelBuilderExtension
    {
        public static void SeedBankCards(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BankCard>().HasData
                 (
                new List<BankCard>
                {
                    new BankCard
                    {
                        Id = 1, 
                        CardNumber = HashingUtility.HashData("5543707370373089"),
                        CardNumberEnding ="3089",
                          CardType = CardType.Mastercard,
                          ValidThrough = new DateTime(2024,1,30),
                          CurrencyId = "BGN",
                           PIN = HashingUtility.HashData("1918"),
                           CVV = HashingUtility.HashData("424"),
                           UserId = 7,
                           Name = "BGN Master Card",
                           HolderName = "IVA IVANOVA"
                    },
                    new BankCard
                    {
                        Id = 2,
                         CardNumber = HashingUtility.HashData("5510216437108842"),
                         CardNumberEnding ="8842",
                          CardType = CardType.Mastercard,
                          ValidThrough = new DateTime(2023,3,30),
                          CurrencyId = "GBP",
                           PIN = HashingUtility.HashData("1111"),
                           CVV = HashingUtility.HashData("424"),
                           UserId = 6,
                           Name = "GBP Credit Card",
                           HolderName = "MARTIN MARTINOV"
                    },

                    new BankCard
                    {
                         Id = 3,
                         CardNumber = HashingUtility.HashData("4716305970177"),
                         CardNumberEnding ="0177",
                          CardType = CardType.Visa,
                          ValidThrough = new DateTime(2023,3,30),
                          CurrencyId = "EUR",
                           PIN = HashingUtility.HashData("0000"),
                           CVV = HashingUtility.HashData("424"),
                           UserId = 3,
                           Name = "EUR Visa",
                           HolderName = "GEORGI GEORGIEV"
                    },

                    new BankCard
                    {
                           Id = 4,
                         CardNumber = HashingUtility.HashData("4556172700878561"),
                         CardNumberEnding ="8561",
                          CardType = CardType.Visa,
                          ValidThrough = new DateTime(2023,12,30),
                          CurrencyId = "USD",
                           PIN = HashingUtility.HashData("9999"),
                           CVV = HashingUtility.HashData("424"),
                           UserId = 2,
                           Name = "USD Visa Card",
                           HolderName = "DIMITER DIMITROV"
                    },

                    new BankCard
                    {
                        Id = 5,
                        CardNumber = HashingUtility.HashData("4485949590374272"),
                        CardNumberEnding ="4272",
                        CardType = CardType.VisaElectron,
                        ValidThrough = new DateTime(2023,10,15),
                        CurrencyId = "AUD",
                        PIN = HashingUtility.HashData("9999"),
                        CVV = HashingUtility.HashData("424"),
                        UserId = 5,
                        Name = "AUD Visa Electron",
                        HolderName = "ALEXANDER ALEXANDROV"
                    },

                    new BankCard
                    {
                        Id = 6,
                        CardNumber = HashingUtility.HashData("1543707370373089"),
                        CardNumberEnding ="3089",
                        CardType = CardType.Visa,
                        ValidThrough = new DateTime(2024,1,30),
                        CurrencyId = "USD",
                        PIN = HashingUtility.HashData("1918"),
                        CVV = HashingUtility.HashData("424"),
                        UserId = 7,
                        Name = "USD Visa",
                        HolderName = "IVA IVANOVA"
                    },

                    new BankCard
                    {
                        Id = 7,
                        CardNumber = HashingUtility.HashData("1543707370371452"),
                        CardNumberEnding ="1452",
                        CardType = CardType.VisaElectron,
                        ValidThrough = new DateTime(2024,1,30),
                        CurrencyId = "RUB",
                        PIN = HashingUtility.HashData("1910"),
                        CVV = HashingUtility.HashData("620"),
                        UserId = 7,
                        Name = "RUB Visa Electron",
                        HolderName = "IVA IVANOVA"
                    }

               });
        }
    }
}
