﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Wallet.Core;
using Wallet.Core.Dtos;
using Wallet.Core.Models;
using Wallet.Helpers.Еxceptions;
using Wallet.Service.Interfaces;

namespace Wallet.Service.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private const string Invalid_Login_Credentials = "Invalid Login Credentials.";
        private const string Invalid_Token = "Invalid access or refresh token.";

        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;

        public AuthenticationService(UserManager<User> userManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _configuration = configuration;
        }

        public async Task<User> ValidateUserAsync(UserLoginDto loginDto)
        {
            User user = await this._userManager.FindByNameAsync(loginDto.Username);

            bool result = user != null && await this._userManager.CheckPasswordAsync(user, loginDto.Password);

            if (!result)
            {
                throw new UnauthorizedOperationException(Invalid_Login_Credentials);
            }

            return user;
        }

        public async Task<string> CreateTokenAsync(User user)
        {
            var signingCredentials = GetSigningCredentials();
            var claims = await GetClaimsAsync(user);
            var tokenOptions = GenerateTokenOptions(signingCredentials, claims);

            return new JwtSecurityTokenHandler().WriteToken(tokenOptions);
        }

        public async Task<string> GenerateRefreshTokenAsync(User user)
        {
            var randomNumber = new byte[64];
            using var rng = RandomNumberGenerator.Create();
            rng.GetBytes(randomNumber);
            string token = Convert.ToBase64String(randomNumber);
            var jwtConfiguration = _configuration.GetSection(Constants.Token.JWT_Config);
            int refreshTokenValidityInMinutes = int.Parse(jwtConfiguration[Constants.Token.Refresh_Token_Validity_In_Minutes_Label]);
            user.RefreshToken = token;
            user.RefreshTokenExpiryTime = DateTime.Now.AddMinutes(refreshTokenValidityInMinutes);
            await _userManager.UpdateAsync(user);

            return token;
        }

        public async Task<TokenModel> RefreshTokenAsync(TokenModel tokenModel)
        {
            _ = tokenModel ?? throw new ArgumentException(Invalid_Token);
            string? accessToken = tokenModel.AccessToken;
            string? refreshToken = tokenModel.RefreshToken;
            var principal = GetPrincipalFromExpiredToken(accessToken);
            _ = principal ?? throw new ArgumentException(Invalid_Token);
            string username = principal.Identity.Name;
            var user = await _userManager.FindByNameAsync(username);
            if (user == null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.Now)
            {
                throw new ArgumentException(Invalid_Token);
            }

            var newAccessToken = await CreateTokenAsync(user);
            var newRefreshToken = await GenerateRefreshTokenAsync(user);
            user.RefreshToken = newRefreshToken;
            await _userManager.UpdateAsync(user);

            return new TokenModel
            {
                AccessToken = newAccessToken,
                RefreshToken = newRefreshToken
            };
        }
        private SigningCredentials GetSigningCredentials()
        {
            var jwtConfig = _configuration.GetSection(Constants.Token.JWT_Config);
            var key = Encoding.UTF8.GetBytes(jwtConfig[Constants.Token.Key]);
            var secret = new SymmetricSecurityKey(key);

            return new SigningCredentials(secret, SecurityAlgorithms.HmacSha256);
        }

        private async Task<List<Claim>> GetClaimsAsync(User user)
        {
            var claims = new List<Claim>
        {
            new Claim(ClaimTypes.Name, user.UserName),
        };
            var roles = await _userManager.GetRolesAsync(user);
            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            return claims;
        }

        private JwtSecurityToken GenerateTokenOptions(SigningCredentials signingCredentials, List<Claim> claims)
        {
            var jwtSettings = _configuration.GetSection(Constants.Token.JWT_Config);
            var tokenOptions = new JwtSecurityToken
            (
            issuer: jwtSettings[Constants.Token.Issuer],
            audience: jwtSettings[Constants.Token.Audience],
            claims: claims,
            expires: DateTime.Now.AddMinutes(Convert.ToDouble(jwtSettings[Constants.Token.Token_Validity_In_Minutes_Label])),
            signingCredentials: signingCredentials
            );

            return tokenOptions;
        }

        private ClaimsPrincipal? GetPrincipalFromExpiredToken(string? token)
        {
            var jwtConfig = _configuration.GetSection(Constants.Token.JWT_Config);
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtConfig[Constants.Token.Key])),
                ValidateLifetime = true
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
            if (securityToken is not JwtSecurityToken jwtSecurityToken || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException(Invalid_Token);

            return principal;
        }
    }
}
