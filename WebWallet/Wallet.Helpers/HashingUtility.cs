﻿using System.Security.Cryptography;
using System.Text;

namespace Wallet.Helpers
{
    public static class HashingUtility
    {
        public static string HashData(string data)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] bytes = Encoding.UTF8.GetBytes(data);
                byte[] hash = sha256.ComputeHash(bytes);

                return Convert.ToBase64String(hash);
            }
        }
        public static bool VerifyHashedData(string data, string storedHash)
        {
            string computedHash = HashData(data);

            return string.Equals(computedHash, storedHash);
        }
    }
}
