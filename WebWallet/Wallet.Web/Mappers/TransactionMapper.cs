﻿using Wallet.Core;
using Wallet.Core.Dtos;
using Wallet.Core.Dtos.Currency;
using Wallet.Core.Models;
using Wallet.Web.ViewModels.Interfaces;

namespace Wallet.Web.Mappers
{
    public class TransactionMapper
    {
        private const int Display_Amount_Decimal_Precision = 2;
        public TransactionExternalCreateDto TransactionExternalCreateDtoMap<T>(T viewModel) where T : IExternalTransaction
        {
            return new TransactionExternalCreateDto
            {
                SenderAccountId = viewModel.SenderAccountId,
                RecepientUserId = viewModel.RecepientUserId,
                TargetAmount = viewModel.TargetAmount,
                TargetCurrencyId = viewModel.TargetCurrencyId,
                Description = viewModel.Description
            };
        }
        public TransactionInternalCreateDto TransactionInternalCreateDtoMap<T>(T viewModel) where T : ITransaction
        {
            return new TransactionInternalCreateDto
            {
                SenderAccountId = viewModel.SenderAccountId,
                RecepientAccountId = viewModel.RecepientAccountId,
                TargetAmount = viewModel.TargetAmount
            };
        }

        public Transaction TransactionInternalCreateMap(TransactionInternalCreateDto internalCreateDto, ExternalCurrencyConversionResponseDto exchangeDto, int userId)
        {

            Transaction transaction
                = new Transaction
                {
                    SenderUserId = userId,
                    SenderAccountId = internalCreateDto.SenderAccountId,
                    RecepientUserId = userId,
                    RecepientAccountId = internalCreateDto.RecepientAccountId,
                    TargetAmount = Math.Round(internalCreateDto.TargetAmount, Display_Amount_Decimal_Precision),
                    TargetCurrencyId = exchangeDto.From,
                    Description = String.Format(Constants.Operations.Own_Wallets_Exchange_From_To, exchangeDto.To, exchangeDto.From),
                    OriginCurrencyId = exchangeDto.To,
                    OriginAmount = Math.Round(exchangeDto.Value, Display_Amount_Decimal_Precision),
                    ExchangeRate = Math.Round((internalCreateDto.TargetAmount / exchangeDto.Value), Constants.Formats.Decimal_Point_Precision),
                    IsInternal = true
                };

            return transaction;
        }

        public Transaction TransactionExternalWithExchangeCreateMap(TransactionExternalCreateDto transactionDto, ExternalCurrencyConversionResponseDto exchangeDto, int userId, int recepientAccountId)
        {
            Transaction transaction
                = new Transaction
                {
                    SenderUserId = userId,
                    SenderAccountId = transactionDto.SenderAccountId,
                    RecepientUserId = transactionDto.RecepientUserId,
                    RecepientAccountId = recepientAccountId,
                    TargetAmount = Math.Round(transactionDto.TargetAmount, Display_Amount_Decimal_Precision),
                    TargetCurrencyId = exchangeDto.From,
                    Description = transactionDto.Description,
                    OriginCurrencyId = exchangeDto.To,
                    OriginAmount = Math.Round(exchangeDto.Value, Display_Amount_Decimal_Precision),
                    ExchangeRate = Math.Round((transactionDto.TargetAmount / exchangeDto.Value), Constants.Formats.Decimal_Point_Precision)
                };

            return transaction;
        }

        public Transaction TransactionExternalNoExchangeCreateMap(TransactionExternalCreateDto transactionDto, int recepientAccountId, int userId)
        {

            Transaction transaction
                = new Transaction
                {
                    SenderUserId = userId,
                    SenderAccountId = transactionDto.SenderAccountId,
                    RecepientUserId = transactionDto.RecepientUserId,
                    RecepientAccountId = recepientAccountId,
                    TargetAmount = Math.Round(transactionDto.TargetAmount, Display_Amount_Decimal_Precision),
                    TargetCurrencyId = transactionDto.TargetCurrencyId,
                    Description = transactionDto.Description,
                    OriginCurrencyId = transactionDto.TargetCurrencyId,
                    OriginAmount = Math.Round(transactionDto.TargetAmount, Display_Amount_Decimal_Precision),
                    ExchangeRate = Transaction.Same_Currency_Exchange_Rate,
                };

            return transaction;
        }

        public TransactionDisplayDto TransactionDislayMap(Transaction transaction)
        {
            TransactionDisplayDto dto
                = new TransactionDisplayDto
                {
                    Id = transaction.Id,
                    CreatedAt = transaction.CreatedAt.ToString(Constants.Formats.CreatedAtDateTime),
                    SenderUserId = transaction.SenderUserId,
                    SenderAccountId = transaction.SenderAccountId,
                    RecepientUserId = transaction.RecepientUserId,
                    RecepientAccountId = transaction.RecepientAccountId,
                    OriginAmount = transaction.OriginAmount,
                    OriginCurrencyId = transaction.OriginCurrencyId,
                    TargetAmount = transaction.TargetAmount,
                    TargetCurrencyId = transaction.TargetCurrencyId,
                    ExchangeRate = transaction.ExchangeRate,
                    Description = transaction.Description,
                    IsInternal = transaction.IsInternal
                };

            return dto;
        }
    }
}
