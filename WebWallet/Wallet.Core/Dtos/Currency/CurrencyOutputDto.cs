﻿namespace Wallet.Core.Dtos.Currency
{
    public class CurrencyOutputDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
