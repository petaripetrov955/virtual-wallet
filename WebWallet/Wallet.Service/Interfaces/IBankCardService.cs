﻿using Wallet.Core.Dtos.BankCard;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;

namespace Wallet.Service.Interfaces
{
    public interface IBankCardService
    {
        Task<ICollection<BankCard>> GetAllAsync(BankCardQueryParameters parameters, User user);
        Task<BankCard> GetByIdAsync(int id, User user);
        Task<BankCard> CreateAsync(BankCard cardToCreate, User user);
        Task<BankCard> DeleteAsync(int id, User user);
        Task<BankCard> UpdateAsync(BankCardUpdateDto cardUpdates, User user);
    }
}
