﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;

namespace Wallet.Repo.Interfaces
{
    public interface ITransfersRepository
    {
        
        Task<List<Transfer>> GetAllTransfersAsync();
        Task<List<Transfer>> GetTransfersByUserIdAsync(int userId);
        Task<Transfer> GetTransferByIdAsync(int id);
        Task<Transfer> CreateTransferAsync(Transfer transferToCreate);
        Task<List<Transfer>> FilterByAsync(TransferQueryParameters filterParams, User user, bool IsAdmin);
    }
}
