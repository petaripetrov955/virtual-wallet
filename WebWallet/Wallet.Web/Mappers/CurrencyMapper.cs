﻿using Wallet.Core.Dtos.Currency;
using Wallet.Core.Models;

namespace Wallet.Web.Mappers
{
    public class CurrencyMapper
    {
        public CurrencyConversionResultDto Map(ExternalCurrencyConversionResponseDto externalResponse)
        {
            return new CurrencyConversionResultDto
            {
                ReceivedAmount = Math.Round(externalResponse.Amount, 4),
                ReceivedCurrency = externalResponse.From,
                SentAmount = Math.Round(externalResponse.Value, 4),
                SentCurrency = externalResponse.To,
            };
        }

        public CurrencyOutputDto Map(Currency currency)
        {
            return new CurrencyOutputDto
            {
                Id = currency.Id,
                Name = currency.Name,
            };
        }
    }
}
