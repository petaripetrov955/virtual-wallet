﻿
namespace Wallet.Helpers.Еxceptions
{
	public class DuplicateEntityException : ApplicationException
	{
        private const string DefaultMessage = "Entity already exists.";

        public DuplicateEntityException() : base(DefaultMessage)
        {
        }

        public DuplicateEntityException(string message)
			: base(message)
		{
		}
	}
}
