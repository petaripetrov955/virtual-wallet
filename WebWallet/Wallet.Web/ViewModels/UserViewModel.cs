﻿using System.ComponentModel.DataAnnotations;
using Wallet.Core;

namespace Wallet.Web.ViewModels
{
	public class UserViewModel
	{
		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		[MinLength(2, ErrorMessage = ErrorMessages.MinLengthErrorMessage)]
		[MaxLength(32, ErrorMessage = ErrorMessages.MaxLengthErrorMessage)]
		public string FirstName { get; set; }
		public string LastName { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string Email { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string UserName { get; set; }

		[StringLength(10, ErrorMessage = ErrorMessages.LengthErrorMessage)]
		public string? PhoneNumber { get; set; }

		public IFormFile? ImageFile { get; set; }

		public string? AvatarURL { get; set; }
	}
}
