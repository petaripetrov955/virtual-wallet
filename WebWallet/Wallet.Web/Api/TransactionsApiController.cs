﻿using CloudinaryDotNet;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography.Xml;
using Wallet.Core;
using Wallet.Core.Dtos;
using Wallet.Core.Dtos.Currency;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Helpers.Еxceptions;
using Wallet.Service.Interfaces;
using Wallet.Service.Services;
using Wallet.Web.Mappers;
using Wallet.Web.QueryParameters;

namespace Wallet.Web.Api
{
    [Route("api/transactions")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TransactionsApiController : ControllerBase
    {
        private readonly ITransactionsService _transactionsService;
        private readonly IUsersService _usersService;
        private readonly IAccountService _accountService;
        private readonly ICurrencyService _currencyService;
        private readonly TransactionMapper _transactionMapper;

        public TransactionsApiController(ITransactionsService transactionsService, IUsersService usersService, IAccountService accountService, ICurrencyService currencyService, TransactionMapper transactionMapper)
        {
            this._transactionsService = transactionsService;
            this._usersService = usersService;
            this._accountService = accountService;
            this._currencyService = currencyService;
            this._transactionMapper = transactionMapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllTransactionsAsync()
        {
            try
            {
                string username = Request.HttpContext.User.Identity.Name;                
                User user = await _usersService.GetByUserNameAsync(username);
                bool IsAdmin = ((List<string>)await _usersService.GetRolesAsync(user)).Contains("Admin");
                List<Transaction> transactions;
                if(IsAdmin)
                {
                    transactions = await _transactionsService.GetAllTransactionsAsync();
                }
                else
                {
                    transactions = await _transactionsService.GetTransactionsBySenderOrRecepientUserIdAsync(user.Id);
                }                
                if (!transactions.Any())
                {
                    return StatusCode(StatusCodes.Status204NoContent, "There are no transactions in the system.");
                }
                List<TransactionDisplayDto> transactionsShown = transactions.Select(tr => _transactionMapper.TransactionDislayMap(tr)).ToList();
                return StatusCode(StatusCodes.Status200OK, transactionsShown);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetAsync([FromRoute] int id)
        {
            try
            {
                string username = Request.HttpContext.User.Identity.Name;                
                User user = await _usersService.GetByUserNameAsync(username);
                bool IsAdmin = ((List<string>)await _usersService.GetRolesAsync(user)).Contains("Admin");
                Transaction transaction = await _transactionsService.GetTransactionByIdAsync(id, user);
                if (!IsAdmin && transaction.SenderUserId != user.Id && transaction.RecepientUserId != user.Id)
                {
                    return StatusCode(StatusCodes.Status401Unauthorized, "You are not authorized to access this Transaction!");
                }
                TransactionDisplayDto transactionShown = _transactionMapper.TransactionDislayMap(transaction);
                return Ok(transactionShown);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("internal")]
        public async Task<IActionResult> CreateInternalAsync([FromBody] TransactionInternalCreateDto internalCreateDto)
        {
            try
            {
                string username = Request.HttpContext.User.Identity.Name;                
                User user = await _usersService.GetByUserNameAsync(username);
                bool IsAdmin = ((List<string>)await _usersService.GetRolesAsync(user)).Contains("Admin");                

                Wallet.Core.Models.Account senderAccount = await _accountService.GetByIdAsync(internalCreateDto.SenderAccountId);
                Wallet.Core.Models.Account recepientAccount = await _accountService.GetByIdAsync(internalCreateDto.RecepientAccountId);
                if (user.IsBlocked)
                {
                    return StatusCode(StatusCodes.Status401Unauthorized, ErrorMessages.OperationBlockedUserMessage);
                }
                if (senderAccount.UserId != user.Id || recepientAccount.UserId != user.Id)
                {
                    return StatusCode(StatusCodes.Status401Unauthorized, ErrorMessages.TransactionUnauthorizedMessage);
                }                

                if (internalCreateDto.SenderAccountId.Equals(internalCreateDto.RecepientAccountId))
                {
                    return StatusCode(StatusCodes.Status406NotAcceptable, $"Please select two different Accounts.");
                }

                string sentCurrencyId = senderAccount.CurrencyId;
                string recievedCurrencyId = recepientAccount.CurrencyId;                
                ExternalCurrencyConversionResponseDto exchangeDto = await _currencyService.ConvertAsync(recievedCurrencyId, sentCurrencyId, internalCreateDto.TargetAmount);
                Transaction transactionToCreate = _transactionMapper.TransactionInternalCreateMap(internalCreateDto, exchangeDto, user.Id);
                //bool IsValidOperation = _transactionsService.IsBalanceSufficientForTransaction(transactionToCreate, senderAccount, recepientAccount);
                //if (!IsValidOperation)
                //{
                //    return StatusCode(StatusCodes.Status406NotAcceptable, ErrorMessages.TransactionNotValidMessage);
                //}

                Transaction createdTransaction = await _transactionsService.CreateTransactionAsync(transactionToCreate, user);
                TransactionDisplayDto transactionShown = _transactionMapper.TransactionDislayMap(createdTransaction);
                return StatusCode(StatusCodes.Status201Created, transactionShown);
            }

            catch (EntityNotFoundException ex)
            {
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
            catch (CurrencyBeaconApiException ex)
            {
                return StatusCode(StatusCodes.Status503ServiceUnavailable, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpPost("external")]
        public async Task<IActionResult> CreateExternalAsync([FromBody] TransactionExternalCreateDto externalCreateDto)
        {
            try
            {
                string username = Request.HttpContext.User.Identity.Name;                
                User senderUser = await _usersService.GetByUserNameAsync(username);
                if (senderUser.IsBlocked)
                {
                    return StatusCode(StatusCodes.Status401Unauthorized, ErrorMessages.OperationBlockedUserMessage);
                }

                Wallet.Core.Models.Account senderAccount = await _accountService.GetByIdAsync(externalCreateDto.SenderAccountId);
                if (senderUser.Id != senderAccount.UserId)
                {
                    return StatusCode(StatusCodes.Status401Unauthorized, ErrorMessages.TransactionUnauthorizedMessage);
                }

                User recepient = await _usersService.GetByIdWithAccountsAsync(externalCreateDto.RecepientUserId);
                Wallet.Core.Models.Account? recepientAccount = recepient.Accounts.Where(acc => acc.CurrencyId == externalCreateDto.TargetCurrencyId).FirstOrDefault();
                if (recepientAccount is null)
                {
                    return StatusCode(StatusCodes.Status406NotAcceptable, $"User '{recepient.UserName}' has no Account in Currency {externalCreateDto.TargetCurrencyId}");
                }
                if (recepientAccount.UserId == senderAccount.UserId)
                {
                    return StatusCode(StatusCodes.Status405MethodNotAllowed, "Params not valid for external transaction. Create an internal transaction ('transactions/internal'), if you want to exchange money between own Accounts.");
                }

                Transaction transactionToCreate;
                if (senderAccount.CurrencyId == recepientAccount.CurrencyId)
                {
                    transactionToCreate = _transactionMapper.TransactionExternalNoExchangeCreateMap(externalCreateDto, recepientAccount.Id, senderUser.Id);
                }
                else
                {
                    ExternalCurrencyConversionResponseDto exchangeDto = await _currencyService.ConvertAsync(recepientAccount.CurrencyId, senderAccount.CurrencyId, externalCreateDto.TargetAmount);
                    transactionToCreate = _transactionMapper.TransactionExternalWithExchangeCreateMap(externalCreateDto, exchangeDto, senderUser.Id, recepientAccount.Id);
                }
                //bool IsValidOperation = _transactionsService.IsBalanceSufficientForTransaction(transactionToCreate, senderAccount);
                //if (!IsValidOperation)
                //{
                //    return StatusCode(StatusCodes.Status406NotAcceptable, ErrorMessages.TransactionNotValidMessage);
                //}

                Transaction createdTransaction = await _transactionsService.CreateTransactionAsync(transactionToCreate, senderUser);
                TransactionDisplayDto transactionShown = _transactionMapper.TransactionDislayMap(createdTransaction);
                return StatusCode(StatusCodes.Status201Created, transactionShown);
            }

            catch (EntityNotFoundException ex)
            {
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
            catch (CurrencyBeaconApiException ex)
            {
                return StatusCode(StatusCodes.Status503ServiceUnavailable, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpGet]
        [Route("search")]
        public async Task<IActionResult> GetFilterBy([FromQuery] TransactionQueryParameters filterParams)
        {            
            try
            {
                string username = Request.HttpContext.User.Identity.Name;                
                User user = await _usersService.GetByUserNameAsync(username);
                bool IsAdmin = ((List<string>)await _usersService.GetRolesAsync(user)).Contains("Admin");               
                List<Transaction> result = await _transactionsService.FilterByAsync( user, IsAdmin, filterParams);
                if (!result.Any())
                {
                    return StatusCode(StatusCodes.Status204NoContent);
                }
                List<TransactionDisplayDto> resultShown = result.Select(trans => _transactionMapper.TransactionDislayMap(trans)).ToList();               
                return StatusCode(StatusCodes.Status200OK, resultShown);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }
    }
}
