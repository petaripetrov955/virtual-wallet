﻿    $(document).ready(function() {

        function handleOperationChange(selectElement) {
            var selectedValue = selectElement.value;

            if (selectedValue !== '-1') {
                selectElement.form.submit(); 
            }
        }

        window.onload = function () {
            var dropdown = document.querySelector('select[name="id"]');
            dropdown.value = '-1'; 
        };



        $('.btn-primary[data-bs-toggle="modal"]').click(function () {
            var accountId = $(this).data('account-id');
            $('#accountIdInput').val(accountId);
        });


    function validateForm() {
        var isValid =
    validateName($('#Name').val()) &&
    validateCurrency($('#CurrencyId').val())

    $('#createButton').prop('disabled', !isValid);
    }

    $('#Name').on('input', function() {
        var Name = $(this).val();
    var isValid = validateName(Name);

    if (isValid) {
        $(this).removeClass('is-invalid').addClass('is-valid');
    $('#NameError').text('');
        } else {
        $(this).removeClass('is-valid').addClass('is-invalid');
    $('#NameError').text('The Name field must start with a capital letter and contain only alphabetical letters.');
        }

    validateForm();
    });

    function validateName(Name) {
        var namePattern = /^[A-Z][a-zA-Z\s]*$/;
    return namePattern.test(Name);
    }


    $('#CurrencyId').on('input', function() {
        var currencyId = $(this).val();
    var isValid = validateCurrency(currencyId);

    if (isValid) {
        $(this).removeClass('is-invalid').addClass('is-valid');
    $('#CurrencyIdError').text('');
        } else {
        $(this).removeClass('is-valid').addClass('is-invalid');
    $('#CurrencyIdError').text('Invalid Currency ID. The currency ID must be exactly 3 letters long and consist of capital letters A-Z only.');
        }

    validateForm();
    });

    function validateCurrency(currencyId) {
        var currencyIdPattern = /^[A-Z]{3}$/;
    return currencyIdPattern.test(currencyId);
    }

    $('.edit-account-button').click(function() {
        var accountId = $(this).data('account-id');
        var editedName = $('#UpdatedName-' + accountId).val();

    $('#editedAccountIdInput').val(accountId);
    $('#editedNameInput').val(editedName);

    validateEditForm(accountId);
    });

    function validateEditForm(accountId) {
        $('#UpdatedName-' + accountId).on('input', function () {
            var editedName = $(this).val();
            var isValid = validateEditedName(editedName);

            if (isValid) {
                $(this).removeClass('is-invalid').addClass('is-valid');
                $('#UpdatedNameError-' + accountId).text('');
            } else {
                $(this).removeClass('is-valid').addClass('is-invalid');
                $('#UpdatedNameError-' + accountId).text('The Name field must start with a capital letter and contain only alphabetical letters.');
            }
        });
    }

    function validateEditedName(editedName) {
        var namePattern = /^[A-Z][a-zA-Z\s]*$/;
    return namePattern.test(editedName);
    }
});
