﻿using System.ComponentModel.DataAnnotations;
using Wallet.Core.CustomAttributes;
using Wallet.Core.Dtos.Account;
using Wallet.Core;
using Wallet.Web.ViewModels.Interfaces;

namespace Wallet.Web.ViewModels
{
    public class PaymentViewModel : IExternalTransaction
    {
        [Range(1, int.MaxValue, ErrorMessage = "Value for {0} must be provided")]
        public int SenderUserId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Value for {0} must be provided")]
        public int RecepientUserId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description is required")]
        public string RecepientUserName { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Value for {0} must be provided")]
        public int SenderAccountId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Value for {0} must be provided")]
        public int RecepientAccountId { get; set; }

        public bool IsInternal { get; set; } = false;

        [Range(0.01, 1000000.00, ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [DecimalPrecision(2)]
        public decimal TargetAmount { get; set; }

        public decimal OriginAmount { get; set; }

        public string OriginCurrencyId { get; set; }
        public string TargetCurrencyId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description is required")]
        [MaxLength(256, ErrorMessage = "The '{0}' field must be less than {1} characters.")]
        [MinLength(8, ErrorMessage = "The '{0}' field must be at least {1} character.")]
        public string Description { get; set; }

        public decimal CurrentExchangeRate { get; set; }


        public decimal CurrentOriginBalance { get; set; }       

        public string? Type { get; set; }
        public string? From { get; set; }
        public string? To { get; set; }

        public bool IsConfirmed { get; set; }

        public decimal CalculatedOrigin { get; set; }


        public List<AccountForOperationDto> SenderAccounts { get; set; } = new();
        public bool HasSenderAccounts { get; set; } = true;

        public List<AccountForOperationDto> RecepientAccounts { get; set; } = new();
        public bool HasRecepientAccounts { get; set; } = true;

        //public string? SenderAccountName { get; set; }
        //public string? RecepientAccountName { get; set; }

    }
}
