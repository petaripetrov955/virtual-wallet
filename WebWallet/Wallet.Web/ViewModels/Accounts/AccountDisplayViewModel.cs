﻿namespace Wallet.Web.ViewModels.Accounts
{
    public class AccountDisplayViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CurrencyId { get; set; }
        public decimal Balance { get; set; }
    }
}
