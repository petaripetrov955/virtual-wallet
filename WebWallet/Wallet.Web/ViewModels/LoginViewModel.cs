﻿using System.ComponentModel.DataAnnotations;
using Wallet.Core;

namespace Wallet.Web.ViewModels
{
	public class LoginViewModel
	{
		[MinLength(2, ErrorMessage = ErrorMessages.MinLengthErrorMessage)]
		[MaxLength(20, ErrorMessage = ErrorMessages.MaxLengthErrorMessage)]
		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string? UserName { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		public string? Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
