﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Repo.Data;
using Wallet.Repo.Interfaces;

namespace Wallet.Repo.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private UserManager<User> _userManager;

        public UsersRepository(UserManager<User> userManager)
        {
            this._userManager = userManager;
        }

        public async Task<List<User>> GetUsersAsync()
        {
            return await _userManager.Users
                                    .Where(u => !u.IsDeleted)
                                    .ToListAsync();
        }

        public async Task<List<User>> GetUsersWithAllTransactionsAsync()
        {
            return await _userManager.Users
                                    .Include(u => u.Transfers)
                                    .Include(u => u.IncomingTransactions)
                                    .Include(u => u.OutgoingTransactions)
                                    .Where(u => !u.IsDeleted)
                                    .ToListAsync();
        }

        public async Task<int> CountAllAsync()
        {
            return await _userManager.Users.Where(u => !u.IsDeleted).CountAsync();
        }

		public async Task<User> GetByIdAsync(int id)
		{
			var user = await _userManager.FindByIdAsync(id.ToString());
			if (user != null && !user.IsDeleted)
			{
				return user;
			}

			return null;
        }

        public async Task<User> GetByIdWithBankCardsAsync(int id)
        {
            return await _userManager.Users
                                    .Include(u => u.BankCards.Where(bc => bc.IsActive))
                                    .Where(u => u.Id == id && !u.IsDeleted)
                                    .FirstOrDefaultAsync();
        }

        public async Task<User> GetByIdWithAccountsAsync(int id)
        {
            return await _userManager.Users
                                    .Include(u => u.Accounts.Where(ac => !ac.IsDeleted))
                                    .Where(u => u.Id == id && !u.IsDeleted)
                                    .FirstOrDefaultAsync();
        }

        public async Task<User> GetByUsernameWithAccountsAsync(string username)
        {
            return await _userManager.Users
                                    .Include(u => u.Accounts.Where(ac => !ac.IsDeleted))
                                    .Where(u => u.UserName == username && !u.IsDeleted)
                                    .FirstOrDefaultAsync();
        }

        public async Task<User> GetByUsernameWithBankCardsAsync(string username)
        {
            return await _userManager.Users
                                    .Include(u => u.BankCards.Where(bc => bc.IsActive))
                                    .Where(u => u.UserName == username && !u.IsDeleted)
                                    .FirstOrDefaultAsync();
        }

        public async Task<User> GetByIdWithAllTransactionsAsync(int id)
        {
            return await _userManager.Users
                                    .Include(u => u.Transfers)
                                    .Include(u => u.IncomingTransactions)
                                    .Include(u => u.OutgoingTransactions)
                                    .Where(u => u.Id == id && !u.IsDeleted)
                                    .FirstOrDefaultAsync();
        }

        public async Task<User> GetByUserNameAsync(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user != null && !user.IsDeleted)
            {
                return user;
            }
            return null;
        }

        public async Task<User> GetByPhoneNumberAsync(string phoneNumber)
        {
            return await _userManager.Users.FirstOrDefaultAsync(u => u.PhoneNumber == phoneNumber && !u.IsDeleted);
        }

        public async Task<IdentityResult> AddToRoleAsync(User user, string role)
        {
            return await _userManager.AddToRoleAsync(user, role);          
        }

        public async Task<IList<string>> GetRolesAsync(User user)
        {
            return await _userManager.GetRolesAsync(user);
        }

		public async Task<bool> IsInRoleAsync(User user, string role)
		{
			return await _userManager.IsInRoleAsync(user, role);
		}

        public async Task<IdentityResult> RegisterAsync(User user, string password)
        {
            var result = await _userManager.CreateAsync(user, password);

            return result;
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(User user)
        {
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            return token;
        }

        public async Task<string> GenerateChangeEmailTokenAsync(User user, string newEmail)
        {
            var token = await _userManager.GenerateChangeEmailTokenAsync(user, newEmail);
            return token;
        }

        public async Task<IdentityResult> ConfirmEmailAsync(User user, string token)
        {
            return await _userManager.ConfirmEmailAsync(user, token);
        }

        public async Task<IdentityResult> ChangeEmailAsync(User user, string newEmail, string token)
        {
            return await _userManager.ChangeEmailAsync(user, newEmail, token);
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<IdentityResult> UpdateAsync(int id, User user)
        {
            User userToUpdate = await _userManager.FindByIdAsync(id.ToString());

            if (!string.IsNullOrEmpty(user.FirstName))
            {
                userToUpdate.FirstName = user.FirstName;
            }

            if (!string.IsNullOrEmpty(user.LastName))
            {
                userToUpdate.LastName = user.LastName;
            }

            if (!string.IsNullOrEmpty(user.PhoneNumber))
            {
                userToUpdate.PhoneNumber = user.PhoneNumber;
            }

            if (!string.IsNullOrEmpty(user.AvatarURL))
            {
                userToUpdate.AvatarURL = user.AvatarURL;
            }

            var result = await _userManager.UpdateAsync(userToUpdate);

            return result;
        }

        public async Task<IdentityResult> ChangePasswordAsync(int id, string currentPassword, string newPassword)
        {
            User userToChange = await _userManager.FindByIdAsync(id.ToString());

            var result = await _userManager.ChangePasswordAsync(userToChange, currentPassword, newPassword);

            return result;
        }

        public async Task<IdentityResult> DeleteAsync(int id)
        {
            var userToDelete = await _userManager.FindByIdAsync(id.ToString());
            userToDelete.IsDeleted = true;

            return await _userManager.UpdateAsync(userToDelete);
        }

        public async Task<IdentityResult> BlockAsync(User user)
        {
            if (!user.IsBlocked)
            {
                user.IsBlocked = true;
            }

            return await _userManager.UpdateAsync(user);
        }

        public async Task<IdentityResult> UnblockAsync(User user)
        {
            if (user.IsBlocked)
            {
                user.IsBlocked = false;
            }

            return await _userManager.UpdateAsync(user);
        }

        public async Task<PaginatedList<User>> SearchByAsync(UserQueryParameters filterParameters)
        {
            IEnumerable<User> result = await _userManager.Users.Where(u => !u.IsDeleted).ToListAsync();

            if (filterParameters is null)
            {
                return (PaginatedList<User>)result.ToList();
            }

            if (!string.IsNullOrEmpty(filterParameters.Keyword))
            {
                var result1 = FilterByFirstName(result, filterParameters.Keyword);
                var result2 = FilterByUserName(result, filterParameters.Keyword);
                var result3 = FilterByEmail(result, filterParameters.Keyword);
                var result4 = FilterByPhoneNumber(result, filterParameters.Keyword);

                var temp = result1.Union(result2);
                temp = temp.Union(result3);
                temp = temp.Union(result4);
                result = temp.ToList();

                result = SortBy(result, filterParameters.SortBy);
                result = SortOrder(result, filterParameters.SortOrder);
            }
            else
            {
                result = FilterByFirstName(result, filterParameters.FirstName);
                result = FilterByUserName(result, filterParameters.UserName);
                result = FilterByEmail(result, filterParameters.Email);
                result = FilterByPhoneNumber(result, filterParameters.PhoneNumber);
                result = SortBy(result, filterParameters.SortBy);
                result = SortOrder(result, filterParameters.SortOrder);
            }

            int itemsCount = result.Count();
            int totalPages = result.Count() / filterParameters.PageSize;
            totalPages = result.Count() % filterParameters.PageSize == 0 ? totalPages : ++totalPages;

            result = Paginate(result, filterParameters.PageNumber, filterParameters.PageSize);

            return new PaginatedList<User>(result.ToList(), totalPages, filterParameters.PageNumber, itemsCount);
        }

        public static IEnumerable<User> Paginate(IEnumerable<User> result, int pageNumber, int pageSize)
        {
            if (pageSize == -1)
            {
                return result;
            }
            return result
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);
        }

        private static IEnumerable<User> FilterByFirstName(IEnumerable<User> users, string firstname)
        {
            if (!string.IsNullOrEmpty(firstname))
            {
                return users.Where(user => user.FirstName.Contains(firstname, StringComparison.InvariantCultureIgnoreCase));
            }

            return users;
        }

        private static IEnumerable<User> FilterByUserName(IEnumerable<User> users, string username)
        {
            if (!string.IsNullOrEmpty(username))
            {
                return users.Where(user => user.UserName.Contains(username, StringComparison.InvariantCultureIgnoreCase));
            }

            return users;
        }

        private static IEnumerable<User> FilterByEmail(IEnumerable<User> users, string email)
        {
            if (!string.IsNullOrEmpty(email))
            {
                return users.Where(user => user.Email.Contains(email, StringComparison.InvariantCultureIgnoreCase));
            }

            return users;
        }

        private static IEnumerable<User> FilterByPhoneNumber(IEnumerable<User> users, string phoneNumber)
        {
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                return users.Where(user => user.PhoneNumber.Contains(phoneNumber, StringComparison.InvariantCultureIgnoreCase));
            }

            return users;
        }

        private static IEnumerable<User> SortBy(IEnumerable<User> users, string sortCriteria)
        {
            switch (sortCriteria)
            {
                case "firstname":
                    return users.OrderBy(user => user.FirstName);
                case "username":
                    return users.OrderBy(user => user.UserName);
                case "email":
                    return users.OrderBy(user => user.Email);
                default:
                    return users.OrderBy(user => user.UserName);
            }
        }

        private static IEnumerable<User> SortOrder(IEnumerable<User> users, string sortOrder)
        {
            return (sortOrder == "desc") ? users.Reverse() : users;
        }
    }
}