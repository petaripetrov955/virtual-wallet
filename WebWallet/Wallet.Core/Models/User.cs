﻿using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace Wallet.Core.Models
{
    public class User : IdentityUser<int>
    {
        public User() : base()
        {
            CreatedAt = DateTime.Now;
            PhoneNumberConfirmed = true;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? AvatarURL { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsDeleted { get; set; }

        public DateTime? RefreshTokenExpiryTime { get; set; }
        public string? RefreshToken { get; set; }

        public string Password
        {
            set
            {
                PasswordHash = (new PasswordHasher<User>()).HashPassword(this, value);
            }
        }

        public ICollection<Account> Accounts { get; set; } = new List<Account>();
        public ICollection<BankCard> BankCards { get; set; } = new List<BankCard>();
        public ICollection<Transaction> IncomingTransactions { get; set; } = new List<Transaction>();
        public ICollection<Transaction> OutgoingTransactions { get; set; } = new List<Transaction>();
        public ICollection<Transfer> Transfers { get; set; } = new List<Transfer>();
    }
}
