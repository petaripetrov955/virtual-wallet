﻿$(document).ready(function () {



    $('.delete-button').on('click', function () {
        var cardId = $(this).siblings('input[name="bankCardId"]').val();
        $('#deleteModal-' + cardId).modal('show');
    });


    function validateForm() {
        var isValid = validateCardNumber($('#CardNumber').val()) &&
            validateName($('#Name').val()) &&
            validateHolderName($('#HolderName').val()) &&
            validatePIN($('#PIN').val()) &&
            validateCVV($('#CVV').val()) &&
            validateCurrency($('#CurrencyId').val())

        $('#createButton').prop('disabled', !isValid);
    }


    $('#CardNumber').on('input', function () {
        var cardNumber = $(this).val();
        var isValid = validateCardNumber(cardNumber);

        if (isValid) {
            $(this).removeClass('is-invalid').addClass('is-valid');
            $('#cardNumberError').text('');
        } else {
            $(this).removeClass('is-valid').addClass('is-invalid');
            $('#cardNumberError').text('The length of the digits must be between 14 and 16.');
        }

        validateForm();
    });

    function validateCardNumber(cardNumber) {
        var cardNumberPattern = /^\d{14,16}$/;
        return cardNumberPattern.test(cardNumber);
    }

    $('#Name').on('input', function () {
        var Name = $(this).val();
        var isValid = validateName(Name);

        if (isValid) {
            $(this).removeClass('is-invalid').addClass('is-valid');
            $('#NameError').text('');
        } else {
            $(this).removeClass('is-valid').addClass('is-invalid');
            $('#NameError').text('The Name field must start with a capital letter and contain only alphabetical letters.');
        }

        validateForm();
    });

    function validateName(Name) {
        var namePattern = /^[A-Z][a-zA-Z\s]*$/;
        return namePattern.test(Name);
    }

    $('#HolderName').on('input', function () {
        var holderName = $(this).val();
        var isValid = validateHolderName(holderName);

        if (isValid) {
            $(this).removeClass('is-invalid').addClass('is-valid');
            $('#holderNameError').text('');
        } else {
            $(this).removeClass('is-valid').addClass('is-invalid');
            $('#holderNameError').text('Please enter a valid first and last name in the format First Last in uppercase letters.');
        }

        validateForm();
    });

    function validateHolderName(holderName) {
        var holderPattern = /^[A-Z]+ [A-Z]+$/;
        return holderPattern.test(holderName);
    }

    $('#PIN').on('input', function () {
        var PIN = $(this).val();
        var isValid = validatePIN(PIN);

        if (isValid) {
            $(this).removeClass('is-invalid').addClass('is-valid');
            $('#PINError').text('');
        } else {
            $(this).removeClass('is-valid').addClass('is-invalid');
            $('#PINError').text('Please enter a 4 to 6-digit numeric PIN for verification.');
        }

        validateForm();
    });

    function validatePIN(PIN) {
        var pinPattern = /^[0-9]{4,6}$/;
        return pinPattern.test(PIN);
    }

    $('#CVV').on('input', function () {
        var CVV = $(this).val();
        var isValid = validateCVV(CVV);

        if (isValid) {
            $(this).removeClass('is-invalid').addClass('is-valid');
            $('#CVVError').text('');
        } else {
            $(this).removeClass('is-valid').addClass('is-invalid');
            $('#CVVError').text('Please enter a 3 or 4-digit numeric CVV for card verification.');
        }

        validateForm();
    });

    function validateCVV(CVV) {
        var cvvPattern = /^[0-9]{3,4}$/;
        return cvvPattern.test(CVV);
    }

    $('#CurrencyId').on('input', function () {
        var currencyId = $(this).val();
        var isValid = validateCurrency(currencyId);

        if (isValid) {
            $(this).removeClass('is-invalid').addClass('is-valid');
            $('#CurrencyIdError').text('');
        } else {
            $(this).removeClass('is-valid').addClass('is-invalid');
            $('#CurrencyIdError').text('Invalid Currency ID. The currency ID must be exactly 3 letters long and consist of capital letters A-Z only.');
        }

        validateForm();
    });

    function validateCurrency(currencyId) {
        var currencyIdPattern = /^[A-Z]{3}$/;
        return currencyIdPattern.test(currencyId);
    }

    $('#ValidThrough').on('change', function () {
        var selectedDate = new Date($(this).val());
        var currentDate = new Date();

        if (selectedDate > currentDate) {
            $(this).removeClass('is-invalid').addClass('is-valid');
            $('#validThroughError').text('');
        } else {
            $(this).removeClass('is-valid').addClass('is-invalid');
            $('#validThroughError').text('Please select a date in the future.');
        }
    });


    $('#UpdateName').on('input', function () {
        var updateName = $(this).val();
        var isValid = validateUpdateName(updateName);

        if (isValid) {
            $(this).removeClass('is-invalid').addClass('is-valid');
            $('#UpdateNameError').text('');
        } else {
            $(this).removeClass('is-valid').addClass('is-invalid');
            $('#UpdateNameError').text('The Name field must start with a capital letter and contain only alphabetical letters.');
        }
    });

    function validateUpdateName(updateName) {
        var updateNamePattern = /^[A-Z][a-zA-Z\s]*$/;
        return updateNamePattern.test(updateName);
    }

    $('#UpdatePIN').on('input', function () {
        var updatePIN = $(this).val();
        var isValid = validateUpdatePIN(updatePIN);

        if (isValid) {
            $(this).removeClass('is-invalid').addClass('is-valid');
            $('#UpdatePINError').text('');
        } else {
            $(this).removeClass('is-valid').addClass('is-invalid');
            $('#UpdatePINError').text('Please enter a 4 to 6-digit numeric PIN for verification.');
        }
    });

    function validateUpdatePIN(updatePIN) {
        var pinUpdatePattern = /^[0-9]{4,6}$/;
        return pinUpdatePattern.test(updatePIN);
    }

});