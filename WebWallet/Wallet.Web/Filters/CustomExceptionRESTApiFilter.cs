﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Wallet.Helpers.Еxceptions;
using Microsoft.IdentityModel.Tokens;

namespace Wallet.Web.Filters
{
    public class CustomExceptionRESTApiFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is EntityNotFoundException)
            {
                context.Result = new NotFoundObjectResult(new
                {
                    StatusCode = 404,
                    Message = context.Exception.Message
                });
            }
            else if (context.Exception is UnauthorizedOperationException ||
                    context.Exception is UnauthenticatedOperationException)
            {
                context.Result = new UnauthorizedObjectResult(new
                {
                    StatusCode = 401,
                    Message = context.Exception.Message
                });
            }
            else if (context.Exception is CurrencyBeaconApiException)
            {
                context.Result = new ObjectResult(new
                {
                    StatusCode = 503,
                    Message = context.Exception.Message
                })
                {
                    StatusCode = 503
                };
            }
            else if (context.Exception is ArgumentException ||
                     context.Exception is SecurityTokenEncryptionFailedException ||
                     context.Exception is InvalidOperationException ||
                     context.Exception is InvalidTransactionException ||
                     context.Exception is NonZeroBalanceException ||
                     context.Exception is DuplicateEntityException)
            {
                context.Result = new BadRequestObjectResult(new
                {
                    StatusCode = 400,
                    Message = context.Exception.Message
                });
            }
        }
    }
}
