﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Wallet.Helpers.Еxceptions;
using Wallet.Service.Interfaces;
using Wallet.Web.ViewModels.Currencies;

namespace Wallet.Web.Controllers
{
    [Authorize]
    public class CurrenciesController : Controller
    {
        ICurrencyService _currencyService;

        public CurrenciesController (ICurrencyService currencyService)
        {
            this._currencyService = currencyService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string currencyBase = "USD")
        {
           var result = await this._currencyService.GetAllWithRatesAsync(currencyBase);
            
            return this.View(result);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(CreateCurrencyViewModel viewModel)
        {
            try
            {
                await this._currencyService.CreateAsync(viewModel.Id, viewModel.Name);

                return RedirectToAction("Index");
            }
            catch (ArgumentException ex)
            {
                return RedirectToAction("Index", "Error", new { id = 400, message = ex.Message });
            }
            catch (DuplicateEntityException ex)
            {
                return RedirectToAction("Index", "Error", new { id = 400, message = ex.Message });
            }
        }
    }
}
