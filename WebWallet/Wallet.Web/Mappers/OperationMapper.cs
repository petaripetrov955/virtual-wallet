﻿
using System.Globalization;
using Wallet.Core;
using Wallet.Core.Dtos.Account;
using Wallet.Core.Dtos.BankCard;
using Wallet.Core.Models;
using Wallet.Web.ViewModels;

using Account = Wallet.Core.Models.Account;

namespace Wallet.Web.Mappers
{
    public class OperationMapper
    {

       



        public List<OperationsViewModel> OperationsMap(List<Transfer> transfers, List<Transaction> transactions, User user, bool IsAdmin)
        {
            List<OperationsViewModel> viewModelList = new();

            foreach (var transfer in transfers)
            {
                OperationsViewModel viewModel = new();
                if (transfer.IsIncoming)
                {
                    viewModel.Type = FormatOperationType(Constants.Operations.Money_Added, IsAdmin, Constants.Labels.User, transfer.User.UserName);
                    viewModel.From = FormatSendingMessage(Constants.Labels.BankCard, transfer.BankCard.Name);
                    viewModel.To = FormatSendingMessage(Constants.Labels.Account, transfer.Account.Name);
                }
                else
                {

                    viewModel.Type = FormatOperationType(Constants.Operations.Withdraw_To_BankCard, IsAdmin, Constants.Labels.User, transfer.User.UserName);
                    viewModel.From = FormatSendingMessage(Constants.Labels.Account, transfer.Account.Name);
                    viewModel.To = FormatSendingMessage(Constants.Labels.BankCard, transfer.BankCard.Name);
                }

                InitializeOperationsViewModel(viewModel, transfer.Id, transfer.GetType().Name, transfer.CreatedAt, transfer.Description);
                viewModel.OriginAmount = FormatCurrencyAmount(Constants.Labels.MinusSign, transfer.Amount, transfer.Currency.Id);
                viewModel.TargetAmount = FormatCurrencyAmount(Constants.Labels.PlusSign, transfer.Amount, transfer.Currency.Id);
                viewModel.OriginAmountAsNumber = (double)transfer.Amount;
                viewModel.TargetAmountAsNumber = (double)transfer.Amount;

                viewModelList.Add(viewModel);
            }

            foreach (var transaction in transactions)
            {
                OperationsViewModel viewModel = new();
                if (transaction.IsInternal)
                {
                    viewModel.Type = FormatOperationType(Constants.Operations.Exchange_In_Accounts, IsAdmin, Constants.Labels.User, transaction.SenderUser.UserName);
                    viewModel.From = FormatSendingMessage(Constants.Labels.Account, transaction.SenderAccount.Name);
                    viewModel.To = FormatSendingMessage(Constants.Labels.Account, transaction.RecepientAccount.Name);
                }
                else
                {
                    if (!IsAdmin)
                    {
                        if (transaction.SenderUserId == user.Id)
                        {
                            viewModel.Type = Constants.Operations.Payment_To_User;
                            viewModel.From = FormatSendingMessage(Constants.Labels.Account, transaction.SenderAccount.Name);
                            viewModel.To = FormatSendingMessage(Constants.Labels.User, transaction.RecepientUser.UserName);
                        }
                        else
                        {
                            viewModel.Type = Constants.Operations.Received_Payment;
                            viewModel.From = FormatSendingMessage(Constants.Labels.User, transaction.SenderUser.UserName);
                            viewModel.To = FormatSendingMessage(Constants.Labels.Account, transaction.RecepientAccount.Name);
                        }
                    }
                    else
                    {
                        viewModel.Type = Constants.Operations.Payment_Between_Users;
                        viewModel.From = $"{FormatSendingMessage(Constants.Labels.User, transaction.SenderUser.UserName)}, {FormatSendingMessage(Constants.Labels.Account, transaction.SenderAccount.Name)}";
                        viewModel.To = $"{FormatSendingMessage(Constants.Labels.User, transaction.RecepientUser.UserName)}, {FormatSendingMessage(Constants.Labels.Account, transaction.RecepientAccount.Name)}";
                    }
                }

                InitializeOperationsViewModel(viewModel, transaction.Id, transaction.GetType().Name, transaction.CreatedAt, transaction.Description);
                viewModel.OriginAmount = FormatCurrencyAmount(Constants.Labels.MinusSign, transaction.OriginAmount, transaction.OriginCurrency.Id);
                viewModel.TargetAmount = FormatCurrencyAmount(Constants.Labels.PlusSign, transaction.TargetAmount, transaction.TargetCurrency.Id);
                viewModel.OriginAmountAsNumber = (double)transaction.OriginAmount;
                viewModel.TargetAmountAsNumber = (double)transaction.TargetAmount;

                viewModelList.Add(viewModel);
            }

            viewModelList.OrderByDescending(vm => vm.Date);

            return viewModelList;
        }

        public OperationDetailsViewModel OperationDetailsTransferMap(Transfer transfer, User user, bool IsAdmin)
        {
            OperationDetailsViewModel viewModel = new();
            if (transfer.IsIncoming)
            {

                viewModel.Type = FormatOperationType(Constants.Operations.Money_Added, IsAdmin, Constants.Labels.User, transfer.User.UserName);
                viewModel.From = FormatSendingMessage(Constants.Labels.BankCard, transfer.BankCard.Name);
                viewModel.To = FormatSendingMessage(Constants.Labels.Account, transfer.Account.Name);

            }
            else
            {
                viewModel.Type = FormatOperationType(Constants.Operations.Withdraw_To_BankCard, IsAdmin, Constants.Labels.User, transfer.User.UserName);
                viewModel.From = FormatSendingMessage(Constants.Labels.Account, transfer.Account.Name);
                viewModel.To = FormatSendingMessage(Constants.Labels.BankCard, transfer.BankCard.Name);
            }

            viewModel.Date = transfer.CreatedAt.ToString(Constants.Formats.CreatedAtDateTime, CultureInfo.CreateSpecificCulture(Constants.Formats.Culture));
            viewModel.OriginAmount = FormatCurrencyAmount(Constants.Labels.MinusSign, transfer.Amount, transfer.Currency.Id);
            viewModel.TargetAmount = FormatCurrencyAmount(Constants.Labels.PlusSign, transfer.Amount, transfer.Currency.Id);
            viewModel.OriginCurrency = viewModel.TargetCurrency = transfer.CurrencyId;
            viewModel.Description = transfer.Description;

            viewModel.InternalType = transfer.GetType().Name;
            viewModel.Id = transfer.Id;
            viewModel.SenderUserId = transfer.UserId;

            return viewModel;
        }

        public OperationDetailsViewModel OperationDetailsTransactionMap(Transaction transaction, User user, bool IsAdmin)
        {
            OperationDetailsViewModel viewModel = new();
            if (transaction.IsInternal)
            {
                viewModel.Type = FormatOperationType(Constants.Operations.Exchange_In_Own_Accounts, IsAdmin, Constants.Labels.User, transaction.SenderUser.UserName);
                viewModel.From = FormatSendingMessage(Constants.Labels.Account, transaction.SenderAccount.Name);
                viewModel.To = FormatSendingMessage(Constants.Labels.Account, transaction.RecepientAccount.Name);
            }
            else
            {
                if (!IsAdmin)
                {
                    if (transaction.SenderUserId == user.Id)
                    {

                        viewModel.Type = Constants.Operations.Payment_To_User;
                        viewModel.From = FormatSendingMessage(Constants.Labels.Account, transaction.SenderAccount.Name);
                        viewModel.To = FormatSendingMessage(Constants.Labels.User, transaction.RecepientUser.UserName);
                    }
                    else
                    {
                        viewModel.Type = Constants.Operations.Received_Payment;
                        viewModel.From = FormatSendingMessage(Constants.Labels.User, transaction.SenderUser.UserName);
                        viewModel.To = FormatSendingMessage(Constants.Labels.Account, transaction.RecepientAccount.Name);
                    }
                }
                else
                {
                    viewModel.Type = Constants.Operations.Payment_Between_Users;
                    viewModel.From = $"{FormatSendingMessage(Constants.Labels.User, transaction.SenderUser.UserName)}, {FormatSendingMessage(Constants.Labels.Account, transaction.SenderAccount.Name)}";
                    viewModel.To = $"{FormatSendingMessage(Constants.Labels.User, transaction.RecepientUser.UserName)}, {FormatSendingMessage(Constants.Labels.Account, transaction.RecepientAccount.Name)}";
                }
            }
            
            viewModel.Date = transaction.CreatedAt.ToString(Constants.Formats.CreatedAtDateTime, CultureInfo.CreateSpecificCulture(Constants.Formats.Culture));
            viewModel.OriginAmount = FormatCurrencyAmount(Constants.Labels.MinusSign, transaction.OriginAmount, transaction.OriginCurrency.Id);
            viewModel.TargetAmount = FormatCurrencyAmount(Constants.Labels.PlusSign, transaction.TargetAmount, transaction.TargetCurrency.Id);
            viewModel.Description = transaction.Description;

            viewModel.InternalType = transaction.GetType().Name;
            viewModel.Id = transaction.Id;
            viewModel.SenderUserId = transaction.SenderUserId;
            viewModel.RecepeintUserId = transaction.RecepientUserId;

            if (transaction.SenderAccount.CurrencyId != transaction.RecepientAccount.CurrencyId)
            {
                viewModel.ExchangeRate = (double)transaction.ExchangeRate;
            }

            viewModel.OriginCurrency = transaction.OriginCurrencyId;
            viewModel.TargetCurrency = transaction.TargetCurrencyId;

            return viewModel;
        }

        public CreateSimilarTransferViewModel SimilarTransferCreateMap(Transfer transfer)
        {
            CreateSimilarTransferViewModel viewModel = new();
            viewModel.AccountId = transfer.AccountId;
            viewModel.BankCardId = transfer.BankCardId;
            viewModel.Amount = transfer.Amount;
            viewModel.IsIncoming = transfer.IsIncoming;

            if (transfer.IsIncoming)
            {

                viewModel.Type = Constants.Operations.Add_Money;
                viewModel.From = FormatSendingMessage(Constants.Labels.BankCard, transfer.BankCard.Name);
                viewModel.To = FormatSendingMessage(Constants.Labels.Account, transfer.Account.Name);
            }
            else
            {
                viewModel.Type = Constants.Operations.Withdraw_To_BankCard;
                viewModel.From = FormatSendingMessage(Constants.Labels.Account, transfer.Account.Name);
                viewModel.To = FormatSendingMessage(Constants.Labels.BankCard, transfer.BankCard.Name);
            }

            viewModel.CurrencyId = transfer.BankCard.CurrencyId;
            viewModel.Description = transfer.Description;
            viewModel.UserId = transfer.UserId;

            return viewModel;
        }

        public CreateSimilarTransactionViewModel SimilarTransactionCreateMap(Transaction transaction)
        {
            CreateSimilarTransactionViewModel viewModel = new();
            viewModel.SenderUserId = transaction.SenderUserId;
            viewModel.RecepientUserId = transaction.RecepientUserId;
            viewModel.SenderAccountId = transaction.SenderAccountId;
            viewModel.RecepientAccountId = transaction.RecepientAccountId;
            viewModel.IsInternal = transaction.IsInternal;

            viewModel.TargetAmount = transaction.TargetAmount;

            if (transaction.IsInternal)
            {
                viewModel.Type = Constants.Operations.Exchange_In_Own_Accounts;
                viewModel.From = FormatSendingMessage(Constants.Labels.Account, transaction.SenderAccount.Name);
                viewModel.To = FormatSendingMessage(Constants.Labels.Account, transaction.RecepientAccount.Name);
            }
            else
            {
                viewModel.Type = Constants.Operations.Pay_To_User;
                viewModel.From = FormatSendingMessage(Constants.Labels.Account, transaction.SenderAccount.Name);
                viewModel.To = FormatSendingMessage(Constants.Labels.User, transaction.RecepientUser.UserName);
            }

            viewModel.Description = transaction.Description;
            viewModel.OriginCurrencyId = transaction.OriginCurrencyId;
            viewModel.TargetCurrencyId = transaction.TargetCurrencyId;

            return viewModel;
        }

        public CreateTransferViewModel TransferViewModelCollectionsMap(ICollection<BankCard> bankCards, ICollection<Account> accounts, CreateTransferViewModel viewModel)
        {

            foreach (BankCard bankCard in bankCards)
            {
                BankCardForOperationDto dto = new BankCardForOperationDto()
                {
                    Id = bankCard.Id,
                    Name = bankCard.Name,
                    CurrencyId = bankCard.CurrencyId
                };
                viewModel.BankCards.Add(dto);
            }

            foreach (Account account in accounts)
            {
                AccountForOperationDto dto = new AccountForOperationDto()
                {
                    Id = account.Id,
                    Name = account.Name,
                    CurrencyId = account.CurrencyId,
                    Balance = account.Balance
                };
                viewModel.Accounts.Add(dto);
            }
            return viewModel;
        }

        public ExchangeViewModel TransactionExchangeViewModelCreate(ICollection<Account> accounts, ExchangeViewModel viewModel)
        {

            foreach (Account account in accounts)
            {
                AccountForOperationDto dto = new AccountForOperationDto()
                {
                    Id = account.Id,
                    Name = account.Name,
                    CurrencyId = account.CurrencyId,
                    Balance = account.Balance
                };
                viewModel.Accounts.Add(dto);
            }
            return viewModel;
        }

        public PaymentViewModel TransactionPaymentViewModelCreate(ICollection<Account> senderAccounts, ICollection<Account> recepientAccounts, PaymentViewModel viewModel)
        {
            foreach (Account account in senderAccounts)
            {
                AccountForOperationDto dto = new AccountForOperationDto()
                {
                    Id = account.Id,
                    Name = account.Name,
                    CurrencyId = account.CurrencyId,
                    Balance = account.Balance
                };
                viewModel.SenderAccounts.Add(dto);
            }

            foreach (Account account in recepientAccounts)
            {
                AccountForOperationDto dto = new AccountForOperationDto()
                {
                    Id = account.Id,
                    Name = account.Name,
                    CurrencyId = account.CurrencyId,
                    Balance = account.Balance
                };

                viewModel.RecepientAccounts.Add(dto);
            }
            return viewModel;
        }

        private string FormatSendingMessage(string source, string accountName)
        {
            return $"{source} {accountName}";
        }
        private string FormatCurrencyAmount(string sign, decimal amount, string currencyId)
        {
            return $"{sign}{amount.ToString(CultureInfo.CreateSpecificCulture(Constants.Formats.Culture))} {currencyId}";
        }

        private string FormatOperationType(string operationType, bool isAdmin, string userLabel, string userName)
        {
            if (isAdmin)
            {
                return $"{operationType}; {userLabel} {userName}";
            }

            return operationType;
        }
        private void InitializeOperationsViewModel(OperationsViewModel viewModel, int id, string internalType, DateTime createdAt, string description)
        {
            viewModel.Id = id;
            viewModel.InternalType = internalType;
            viewModel.Date = createdAt.ToString(Constants.Formats.CreatedAtDateTime, CultureInfo.CreateSpecificCulture(Constants.Formats.Culture));
            viewModel.CreatedAt = createdAt;
            viewModel.Description = description;
        }
    }
}
