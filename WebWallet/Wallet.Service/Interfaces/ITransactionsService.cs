﻿using Wallet.Core.Models;
using Wallet.Web.QueryParameters;

namespace Wallet.Service.Interfaces
{
    public interface ITransactionsService
    {
        Task<List<Transaction>> GetAllTransactionsAsync();
        Task<List<Transaction>> GetTransactionsBySenderOrRecepientUserIdAsync(int userId);
        Task<Transaction> GetTransactionByIdAsync(int id, User user);
        Task<Transaction> CreateTransactionAsync(Transaction transactionToCreate, User user);
        bool IsBalanceSufficientForTransaction(Transaction transaction, Account originAccount);
        Task<List<Transaction>> FilterByAsync(User user, bool IsAdmin, TransactionQueryParameters filterParams = null);
        void ThrowErrorIfSenderMatchesRecepient(User sender, User recepient);

    }
}
