﻿using Wallet.Core.Dtos;
using Wallet.Core.Models;
using Wallet.Web.ViewModels;
using Wallet.Web.ViewModels.Interfaces;

namespace Wallet.Web.Mappers
{
    public class TransferMapper
    {
        public Transfer TransferCreateMap<T>(T  viewModel) where T : ICreateTransferModel
        {
            return new Transfer
            {
                UserId = viewModel.UserId,
                AccountId = viewModel.AccountId,
                BankCardId = viewModel.BankCardId,
                Amount = Math.Round(viewModel.Amount, 2),
                Description = viewModel.Description,
                IsIncoming = viewModel.IsIncoming,
                CurrencyId = viewModel.CurrencyId
            };
        }

        public TransferDispalyDto TransferDislayMap(Transfer transfer)
        {

            TransferDispalyDto dto
                = new TransferDispalyDto
                {
                    Id = transfer.Id,
                    CreatedAt = transfer.CreatedAt.ToString("dddd, dd MMMM yyyy HH:mm:ss"),
                    UserId = transfer.UserId,
                    AccountId = transfer.AccountId,
                    BankCardId = transfer.BankCardId,
                    Amount = transfer.Amount,
                    IsIncoming = transfer.IsIncoming,
                    Description = transfer.Description,
                    CurrencyId = transfer.CurrencyId
                };

            return dto;
        }

        public TransferFromAccountViewModel TransferFromAccountViewModelCreateMap(User user, Account account, BankCard card, bool isIncoming)
        {
            return new TransferFromAccountViewModel
            {
                UserId = user.Id,
                AccountId = account.Id,
                AccountName = account.Name,
                CurrencyId = account.CurrencyId,
                AccountBalance = account.Balance,
                BankCardId = card.Id,
                BankCardName = card.Name,
                IsIncoming = isIncoming
            };
        }
    }
}
