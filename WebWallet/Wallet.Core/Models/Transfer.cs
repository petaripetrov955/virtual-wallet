﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wallet.Core.Models
{
    public class Transfer
    {
        public const decimal Default_Target_Amount = 0.00m;
        public Transfer()
        {
            CreatedAt = DateTime.Now;
        }


        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }

        [Required(ErrorMessage = "UserId is required. (Message from Transfer-Class)")]
        public int UserId { get; set; }
        public User User { get; set; }

        [Required(ErrorMessage = "WalletAcountId is required. (Message from Transfer-Class)")]
        public int AccountId { get; set; }
        public Account Account { get; set; }

        [Required(ErrorMessage = "BankCardId is required. (Message from Transfer-Class)")]        
        public int BankCardId { get; set; }
        public BankCard BankCard { get; set; }

        [Range(0.01, (double)decimal.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2} (Message from Transfer-Class)")]
        public decimal Amount { get; set; }

        public bool IsIncoming { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description is required (Message from Transfer-Class)")]
        [MaxLength(256, ErrorMessage = "The '{0}' field must be less than {1} characters. (Message from Transfer-Class)")]
        [MinLength(8, ErrorMessage = "The '{0}' field must be at least {1} character. (Message from Transfer-Class)")]
        public string Description { get; set; }

        [Required(ErrorMessage = "CurrencyId is required. (Message from Transfer-Class)")]
        public string CurrencyId { get; set; }
        public Currency Currency { get; set; }
        

    }
}
