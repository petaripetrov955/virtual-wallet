﻿using Microsoft.EntityFrameworkCore;
using Wallet.Core.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Wallet.Repo.Data.EntityConfigurations
{
    internal class CurrencyEntityConfiguration : IEntityTypeConfiguration<Currency>
    {
        public void Configure(EntityTypeBuilder<Currency> builder)
        {
            builder.HasMany(c => c.IncomingTransactions)
                   .WithOne(transaction => transaction.OriginCurrency)
                   .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(c => c.OutgoingTransactions)
                  .WithOne(transaction => transaction.TargetCurrency)
                  .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(c => c.Transfers)
                  .WithOne(transfer => transfer.Currency)
                  .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(c => c.Accounts)
                .WithOne(a => a.Currency)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
