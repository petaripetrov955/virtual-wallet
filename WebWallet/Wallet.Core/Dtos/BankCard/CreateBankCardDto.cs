﻿using System.ComponentModel.DataAnnotations;
using Wallet.Core.Enums;

namespace Wallet.Core.Dtos.BankCard
{
    public class CreateBankCardDto
    {
        [Required]
        [RegularExpression(Models.Account.Name_Regex_Pattern, ErrorMessage = ErrorMessages.InvalidAccountNameErrorMessage)]
        public string Name { get; set; }

        [Required]
        [RegularExpression(Models.BankCard.CardNumberPattern, ErrorMessage = Models.BankCard.InvalidCardNumberErrorMessage)]
        public string CardNumber { get; set; }

        [Required]
        public DateTime ValidThrough { get; set; }

        [Required]
        [RegularExpression(Models.BankCard.HolderPattern, ErrorMessage = Models.BankCard.InvalidHolderNameErrorMessage)]
        [MaxLength(28)]
        public string HolderName { get; set; }

        [Required]
        [RegularExpression(Models.BankCard.PinPattern, ErrorMessage = Models.BankCard.InvalidPinErrorMessage)]
        public string PIN { get; set; }

        [Required]
        [RegularExpression(Models.BankCard.CvvPattern, ErrorMessage =Models.BankCard.InvalidCvvErrorMessage)]
        public string CVV { get; set; }

        [Required]
        [EnumDataType(typeof(CardType))]
        public CardType CardType { get; set; }

        public int UserId { get; set; }

        [Required]
        [RegularExpression(Models.Currency.CurrencyPattern, ErrorMessage = ErrorMessages.InvalidCurrencyIdErrorMessage)]
        [MaxLength(3)]
        public string CurrencyId { get; set; }
    }
}
