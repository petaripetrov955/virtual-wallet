﻿namespace Wallet.Core.QueryParameters
{
    public class UserQueryParameters
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? UserName { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public string? SortBy { get; set; }
        public string? SortOrder { get; set; }
        public int PageSize { get; set; } = 3;
        public int PageNumber { get; set; } = 1;

		public string? Control { get; set; }
		public string? Keyword { get; set; }
	}
}
