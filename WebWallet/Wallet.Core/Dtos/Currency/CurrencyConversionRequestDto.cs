﻿using System.ComponentModel.DataAnnotations;

namespace Wallet.Core.Dtos.Currency
{
    public class CurrencyConversionRequestDto
    {
        [Required]
        [RegularExpression(Models.Currency.CurrencyPattern, ErrorMessage = ErrorMessages.InvalidCurrencyIdErrorMessage)]
        [MaxLength(3)]
        public string ReceivingCurrency { get; set; }

        [Required]
        [RegularExpression(Models.Currency.CurrencyPattern, ErrorMessage = ErrorMessages.InvalidCurrencyIdErrorMessage)]
        [MaxLength(3)]
        public string SendingCurrency { get; set; }


        //To Do discuss with Vladi about the limit that can be transferred.
        [Required]
        [Range(1, 100000, ErrorMessage = "Amount must be between 1 and 100000.")]
        public decimal AmountInReceivingCurrency { get; set; }
    }
}
