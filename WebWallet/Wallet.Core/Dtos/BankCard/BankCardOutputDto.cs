﻿namespace Wallet.Core.Dtos.BankCard
{
    public class BankCardOutputDto
    {
        public string Name { get; set; }
        public string ValidThrough { get; set; }
        public string HolderName { get; set; }
        public string  CardType { get; set; }
        public string CurrencyId { get; set; }
        public string CardNumber { get; set; }
    }
}
