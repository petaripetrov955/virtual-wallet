﻿using Microsoft.AspNetCore.Identity;
using System.Data;

namespace Wallet.Core.Models
{
	public class Role : IdentityRole<int>
	{
		public Role(int id, String name) : base(name) 
		{
			Id = id;
			NormalizedName = name.ToUpper();
		}
	}
}
