﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Core.Models;

namespace Wallet.Repo.Data.Seed
{
    static class TransferModelBuilderExtension
    {
        public static void SeedTransfers(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Transfer>().HasData
            (
                new List<Transfer>()
                {
                    new Transfer()
                    {
                        Id = 1,
                        CreatedAt = DateTime.Parse("2023-07-08T16:52:46.1921049+03:00"),
                        UserId = 3,
                        AccountId = 3,
                        BankCardId = 3,
                        Amount = 10.15m,
                        IsIncoming = true,
                        Description = "1st TRANSFER TO MY WALLET",
                        CurrencyId = "EUR"
                    },

                    new Transfer()
                    {
                        Id = 2,
                        CreatedAt = DateTime.Parse("2023-07-31T17:00:02.898005+03:00"),
                        UserId = 3,
                        AccountId = 3,
                        BankCardId = 3,
                        Amount = 24.15m,
                        IsIncoming = true,
                        Description = "2nd TRANSFER TO MY WALLET",
                        CurrencyId = "EUR"
                    },

                     new Transfer()
                     {
                        Id = 3,
                        CreatedAt = DateTime.Parse("2023-08-08T17:01:38.6609707"),
                        UserId = 3,
                        AccountId = 3,
                        BankCardId = 3,
                        Amount = 5.00m,
                        IsIncoming = false,
                        Description = "1st TRANSFER TO MY Eur Bank Card",
                        CurrencyId = "EUR"
                     },

                     new Transfer()
                     {
                        Id = 4,
                        CreatedAt = DateTime.Parse("2023-08-08T17:01:38.6609707"),
                        UserId = 3,
                        AccountId = 3,
                        BankCardId = 3,
                        Amount = 5.00m,
                        IsIncoming = false,
                        Description = "1st TRANSFER TO MY Eur Bank Card",
                        CurrencyId = "EUR"
                     },

                     new Transfer()
                     {
                        Id = 5,
                        CreatedAt = DateTime.Parse("2023-08-08T17:04:04.0071372+03:00"),
                        UserId = 3,
                        AccountId = 3,
                        BankCardId = 3,
                        Amount = 256.79m,
                        IsIncoming = true,
                        Description = "3rd TRANSFER TO MY EUR WALLET",
                        CurrencyId = "EUR"
                     },

                     new Transfer()
                     {
                        Id = 6,
                        CreatedAt = DateTime.Parse("2023-07-15T17:06:49.1825777+03:00"),
                        UserId = 7,
                        AccountId = 1,
                        BankCardId = 1,
                        Amount = 50.48m,
                        IsIncoming = true,
                        Description = "Iva's first transfer to BGN wallet from BGN card",
                        CurrencyId = "BGN"
                     },

                     new Transfer()
                     {
                        Id = 7,
                        CreatedAt = DateTime.Parse("2023-08-01T17:09:21.4077928+03:00"),
                        UserId = 7,
                        AccountId = 1,
                        BankCardId = 1,
                        Amount = 568.79m,
                        IsIncoming = true,
                        Description = "Iva's second transfer to BGN account",
                        CurrencyId = "BGN"
                     },

                     new Transfer()
                     {
                        Id = 8,
                        CreatedAt = DateTime.Parse("2023-08-08T17:10:41.6978438+03:00"),
                        UserId = 7,
                        AccountId = 1,
                        BankCardId = 1,
                        Amount = 75.00m,
                        IsIncoming = false,
                        Description = "To Bank Card for electricity",
                        CurrencyId = "BGN"
                     },

                     new Transfer()
                     {
                        Id = 9,
                        CreatedAt = DateTime.Parse("2023-08-08T17:12:52.9787174+03:00"),
                        UserId = 7,
                        AccountId = 2,
                        BankCardId = 6,
                        Amount = 1200.00m,
                        IsIncoming = true,
                        Description = "1200 dollar for USA trip",
                        CurrencyId = "USD"
                     },

                     new Transfer()
                     {
                        Id = 10,
                        CreatedAt = DateTime.Parse("2023-08-08T17:14:03.6985678+03:00"),
                        UserId = 7,
                        AccountId = 2,
                        BankCardId = 6,
                        Amount = 124.98m,
                        IsIncoming = true,
                        Description = "just like that",
                        CurrencyId = "USD"
                     },

                     new Transfer()
                     {
                        Id = 11,
                        CreatedAt = DateTime.Parse("2023-08-08T17:15:47.2291283+03:00"),
                        UserId = 7,
                        AccountId = 2,
                        BankCardId = 6,
                        Amount = 124.98m,
                        IsIncoming = false,
                        Description = "back to my USD bank card",
                        CurrencyId = "USD"
                     }

                }
            );
        }
    }
}
