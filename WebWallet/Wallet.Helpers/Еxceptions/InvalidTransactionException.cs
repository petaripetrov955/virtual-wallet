﻿namespace Wallet.Helpers.Еxceptions
{
    public class InvalidTransactionException : ApplicationException
    {
        public InvalidTransactionException(string message) : base(message)
        {
        }
    }
}
