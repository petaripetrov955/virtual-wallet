﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wallet.Core.Dtos.Account
{
    public class AccountForOperationDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CurrencyId { get; set; }
        public decimal Balance { get; set; }
    }
}
