﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Core.Models;

namespace Wallet.Repo.Data.Seed
{
    static class TransactionModelBuilderExtension
    {
        public static void SeedTransactions(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Transaction>().HasData
            (
                new List<Transaction>()
                {
                    new Transaction()
                    {
                        Id = 1,
                        CreatedAt = DateTime.Parse("2023-08-08T17:42:54.5021385+03:00"),
                        SenderUserId = 3,
                        SenderAccountId = 5,
                        RecepientUserId = 3,
                        RecepientAccountId = 3,
                        OriginAmount = 977.92m,
                        OriginCurrencyId = "BGN",
                        TargetAmount = 500.00m,
                        TargetCurrencyId = "EUR",
                        ExchangeRate = 0.5113m,
                        Description = "Internal Exchange between own Wallets: BGN -> EUR",
                        IsInternal = true                        

                    },

                    new Transaction()
                    {
                        Id = 2,
                        CreatedAt = DateTime.Parse("2023-08-08T17:46:44.2297731+03:00"),
                        SenderUserId = 3,
                        SenderAccountId = 3,
                        RecepientUserId = 3,
                        RecepientAccountId = 5,
                        OriginAmount = 500.00m,
                        OriginCurrencyId = "EUR",
                        TargetAmount = 977.92m,
                        TargetCurrencyId = "BGN",
                        ExchangeRate = 1.9558m,
                        Description = "Internal Exchange between own Wallets: EUR -> BGN",
                        IsInternal = true

                    },

                    new Transaction()
                    {
                        Id = 3,
                        CreatedAt = DateTime.Parse("2023-08-08T17:50:25.9712747+03:00"),
                        SenderUserId = 3,
                        SenderAccountId = 3,
                        RecepientUserId = 3,
                        RecepientAccountId = 4,
                        OriginAmount = 45.66m,
                        OriginCurrencyId = "EUR",
                        TargetAmount = 50.00m,
                        TargetCurrencyId = "USD",
                        ExchangeRate = 1.0950m,
                        Description = "Internal Exchange between own Wallets: EUR -> USD",
                        IsInternal = true

                    },

                    new Transaction()
                    {
                        Id = 4,
                        CreatedAt = DateTime.Parse("2023-08-08T17:52:15.1673722+03:00"),
                        SenderUserId = 3,
                        SenderAccountId = 5,
                        RecepientUserId = 3,
                        RecepientAccountId = 4,
                        OriginAmount = 228.63m,
                        OriginCurrencyId = "BGN",
                        TargetAmount = 128.00m,
                        TargetCurrencyId = "USD",
                        ExchangeRate = 0.5598m,
                        Description = "Internal Exchange between own Wallets: BGN -> USD",
                        IsInternal = true

                    },

                    new Transaction()
                    {
                        Id = 5,
                        CreatedAt = DateTime.Parse("2023-08-08T17:53:36.4487708+03:00"),
                        SenderUserId = 3,
                        SenderAccountId = 4,
                        RecepientUserId = 3,
                        RecepientAccountId = 5,
                        OriginAmount = 55.98m,
                        OriginCurrencyId = "USD",
                        TargetAmount = 100.00m,
                        TargetCurrencyId = "BGN",
                        ExchangeRate = 1.7862m,
                        Description = "Internal Exchange between own Wallets: USD -> BGN",
                        IsInternal = true

                    },

                    new Transaction()
                    {
                        Id = 6,
                        CreatedAt = DateTime.Parse("2023-08-08T17:55:20.9846328+03:00"),
                        SenderUserId = 7,
                        SenderAccountId = 1,
                        RecepientUserId = 7,
                        RecepientAccountId = 2,
                        OriginAmount = 1.79m,
                        OriginCurrencyId = "BGN",
                        TargetAmount = 1.00m,
                        TargetCurrencyId = "USD",
                        ExchangeRate = 0.5598m,
                        Description = "Internal Exchange between own Wallets: BGN -> USD",
                        IsInternal = true

                    },

                    new Transaction()
                    {
                        Id = 7,
                        CreatedAt = DateTime.Parse("2023-08-08T17:56:46.244623+03:00"),
                        SenderUserId = 7,
                        SenderAccountId = 2,
                        RecepientUserId = 7,
                        RecepientAccountId = 1,
                        OriginAmount = 0.56m,
                        OriginCurrencyId = "USD",
                        TargetAmount = 1.00m,
                        TargetCurrencyId = "BGN",
                        ExchangeRate = 1.7862m,
                        Description = "Internal Exchange between own Wallets: USD -> BGN",
                        IsInternal = true

                    },

                    new Transaction()
                    {
                        Id = 8,
                        CreatedAt = DateTime.Parse("2023-08-08T17:58:37.7734357+03:00"),
                        SenderUserId = 7,
                        SenderAccountId = 2,
                        RecepientUserId = 7,
                        RecepientAccountId = 1,
                        OriginAmount = 31.89m,
                        OriginCurrencyId = "USD",
                        TargetAmount = 56.97m,
                        TargetCurrencyId = "BGN",
                        ExchangeRate = 1.7862m,
                        Description = "Internal Exchange between own Wallets: USD -> BGN",
                        IsInternal = true

                    },

                    new Transaction()
                    {
                        Id = 9,
                        CreatedAt = DateTime.Parse("2023-08-08T18:00:00.3426121+03:00"),
                        SenderUserId = 7,
                        SenderAccountId = 1,
                        RecepientUserId = 7,
                        RecepientAccountId = 2,
                        OriginAmount = 45.26m,
                        OriginCurrencyId = "BGN",
                        TargetAmount = 25.34m,
                        TargetCurrencyId = "USD",
                        ExchangeRate = 0.5598m,
                        Description = "Internal Exchange between own Wallets: BGN -> USD",
                        IsInternal = true

                    },

                    new Transaction()
                    {
                        Id = 10,
                        CreatedAt = DateTime.Parse("2023-08-08T18:03:25.4558899+03:00"),
                        SenderUserId = 3,
                        SenderAccountId = 5,
                        RecepientUserId = 7,
                        RecepientAccountId = 1,
                        OriginAmount = 5.00m,
                        OriginCurrencyId = "BGN",
                        TargetAmount = 5.00m,
                        TargetCurrencyId = "BGN",
                        ExchangeRate = 1.00m,
                        Description = "TAKSATA 5 LEVA. ot moita lev smetka. SINCERELY, GEORGI (No Exchange)",
                        IsInternal = false

                    },

                    new Transaction()
                    {
                        Id = 11,
                        CreatedAt = DateTime.Parse("2023-08-08T18:05:23.3128327+03:00"),
                        SenderUserId = 3,
                        SenderAccountId = 5,
                        RecepientUserId = 7,
                        RecepientAccountId = 2,
                        OriginAmount = 8.94m,
                        OriginCurrencyId = "BGN",
                        TargetAmount = 5.00m,
                        TargetCurrencyId = "USD",
                        ExchangeRate = 0.5596m,
                        Description = "TAKSATA 5 dolara. ot moita lev smetka. SINCERELY, GEORGI (Exchange: BGN -> USD)",
                        IsInternal = false

                    },

                    new Transaction()
                    {
                        Id = 12,
                        CreatedAt = DateTime.Parse("2023-08-08T18:07:41.6655014+03:00"),
                        SenderUserId = 3,
                        SenderAccountId = 3,
                        RecepientUserId = 7,
                        RecepientAccountId = 2,
                        OriginAmount = 9.14m,
                        OriginCurrencyId = "EUR",
                        TargetAmount = 10.00m,
                        TargetCurrencyId = "USD",
                        ExchangeRate = 1.0944m,
                        Description = "TAKSATA 10 dolara. ot moita evro smetka. SINCERELY, GEORGI (Exchange: EUR -> USD)",
                        IsInternal = false

                    },

                    new Transaction()
                    {
                        Id = 13,
                        CreatedAt = DateTime.Parse("2023-08-08T18:12:10.7152695+03:00"),
                        SenderUserId = 7,
                        SenderAccountId = 1,
                        RecepientUserId = 3,
                        RecepientAccountId = 4,
                        OriginAmount = 46.43m,
                        OriginCurrencyId = "BGN",
                        TargetAmount = 25.98m,
                        TargetCurrencyId = "USD",
                        ExchangeRate = 0.5596m,
                        Description = "dollars for Georgi, Iva (Exchange: BGN -> USD)",
                        IsInternal = false

                    },

                    new Transaction()
                    {
                        Id = 14,
                        CreatedAt = DateTime.Parse("2023-08-08T18:14:04.4042436+03:00"),
                        SenderUserId = 7,
                        SenderAccountId = 1,
                        RecepientUserId = 3,
                        RecepientAccountId = 3,
                        OriginAmount = 39.12m,
                        OriginCurrencyId = "BGN",
                        TargetAmount = 20.00m,
                        TargetCurrencyId = "USD",
                        ExchangeRate = 0.5113m,
                        Description = "20 evra for Georgi, Iva (Exchange: BGN -> EUR)",
                        IsInternal = false

                    },

                    new Transaction()
                    {
                        Id = 15,
                        CreatedAt = DateTime.Parse("2023-08-08T18:15:51.2716356+03:00"),
                        SenderUserId = 7,
                        SenderAccountId = 2,
                        RecepientUserId = 3,
                        RecepientAccountId = 5,
                        OriginAmount = 55.97m,
                        OriginCurrencyId = "USD",
                        TargetAmount = 100.00m,
                        TargetCurrencyId = "BGN",
                        ExchangeRate = 1.7866m,
                        Description = "100 kinta za Georgi, Iva (Exchange: USD -> BGN)",
                        IsInternal = false

                    },

                    new Transaction()
                    {
                        Id = 16,
                        CreatedAt = DateTime.Parse("2023-08-08T18:17:49.9523636+03:00"),
                        SenderUserId = 7,
                        SenderAccountId = 2,
                        RecepientUserId = 1,
                        RecepientAccountId = 6,
                        OriginAmount = 21.89m,
                        OriginCurrencyId = "USD",
                        TargetAmount = 20.00m,
                        TargetCurrencyId = "EUR",
                        ExchangeRate = 0.9135m,
                        Description = "20 evro za admina, Iva (Exchange: USD -> EUR)",
                        IsInternal = false

                    },

                    new Transaction()
                    {
                        Id = 17,
                        CreatedAt = DateTime.Parse("2023-08-08T18:20:24.4669789+03:00"),
                        SenderUserId = 7,
                        SenderAccountId = 1,
                        RecepientUserId = 1,
                        RecepientAccountId = 6,
                        OriginAmount = 61.24m,
                        OriginCurrencyId = "BGN",
                        TargetAmount = 31.31m,
                        TargetCurrencyId = "EUR",
                        ExchangeRate = 0.5113m,
                        Description = "31.31 evro za admina, Iva (Exchange: BGN -> EUR)",
                        IsInternal = false

                    }






                }
            );
        }
    }
}
