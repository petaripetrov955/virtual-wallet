﻿using Moq;
using Wallet.Core;
using Wallet.Core.Dtos.Account;
using Wallet.Core.Models;
using Wallet.Helpers.Еxceptions;
using Wallet.Repo.Interfaces;
using Wallet.Service.Interfaces;
using Wallet.Service.Services;

namespace Wallet.Tests
{
    [TestClass]
    public class AccountServiceTests
    {
        private Mock<IAccountRepository> accountRepositoryMock;
        private Mock<ICurrencyService> currencyServiceMock;
        private Mock<IUsersService> usersServiceMock;

        private AccountService testAccountService;

        [TestInitialize]
        public void InitTest()
        {
            accountRepositoryMock = new Mock<IAccountRepository>();
            currencyServiceMock = new Mock<ICurrencyService>();
            usersServiceMock = new Mock<IUsersService>();

            testAccountService = new AccountService(accountRepositoryMock.Object, currencyServiceMock.Object, usersServiceMock.Object);
        }

        [TestMethod]
        public async Task GetByIdAsync_Should_ReturnAccount_When_UserIsAdmin()
        {
            //Arrange
            User user = TestHelper.GetTestAdmin();
            Account expectedAccount = TestHelper.GetTestUserAccount();

            accountRepositoryMock
                .Setup(repo => repo.GetAsync(expectedAccount.Id))
                .ReturnsAsync(expectedAccount);

            usersServiceMock
                .Setup(service => service.GetRolesAsync(user))
                .ReturnsAsync(new List<string> { Constants.Roles.AdminRoleName });

            // Act
            Account actualAccount = await testAccountService.GetByIdAsync(expectedAccount.Id, user);

            // Assert
            Assert.AreEqual(expectedAccount, actualAccount);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public async Task GetByIdAsync_Should_ThrowEntityNotFound_When_AccountIsDeleted()
        {
            // Arrange
            int accountId = 1;
            User user = TestHelper.GetTestUser();
            Account deletedAccount = new Account { Id = accountId, IsDeleted = true };

            accountRepositoryMock
                .Setup(repo => repo.GetAsync(accountId))
                .ReturnsAsync(deletedAccount);

            // Act
            await testAccountService.GetByIdAsync(accountId, user);
        }

        [TestMethod]
        [ExpectedException(typeof(UnauthorizedOperationException))]
        public async Task GetByIdAsync_Should_ThrowUnauthorized_When_UserIsNotOwnerAndNotAdmin()
        {
            // Arrange
            User user = TestHelper.GetTestUser();
            Account account = TestHelper.GetAnotherTestUserAccount();

            accountRepositoryMock
                .Setup(repo => repo.GetAsync(account.Id))
                .ReturnsAsync(account);

            usersServiceMock
                .Setup(service => service.GetRolesAsync(user))
                .ReturnsAsync(new List<string>());

            // Act
            await testAccountService.GetByIdAsync(account.Id, user);
        }

        [TestMethod]
        public async Task GetByIdAsync_Should_ReturnAccount_When_UserIsOwner()
        {
            // Arrange
            User user = TestHelper.GetAnotherTestUser();
            Account account = TestHelper.GetAnotherTestUserAccount();

            accountRepositoryMock
                .Setup(repo => repo.GetAsync(account.Id))
                .ReturnsAsync(account);

            usersServiceMock
               .Setup(service => service.GetRolesAsync(user))
               .ReturnsAsync(new List<string>() { Constants.Roles.UserRoleName });

            // Act
            Account actualAccount = await testAccountService.GetByIdAsync(account.Id, user);

            // Assert
            Assert.AreEqual(account, actualAccount);
        }

        [TestMethod]
        public async Task GetByIdAsync_Should_ReturnAccount_When_AdminAndAccountExists()
        {
            // Arrange
            Account account = TestHelper.GetAnotherTestUserAccount();
            User user = TestHelper.GetTestAdmin();

            accountRepositoryMock
                .Setup(repo => repo.GetAsync(account.Id))
                .ReturnsAsync(account);

            usersServiceMock
                .Setup(service => service.GetRolesAsync(user))
                .ReturnsAsync(new List<string> { Constants.Roles.AdminRoleName });

            // Act
            Account actualAccount = await testAccountService.GetByIdAsync(account.Id, user);

            // Assert
            Assert.AreEqual(account, actualAccount);
        }

        [TestMethod]
        public async Task CreateAsync_Should_CreateAccountSuccessfully_When_AllValid()
        {
            //Arrange
            User user = TestHelper.GetTestUser();
            Account accountToCreate = new Account { UserId = user.Id, CurrencyId = "USD" };

            ICollection<Currency> supportedCurrencies = TestHelper.GetTestCurrencies();

            currencyServiceMock
                .Setup(service => service.GetAllAsync())
                .ReturnsAsync(supportedCurrencies);
            accountRepositoryMock
                .Setup(repo => repo.CreateAsync(accountToCreate))
                .ReturnsAsync(accountToCreate);

            // Act
            Account createdAccount = await testAccountService.CreateAsync(accountToCreate, user);

            // Assert
            Assert.AreEqual(accountToCreate, createdAccount);
            Assert.IsTrue(user.Accounts.Contains(accountToCreate));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task CreateAsync_Should_ThrowUnsupportedCurrency_When_CurrencyNotSupported()
        {
            // Arrange
            User user = TestHelper.GetTestUser();
            Account accountToCreate = new Account { UserId = user.Id, CurrencyId = "JMD" };

            ICollection<Currency> supportedCurrencies = new List<Currency>
            {
                TestHelper.GetTestCurrency()
            };

            currencyServiceMock
                .Setup(service => service.GetAllAsync())
                .ReturnsAsync(supportedCurrencies);

            // Act
            await testAccountService.CreateAsync(accountToCreate, user);
        }

        [TestMethod]
        public async Task UpdateAsync_Should_UpdateAccountSuccessfully_When_AllValid()
        {
            // Arrange
            User user = TestHelper.GetAnotherTestUser();
            AccountUpdateDto accountUpdates = new AccountUpdateDto { Id = TestHelper.GetAnotherTestUserAccount().Id, Name = "New Account Name" };
            Account accountToUpdate = TestHelper.GetAnotherTestUserAccount();

            accountRepositoryMock
                .Setup(repo => repo.GetAsync(accountUpdates.Id))
                .ReturnsAsync(accountToUpdate);

            accountRepositoryMock
                .Setup(repo => repo.UpdateAsync(accountToUpdate, accountUpdates.Name))
                .ReturnsAsync(new Account { Id = accountToUpdate.Id, UserId = user.Id, Name = "New Account Name" });

            usersServiceMock
               .Setup(service => service.GetRolesAsync(user))
               .ReturnsAsync(new List<string>() { Constants.Roles.UserRoleName });

            // Act
            Account updatedAccount = await testAccountService.UpdateAsync(accountUpdates, user);

            // Assert
            Assert.AreEqual(accountUpdates.Name, updatedAccount.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task UpdateAsync_Should_ThrowDuplicateName_When_NameAlreadyExists()
        {
            // Arrange
            User user = TestHelper.GetTestUser();
            AccountUpdateDto accountUpdates = new AccountUpdateDto { Id = TestHelper.GetTestUserAccount().Id, Name = TestHelper.GetTestUserAccount().Name };
            Account accountToUpdate = TestHelper.GetTestUserAccount();

            accountRepositoryMock
                .Setup(repo => repo.GetAsync(accountUpdates.Id))
                .ReturnsAsync(accountToUpdate);

            usersServiceMock
                .Setup(service => service.GetRolesAsync(user))
                .ReturnsAsync(new List<string>() { Constants.Roles.UserRoleName });

            // Act
            await testAccountService.UpdateAsync(accountUpdates, user);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task UpdateAsync_Should_ThrowInvalidName_When_InvalidNameFormat()
        {
            // Arrange
            User user = TestHelper.GetTestUser();
            AccountUpdateDto accountUpdates = new AccountUpdateDto { Id = TestHelper.GetTestUserAccount().Id, Name = "Invalid Name!" };
            Account accountToUpdate = TestHelper.GetTestUserAccount();

            accountRepositoryMock
                .Setup(repo => repo.GetAsync(accountUpdates.Id))
                .ReturnsAsync(accountToUpdate);

            usersServiceMock
               .Setup(service => service.GetRolesAsync(user))
               .ReturnsAsync(new List<string>() { Constants.Roles.UserRoleName });

            // Act
            await testAccountService.UpdateAsync(accountUpdates, user);
        }

        [TestMethod]
        [ExpectedException(typeof(NonZeroBalanceException))]
        public async Task DeleteAsync_Should_ThrowNonZeroBalance_When_AccountBalanceNotZero()
        {
            // Arrange
            User user = TestHelper.GetTestUser();
            Account accountToDelete = TestHelper.GetTestUserAccount();

            accountRepositoryMock
                .Setup(repo => repo.GetAsync(accountToDelete.Id))
                .ReturnsAsync(accountToDelete);

            usersServiceMock
               .Setup(service => service.GetRolesAsync(user))
               .ReturnsAsync(new List<string>() { Constants.Roles.UserRoleName });

            // Act
            await testAccountService.DeleteAsync(accountToDelete.Id, user);
        }

        [TestMethod]
        public async Task DeleteAsync_Should_DeleteAccountSuccessfully_When_BalanceIsZero()
        {
            // Arrange
            int accountId = 1;
            User user = TestHelper.GetAnotherTestUser();
            Account accountToDelete = TestHelper.GetAnotherTestUserAccount();

            accountRepositoryMock
                .Setup(repo => repo.GetAsync(TestHelper.GetAnotherTestUserAccount().Id))
                .ReturnsAsync(accountToDelete);

            accountRepositoryMock
                .Setup(repo => repo.DeleteAsync(accountToDelete))
                .ReturnsAsync(accountToDelete);

            usersServiceMock
               .Setup(service => service.GetRolesAsync(user))
               .ReturnsAsync(new List<string>() { Constants.Roles.UserRoleName });

            // Act
            Account deletedAccount = await testAccountService.DeleteAsync(TestHelper.GetAnotherTestUserAccount().Id, user);

            // Assert
            Assert.AreEqual(accountToDelete, deletedAccount);
        }
    }
}
