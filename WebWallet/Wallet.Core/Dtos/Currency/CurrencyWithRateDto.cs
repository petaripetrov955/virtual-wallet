﻿using System.ComponentModel.DataAnnotations;

namespace Wallet.Core.Dtos.Currency
{
    public class CurrencyWithRateDto
    {
        [Required]
        [RegularExpression(Models.Currency.CurrencyPattern, ErrorMessage = ErrorMessages.InvalidCurrencyIdErrorMessage)]
        [MaxLength(3)]
        public string BaseCurrencyCode { get; set; }

        [Required]
        [RegularExpression(Models.Currency.CurrencyPattern, ErrorMessage = ErrorMessages.InvalidCurrencyIdErrorMessage)]
        [MaxLength(3)]
        public string ComparedToCurrencyCode { get; set; }
    }
}
