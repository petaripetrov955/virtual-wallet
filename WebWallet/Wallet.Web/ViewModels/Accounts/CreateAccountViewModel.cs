﻿using System.ComponentModel.DataAnnotations;
using Wallet.Core;

using static Wallet.Core.Models.Account;

namespace Wallet.Web.ViewModels.Accounts
{
    public class CreateAccountViewModel
    {
        [Required]
        [RegularExpression(Name_Regex_Pattern, ErrorMessage = ErrorMessages.InvalidAccountNameErrorMessage)]
        public string Name { get; set; }

        [Required]
        public string CurrencyId { get; set; }
    }
}
