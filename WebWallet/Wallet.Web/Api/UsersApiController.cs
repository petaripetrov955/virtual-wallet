﻿using Microsoft.AspNetCore.Mvc;
using Wallet.Core.Dtos;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Service.Interfaces;
using Wallet.Helpers.Еxceptions;
using Wallet.Web.Mappers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Wallet.Core;

namespace Wallet.Web.Api
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/users")]
    [ApiController]
    public class UsersApiController : ControllerBase
    {
		private readonly UserMapper _userMapper;
		private readonly IUsersService _usersService;		

		public UsersApiController(UserMapper userMapper, IUsersService userService)
        {
			this._userMapper = userMapper;
            this._usersService = userService;
        }


		[HttpGet("")]
		public async Task<IActionResult> GetUsersAsync()
		{
			List<User> result = await this._usersService.GetAllAsync();
			List<UserUpdateDto> resultDto = result.Select(user => this._userMapper.UpdateDtoMap(user)).ToList();

			return StatusCode(StatusCodes.Status200OK, resultDto);
		}


		[AllowAnonymous]
		[HttpGet("count")]
		public async Task<IActionResult> CountAllAsync()
		{
			int result = await this._usersService.CountAllAsync();

			return StatusCode(StatusCodes.Status200OK, result);
		}


        [AllowAnonymous]
        [HttpPost("register")]
		public async Task<IActionResult> RegsterUserAsync([FromBody] RegistrationDto dto)
		{
			try
			{
				User user = this._userMapper.CreateMap(dto);
				var result = await this._usersService.RegisterAsync(user, dto.Password);

				return StatusCode(StatusCodes.Status201Created, result);
			}
			catch (DuplicateEntityException e)
			{
				return StatusCode(StatusCodes.Status409Conflict, e.Message);
			}
			catch (Exception e)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
			}
		}

		[HttpPut("{id}/profile")]
		public async Task<IActionResult> UpdateProfileAsync(int id, [FromBody] UserUpdateDto dto)
		{
			try
			{
                //TODO: credentials
                User currentUser = await this._usersService.GetByUserNameAsync(User.Identity.Name);
                //if (id != currentUser.Id)
                //{
                //    this.Response.StatusCode = StatusCodes.Status403Forbidden;
                //    this.ViewData["ErrorMessage"] = $"You cannot edit another user's profile.";

                //    return View("Error");
                //}

                User userToUpdate = this._userMapper.UpdateMap(dto);
				var result = await this._usersService.UpdateProfileAsync(id, userToUpdate);

				return StatusCode(StatusCodes.Status200OK, result);
			}
			catch (EntityNotFoundException e)
			{
				return StatusCode(StatusCodes.Status404NotFound, e.Message);
			}
			catch (DuplicateEntityException e)
			{
				return StatusCode(StatusCodes.Status409Conflict, e.Message);
			}
			catch (Exception e)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
			}
		}


        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
		public async Task<IActionResult> DeleteUserAsync(int id)
		{
			try
			{
				//TODO: credentials
				//User userToDelete = await usersService.DeleteAsync(id, loggedUser);
				var result = await this._usersService.DeleteAsync(id);

				return StatusCode(StatusCodes.Status200OK, result);
			}
            catch (NonZeroBalanceException e)
            {
				return StatusCode(StatusCodes.Status409Conflict, ErrorMessages.DeleteUserAccountErrorMessage);
            }
            catch (EntityNotFoundException e)
			{
				return StatusCode(StatusCodes.Status404NotFound, e.Message);
			}
			catch (Exception e)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
			}
		}


        [Authorize(Roles = "Admin")]
        [HttpPut("{id}/block")]
		public async Task<IActionResult> BlockAsync(int id)
		{
			try
			{
				//TODO: credentials
				//User user = await usersService.BlockAsync(id, loggedUser);
				var result = await this._usersService.BlockAsync(id);

				return StatusCode(StatusCodes.Status200OK, result);
			}
			catch (EntityNotFoundException e)
			{
				return StatusCode(StatusCodes.Status404NotFound, e.Message);
			}
			catch (Exception e)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
			}
		}


        [Authorize(Roles = "Admin")]
        [HttpPut("{id}/unblock")]
		public async Task<IActionResult> UnblockAsync(int id)
		{
			try
			{
				//TODO: credentials
				//User user = await usersService.UnblockAsync(id, loggedUser);
				var result = await this._usersService.UnblockAsync(id);

				return StatusCode(StatusCodes.Status200OK, result);
			}
			catch (EntityNotFoundException e)
			{
				return StatusCode(StatusCodes.Status404NotFound, e.Message);
			}
			catch (Exception e)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
			}
		}


		[HttpGet("search")]
		public async Task<IActionResult> GetSearchedUsersAsync([FromQuery] UserQueryParameters filterParameters)
		{
			try
			{
				PaginatedList<User> result = await this._usersService.SearchByAsync(filterParameters);
				List<UserUpdateDto> resultDto = result.Select(user => this._userMapper.UpdateDtoMap(user)).ToList();
				return StatusCode(StatusCodes.Status200OK, resultDto);
			}
			catch (Exception e)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
			}
		}
	}
}
