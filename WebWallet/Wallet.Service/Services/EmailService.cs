﻿using sib_api_v3_sdk.Api;
using sib_api_v3_sdk.Model;
using Wallet.Service.Interfaces;

namespace Wallet.Service
{
	public class EmailService : IEmailService
    {
		private readonly TransactionalEmailsApi mailService;

		public EmailService(TransactionalEmailsApi mailService)
		{
			this.mailService = mailService;
		}

		public async Task<bool> SendEmailAsync(string userEmail, string confirmationLink)
		{
			try
			{
				var to = new SendSmtpEmailTo(email: userEmail);
				var sender = new SendSmtpEmailSender("The Wallet", "support@the-wallet.com");
				var email = new SendSmtpEmail(
					to: new List<SendSmtpEmailTo> { to },
					sender: sender,
					subject: "Wallet Account Email verification",
					htmlContent: $"<p>To verify your email, please confirm by clicking the following link: </p><p>({confirmationLink})</p>"
				);

				CreateSmtpEmail result = await mailService.SendTransacEmailAsync(email);
				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}
	}
}
