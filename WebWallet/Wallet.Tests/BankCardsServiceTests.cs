﻿using Moq;
using Wallet.Core.Dtos.BankCard;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Helpers;
using Wallet.Helpers.Еxceptions;
using Wallet.Repo.Interfaces;
using Wallet.Service.Interfaces;
using Wallet.Service.Services;

namespace Wallet.Tests
{
    [TestClass]
    public class BankCardsServiceTests
    {
        private Mock<IBankCardRepository> bankCardRepositoryMock;
        private Mock<ICurrencyService> currencyServiceMock;
        private Mock<IUsersService> usersServiceMock;

        private BankCardService testBankCardService;

        [TestInitialize]
        public void InitTest()
        {
            bankCardRepositoryMock = new Mock<IBankCardRepository>();
            currencyServiceMock = new Mock<ICurrencyService>();
            usersServiceMock = new Mock<IUsersService>();

            testBankCardService = new BankCardService(bankCardRepositoryMock.Object, currencyServiceMock.Object, usersServiceMock.Object);
        }

        [TestMethod]
        public async Task GetAllAsync_Should_ReturnValidCollection_When_UserIsAdmin()
        {
            // Arrange
            BankCardQueryParameters parameters = new BankCardQueryParameters();
            var user = TestHelper.GetTestAdmin();
            var expectedCards = TestHelper.GetTestBankCards();

            ICollection<BankCard> expectedBankCards = expectedCards;

            bankCardRepositoryMock
                .Setup(repo => repo.GetAllAsync(parameters))
                .Returns(Task.FromResult(expectedBankCards));

            usersServiceMock
                .Setup(service => service.GetRolesAsync(user))
                .ReturnsAsync(new List<string> { "Admin" });

            // Act
            ICollection<BankCard> actualCards = await testBankCardService.GetAllAsync(parameters, user);

            // Assert
            CollectionAssert.AreEqual(expectedCards, actualCards.ToList());
        }

        [TestMethod]
        [ExpectedException(typeof(UnauthorizedOperationException))]
        public async Task GetAllAsync_Should_ThrowUnauthorized_When_UserIsNotOwnerAndHolderSpecified()
        {
            // Arrange
            var user = TestHelper.GetTestUser();
            BankCardQueryParameters parameters = new BankCardQueryParameters { Holder = $"{TestHelper.GetTestUser().FirstName} {TestHelper.GetTestUser().LastName}" };

            usersServiceMock
                .Setup(service => service.GetRolesAsync(user))
                .ReturnsAsync(new List<string>());

            // Act
            await testBankCardService.GetAllAsync(parameters, user);
        }

        [TestMethod]
        public async Task GetAllAsync_Should_ReturnUserCards_When_UserIsOwnerAndHolderSpecified()
        {
            // Arrange
            var user = TestHelper.GetTestUser();
            BankCardQueryParameters parameters = new BankCardQueryParameters { Holder = $"{TestHelper.GetTestUser().FirstName.ToUpper()} {TestHelper.GetTestUser().LastName.ToUpper()}" };
            var expectedCards = TestHelper.GetTestUserBankCards();;

            bankCardRepositoryMock
                .Setup(repo => repo.GetAllAsync(parameters))
                .ReturnsAsync(expectedCards);

            usersServiceMock
                .Setup(service => service.GetRolesAsync(user))
                .ReturnsAsync(new List<string>());
            // Act
            ICollection<BankCard> actualCards = await testBankCardService.GetAllAsync(parameters, user);

            // Assert
            CollectionAssert.AreEqual(expectedCards, actualCards.ToList());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public async Task GetAllAsync_Should_ThrowEmptyCollection_When_NoCardsFound()
        {
            // Arrange
            var user = TestHelper.GetTestAdmin();
            BankCardQueryParameters parameters = new BankCardQueryParameters();
            

            bankCardRepositoryMock
                .Setup(repo => repo.GetAllAsync(parameters))
                .ReturnsAsync(new List<BankCard>());

            usersServiceMock
                .Setup(service => service.GetRolesAsync(user))
                .ReturnsAsync(new List<string> { "Admin" });

            // Act
            await testBankCardService.GetAllAsync(parameters, user);
        }

        [TestMethod]
        [ExpectedException(typeof(UnauthorizedOperationException))]
        public async Task GetByIdAsync_Should_ThrowUnauthorized_When_UserIsBlocked()
        {
            // Arrange
            var user = TestHelper.GetBlockedUser();
            int cardId = It.IsAny<int>();
            BankCard card = TestHelper.GetTestBankCard();

            bankCardRepositoryMock
               .Setup(repo => repo.GetByIdAsync(cardId))
               .ReturnsAsync(card);

            // Act
            await testBankCardService.GetByIdAsync(cardId, user);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public async Task GetByIdAsync_Should_ThrowEntityNotFound_When_CardIsNotActive()
        {
            // Arrange
            var user = TestHelper.GetTestUser();
            BankCard card = TestHelper.GetInactiveBankCard();

            bankCardRepositoryMock
                .Setup(repo => repo.GetByIdAsync(TestHelper.GetInactiveBankCard().Id))
                .ReturnsAsync(card);

            // Act
            await testBankCardService.GetByIdAsync(TestHelper.GetInactiveBankCard().Id, user);
        }

        [TestMethod]
        [ExpectedException(typeof(UnauthorizedOperationException))]
        public async Task GetByIdAsync_Should_ThrowUnauthorized_When_UserIsNotOwner()
        {
            // Arrange
            var user = TestHelper.GetTestUser();
            BankCard card = TestHelper.GetTestBankCard();

            bankCardRepositoryMock
                .Setup(repo => repo.GetByIdAsync(TestHelper.GetTestBankCard().Id))
                .ReturnsAsync(card);

            // Act
            await testBankCardService.GetByIdAsync(TestHelper.GetTestBankCard().Id, user);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task CreateAsync_Should_ThrowUnsupportedCurrency_When_CurrencyNotSupported()
        {
            // Arrange
            var user = TestHelper.GetTestUser();
            BankCard cardToCreate = TestHelper.GetTestBankCard();

            currencyServiceMock
                .Setup(service => service.GetAllAsync())
                .ReturnsAsync(new List<Currency>());

            // Act
            await testBankCardService.CreateAsync(cardToCreate, user);
        }

        [TestMethod]
        public async Task CreateAsync_Should_CreateCardSuccessfully_When_AllValid()
        {
            // Arrange
            var user = TestHelper.GetAnotherTestUser();
            var bgCurrency = TestHelper.GetTestCurrency();
            BankCard cardToCreate = TestHelper.GetTestBankCard();

            ICollection<Currency> currencies = new List<Currency>
            {
                bgCurrency
            };

            currencyServiceMock
                .Setup(service => service.GetAllAsync())
                .ReturnsAsync(currencies);

            bankCardRepositoryMock
                .Setup(repo => repo.CreateAsync(cardToCreate))
                .ReturnsAsync(cardToCreate);

            // Act
            BankCard createdCard = await testBankCardService.CreateAsync(cardToCreate, user);
            // Assert
            Assert.AreEqual(cardToCreate, createdCard);
            Assert.AreEqual(user.BankCards.First(), createdCard);
        }

        [TestMethod]
        [ExpectedException(typeof(UnauthorizedOperationException))]
        public async Task CreateAsync_Should_ThrowUnauthorized_When_UserIsBlocked()
        {
            // Arrange
            var user = TestHelper.GetBlockedUser();
            BankCard cardToCreate = new BankCard();

            currencyServiceMock
                .Setup(service => service.GetAllAsync())
                .ReturnsAsync(new List<Currency>());

            // Act
            await testBankCardService.CreateAsync(cardToCreate, user);
        }

        [TestMethod]
        public async Task UpdateAsync_Should_UpdateCardSuccessfully_When_AllValid()
        {
            // Arrange
            var user = TestHelper.GetAnotherTestUser();
            BankCardUpdateDto updates = new BankCardUpdateDto { Id = 4, Name = "New Card Name", PIN = "2222" };
            BankCard cardToUpdate = TestHelper.GetTestBankCard();

            bankCardRepositoryMock
                .Setup(repo => repo.GetByIdAsync(updates.Id))
                .ReturnsAsync(cardToUpdate);

            bankCardRepositoryMock
                .Setup(repo => repo.UpdateAsync(cardToUpdate, updates))
                .ReturnsAsync(new BankCard
                {
                    Id = 1,
                    UserId = user.Id,
                    Name = updates.Name,
                    PIN = HashingUtility.HashData(updates.PIN)
                });

            // Act
            BankCard updatedCard = await testBankCardService.UpdateAsync(updates, user);

            // Assert
            Assert.AreEqual(updates.Name, updatedCard.Name);
            Assert.AreNotEqual(cardToUpdate.PIN, updatedCard.PIN);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task UpdateAsync_Should_ThrowInvalidPin_When_InvalidPinFormat()
        {
            // Arrange
            var user = TestHelper.GetAnotherTestUser();
            BankCardUpdateDto updates = new BankCardUpdateDto { Id = 4, PIN = "newPin" };
            BankCard cardToUpdate = TestHelper.GetTestBankCard();

            bankCardRepositoryMock
                .Setup(repo => repo.GetByIdAsync(updates.Id))
                .ReturnsAsync(cardToUpdate);

            // Act
            await testBankCardService.UpdateAsync(updates, user);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task UpdateAsync_Should_ThrowUpdateCancelled_When_PINSameAsCurrent()
        {
            // Arrange
            var user = TestHelper.GetAnotherTestUser();
            BankCardUpdateDto updates = new BankCardUpdateDto { Id = 4, PIN = TestHelper.GetTestBankCard().PIN };
            BankCard cardToUpdate = TestHelper.GetTestBankCard();

            bankCardRepositoryMock
                .Setup(repo => repo.GetByIdAsync(updates.Id))
                .ReturnsAsync(cardToUpdate);

            // Act
            await testBankCardService.UpdateAsync(updates, user);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task UpdateAsync_Should_ThrowInvalidAccountName_When_InvalidNameFormat()
        {
            // Arrange
            var user = TestHelper.GetAnotherTestUser();
            BankCardUpdateDto updates = new BankCardUpdateDto { Id = 4, Name = "Invalid Name!" };
            BankCard cardToUpdate = TestHelper.GetTestBankCard();

            bankCardRepositoryMock
                .Setup(repo => repo.GetByIdAsync(updates.Id))
                .ReturnsAsync(cardToUpdate);

            // Act
            await testBankCardService.UpdateAsync(updates, user);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task UpdateAsync_Should_ThrowUpdateCancelled_When_NameSameAsCurrent()
        {
            // Arrange
            var user = TestHelper.GetAnotherTestUser();
            BankCardUpdateDto updates = new BankCardUpdateDto { Id = 4, Name = TestHelper.GetTestBankCard().Name };
            BankCard cardToUpdate = TestHelper.GetTestBankCard();

            bankCardRepositoryMock
                .Setup(repo => repo.GetByIdAsync(updates.Id))
                .ReturnsAsync(cardToUpdate);

            // Act
            await testBankCardService.UpdateAsync(updates, user);
        }

        [TestMethod]
        public async Task DeleteAsync_Should_DeleteCardSuccessfully_When_AllValid()
        {
            // Arrange
            var user = TestHelper.GetAnotherTestUser();
            int cardId = 1;
            BankCard cardToDelete = TestHelper.GetTestBankCard();

            bankCardRepositoryMock
                .Setup(repo => repo.GetByIdAsync(cardId))
                .ReturnsAsync(cardToDelete);

            bankCardRepositoryMock
                .Setup(repo => repo.DeleteAsync(cardToDelete))
                .ReturnsAsync(cardToDelete);

            // Act
            BankCard removedCard = await testBankCardService.DeleteAsync(cardId, user);

            // Assert
            Assert.AreEqual(cardToDelete, removedCard);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public async Task DeleteAsync_Should_ThrowNotFound_When_CardNotFound()
        {
            // Arrange
            var user = TestHelper.GetTestUser();
            int cardId = It.IsAny<int>();

            bankCardRepositoryMock
                .Setup(repo => repo.GetByIdAsync(cardId))
                .ReturnsAsync((BankCard)null);

            // Act
            await testBankCardService.DeleteAsync(cardId, user);
        }
    }
}
