﻿using Microsoft.AspNetCore.Mvc;

namespace Wallet.Web.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Index([FromRoute] int id, string message)
        {            
            this.ViewData["ErrorNumber"] = id;
            if (!string.IsNullOrEmpty(message))
            {
                this.ViewData["ErrorMessage"] = message;
                return View("Error");
            }
            else
            {
                if (id == 401)
                {
                    this.ViewData["ErrorMessage"] = "You are not authorized for this page!";
                }
                if (id == 404)
                {
                    this.ViewData["ErrorMessage"] = "Page not Found";
                }
                if (id == 406)
                {
                    this.ViewData["ErrorMessage"] = "Not acceptible";
                }
                if (id == 500)
                {
                    this.ViewData["ErrorMessage"] = "An unknown Error has occured";
                }
                if (id == 503)
                {
                    this.ViewData["ErrorMessage"] = "External Currency conversion Service not availbale right now. Please try later.";
                }
                if (id == 451)
                {
                    this.ViewData["ErrorMessage"] = "The recepient user is not allowed to receive payments.";
                }

                return View("Error");
            }
            
                


        }
        
    }
}
