﻿
namespace Wallet.Helpers.Еxceptions
{
	public class UnauthenticatedOperationException : ApplicationException
	{
		public UnauthenticatedOperationException(string message)
		   : base(message)
		{
		}
	}
}
