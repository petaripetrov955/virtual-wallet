﻿using Wallet.Repo.Interfaces;
using Wallet.Service.Interfaces;
using Wallet.Core.Models;
using Wallet.Core;
using Wallet.Helpers.Еxceptions;
using Wallet.Web.QueryParameters;

namespace Wallet.Service.Services
{
    public class TransactionsService : ITransactionsService
    {
        private const string NotEnoughFundsMessage = "The amount in your account is less than {0} {1}. Missing {2} {3}";
        private const string SwitchOperationToExchangeInOwnAccounts = "In order to exchange funds between own accounts, please switch the operation to Exchange in own Accounts.";

        private readonly ITransactionsRepository _transactionsRepository;
        private readonly IUsersService _usersService;
        private readonly IAccountService _accountService;

        public TransactionsService(ITransactionsRepository transactionsRepository, IUsersService usersService, IAccountService accountService)
        {
            _transactionsRepository = transactionsRepository;
            _usersService = usersService;
            _accountService = accountService;
        }


        public async Task<List<Transaction>> GetAllTransactionsAsync()
        {
            return await this._transactionsRepository.GetAllTransactionsAsync();
        }

        public async Task<List<Transaction>> GetTransactionsBySenderOrRecepientUserIdAsync(int userId)
        {
            return await this._transactionsRepository.GetTransactionsBySenderOrRecepientUserIdAsync(userId);
        }

        public async Task<Transaction> GetTransactionByIdAsync(int id, User user)
        {
            Transaction transaction = await this._transactionsRepository.GetTransactionByIdAsync(id);
            bool isAdmin = await this._usersService.IsInRoleAsync(user, Constants.Roles.AdminRoleName);

            if (!isAdmin && transaction.SenderUserId != user.Id && transaction.RecepientUserId != user.Id)
            {
                throw new UnauthorizedOperationException(String.Format(ErrorMessages.UserIsNotOwnerErrorMessage, nameof(Transaction), nameof(Transaction)));
            }

            return transaction;
        }

        public async Task<Transaction> CreateTransactionAsync(Transaction transactionToCreate, User sender)
        {
            this._usersService.ValidateUserStatus(sender);
            User recepient = await this._usersService.GetByIdAsync(transactionToCreate.RecepientUserId);
            this._usersService.ValidateUserStatus(recepient);
            Account senderAccount = await _accountService.GetByIdAsync(transactionToCreate.SenderAccountId, sender);
            Account recepientAccount = await _accountService.GetByIdAsync(transactionToCreate.RecepientAccountId, recepient);
            if (!IsBalanceSufficientForTransaction(transactionToCreate, senderAccount))
            {
                string errorMessage = String.Format(NotEnoughFundsMessage, transactionToCreate.OriginAmount, transactionToCreate.OriginCurrencyId, transactionToCreate.OriginAmount - senderAccount.Balance, transactionToCreate.OriginCurrencyId);
                throw new InvalidOperationException(errorMessage);
            }

            AdjustAccountBalance(transactionToCreate, senderAccount, recepientAccount);
            Transaction createdTransaction = await _transactionsRepository.CreateTransactionAsync(transactionToCreate);

            return createdTransaction;
        }

        public bool IsBalanceSufficientForTransaction(Transaction transaction, Account originAccount)
        {
            if (transaction.OriginAmount > originAccount.Balance)
            {
                return false;
            }

            return true;
        }

        public async Task<List<Transaction>> FilterByAsync(User user, bool IsAdmin, TransactionQueryParameters filterParams = null)
        {
            return await _transactionsRepository.FilterByAsync(filterParams, user, IsAdmin);
        }

        public void AdjustAccountBalance(Transaction transaction, Account originAccount, Account targetAccount)
        {
            originAccount.Balance -= transaction.OriginAmount;
            targetAccount.Balance += transaction.TargetAmount;
        }

        public void ThrowErrorIfSenderMatchesRecepient(User sender, User recepient)
        {
            if (sender.Id == recepient.Id)
            {
                throw new InvalidTransactionException(SwitchOperationToExchangeInOwnAccounts);
            }
        }
    }
}
