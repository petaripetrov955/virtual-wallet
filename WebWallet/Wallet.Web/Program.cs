using CloudinaryDotNet;
using dotenv.net;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using sib_api_v3_sdk.Client;
using sib_api_v3_sdk.Api;
using Wallet.Web;
using Microsoft.AspNetCore.Identity;
using Wallet.Core.Models;
using Wallet.Web.HelpersWeb;
using Wallet.Web.Filters;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddMvc(options =>
{}).AddNewtonsoftJson(options =>
{
    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
});

// Swagger allows you to explore your rest api in a more interactive way.
// To test this functionality, update the launchUrl in Properties/launchSettings.json
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo { Title = "Wallet API V1", Version = "v1" });
    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
    {
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
        Description = "JWT Authorization header using the Bearer scheme."
    });
    options.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme()
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
				}
            },
            new string [] { }
        }
    });
});

builder.Services.ConfigureSqlContext(builder.Configuration);
builder.Services.ConfigureMapping();
builder.Services.RegisterDependencies();
builder.Services.ConfigureIdentity();
builder.Services.ConfigureAuthentication(builder.Configuration);

builder.Services.AddScoped<IUserClaimsPrincipalFactory<User>, CustomClaimsPrincipalFactory>();

DotEnv.Load(options: new DotEnvOptions(probeForEnv: true));

Cloudinary cloudinary = new Cloudinary(Environment.GetEnvironmentVariable("CLOUDINARY_URL"));
cloudinary.Api.Secure = true;
builder.Services.AddScoped(serviceProvider => cloudinary);

Configuration.Default.AddApiKey("api-key", Environment.GetEnvironmentVariable("BREVO_API_KEY"));
var brevoEmailService = new TransactionalEmailsApi();
builder.Services.AddSingleton(serviceProvider => brevoEmailService);

var app = builder.Build();

if (builder.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}


app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapDefaultControllerRoute();

// Enables the endpoint http://localhost:5000/api/swagger
// Use it as an alternative to Postman for sending POST requests
// Use launchSettings.json to change the launch url.
app.UseSwagger();
app.UseSwaggerUI(options =>
{
    options.SwaggerEndpoint("/swagger/v1/swagger.json", "Wallet 1.0 API V1");
    options.RoutePrefix = "api/swagger";
});

app.Run();
