﻿namespace Wallet.Core.QueryParameters
{
    public class AccountQueryParameters
    {
        public string? CurrencyId { get; set; }
        public string? Name { get; set; }
        public string? SortBy { get; set; }
        public string? SortOrder { get; set; }
        
        public string? Keyword { get; set; }

        public int PageSize { get; set; } = 3;
        public int PageNumber { get; set; } = 1;
    }
}
