﻿namespace Wallet.Web.QueryParameters
{
    public class TransactionQueryParameters
    {
        
        public string? SenderUserName { get; set; } 
        public int? SenderUserId { get; set; } 
        public string? RecepientUserName {  get; set; }
        public int? RecepientUserId { get; set; }
        public string? SenderAccountName { get; set; }
        public int? SenderAccountId { get; set; }
        public string? RecepientAccountName { get; set; }
        public int? RecepientAccountId { get; set; }
        public string? TargetCurrencyName { get; set; }
        public string? TargetCurrencyId { get; set; }
        public string? OriginCurrencyName { get; set; }
        public string? OriginCurrencyId { get; set; }
        public string? Description { get; set; }
        public bool? IsInternal { get; set; }

        public int? SenderOrRecepientAccountId { get; set; }

        public string? SortBy { get; set; }
        public string? SortOrder { get; set; }

        public string? Keyword { get; set; }

        public int PageSize { get; set; } = 4; // = userService.GetPageSizeByUserId(id)
        public int PageNumber { get; set; } = 1;

    }
}
