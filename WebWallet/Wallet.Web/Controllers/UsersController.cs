﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Wallet.Core;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Helpers.Еxceptions;
using Wallet.Service.Interfaces;
using Wallet.Services;
using Wallet.Web.Mappers;
using Wallet.Web.ViewModels;

namespace Wallet.Web.Controllers
{
	public class UsersController : Controller
	{
        public const string EmailConfirmationMessage = "To complete your registration, please go to your mailbox and click on the confirmation link!";
        public const string ChangeEmailConfirmationMessage = "To complete the change of your email, please go to your mailbox and click on the confirmation link!";
        private readonly IUsersService _usersService;
		private readonly UserMapper _userMapper;
		private readonly IAvatarService _avatarService;
		private readonly IEmailService _emailService;
        private readonly SignInManager<User> _signInManager;

        public UsersController(IUsersService usersService, UserMapper userMapper, IAvatarService avatarService, IEmailService emailService, SignInManager<User> signInManager)
		{
			this._usersService = usersService;
			this._userMapper = userMapper;
			this._avatarService = avatarService;
			this._emailService = emailService;
			this._signInManager = signInManager;
		}

		[HttpGet]
		public IActionResult Login(string? userName, string emailMessage)
		{
			var viewModel = new LoginViewModel()
			{
				UserName = userName
			};

            this.ViewData["EmailMessage"] = emailMessage;
            
			return this.View(viewModel);
		}


		[HttpPost]
		public async Task<IActionResult> LoginAsync(LoginViewModel viewModel)
		{
			if (!this.ModelState.IsValid)
			{
				return this.View(viewModel);
			}

			try
			{
                var user = await this._usersService.GetByUserNameAsync(viewModel.UserName);
                var result = await _signInManager.PasswordSignInAsync(user, viewModel.Password, viewModel.RememberMe, lockoutOnFailure: true);
                if (result.Succeeded)
				{
					return this.RedirectToAction("Index", "Operations");
				}										

                if (result.IsLockedOut)
                {
                    this.ModelState.AddModelError("Password", ErrorMessages.UserLockedErrorMessage);
                    
					return this.View(viewModel);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ErrorMessages.LoginErrorMessage);
                    
					return this.View(viewModel);
                }
            }
			catch (EntityNotFoundException e)
			{
                this.ModelState.AddModelError("UserName", e.Message);
                return this.View(viewModel);
            }
		}

        [Authorize]
		[HttpGet]
		public async Task<IActionResult> LogoutAsync()
		{
            await _signInManager.SignOutAsync();

            return this.RedirectToAction("Index", "Home");
		}


		[HttpGet]
		public IActionResult Register()
		{
			var viewModel = new RegisterViewModel();

			return this.View(viewModel);
		}


		[HttpPost]
		public async Task<IActionResult> RegisterAsync(RegisterViewModel viewModel)
		{
			if (!this.ModelState.IsValid)
			{
				return this.View(viewModel);
			}
			
			try
			{
				viewModel.ImageFile ??= AvatarService.GeneratePlaceholderAvatar(
					viewModel.FirstName, viewModel.LastName);

				AvatarService.ValidateImage(viewModel.ImageFile);			

				viewModel.AvatarURL = _avatarService.UploadImage(viewModel.ImageFile);

				if (viewModel.AvatarURL == null)
				{
					this.ModelState.AddModelError("ImageFile", ErrorMessages.ImageUploadErrorMessage);

					return this.View(viewModel);
				}

				User user = this._userMapper.MapUserToViewModel(viewModel);
				var result = await this._usersService.RegisterAsync(user, viewModel.Password);
				if (result.Succeeded)
				{
					var createdUser = await this._usersService.GetByUserNameAsync(user.UserName);
					var token = await this._usersService.GenerateEmailConfirmationTokenAsync(createdUser);
					var confirmationLink = Url.Action("ConfirmEmail", "Users", new { token, email = createdUser.Email }, Request.Scheme);
					var emailResponse = await _emailService.SendEmailAsync(createdUser.Email, confirmationLink);

                    return this.RedirectToAction("Login", "Users", new { userName = createdUser.UserName, emailMessage = EmailConfirmationMessage });
                }
				else
				{
					foreach (var error in result.Errors)
					{
						if (error.Code == Errors.UserNameAlreadyExists.Code)
						{
							this.ModelState.AddModelError("UserName", error.Description);
						}
						if (error.Code == Errors.PhoneNumerAlreadyExists.Code)
						{
							this.ModelState.AddModelError("PhoneNumber", error.Description);
						}
					}
					return this.View(viewModel);
				}
			}
			catch (InvalidImageException e)
			{
				this.ModelState.AddModelError("ImageFile", e.Message);

				return this.View(viewModel);
			}
			catch (Exception e)
			{
				this.Response.StatusCode = StatusCodes.Status404NotFound;
				this.ViewData["ErrorMessage"] = e.Message;

				return this.View("Error");
			}			
		}

		[HttpGet]
		public async Task<IActionResult> ConfirmEmailAsync(string token, string email)
		{
			var user = await this._usersService.FindByEmailAsync(email);

			if (user == null)
			{
                this.Response.StatusCode = StatusCodes.Status404NotFound;
                return View("Error");
			}				

			var result = await this._usersService.ConfirmEmailAsync(user, token);

			return View(result.Succeeded ? "ConfirmEmail" : "Error");
		}

        [HttpGet]
        public async Task<IActionResult> ConfirmNewEmailAsync(string token, string newEmail, string oldEmail)
        {
            var user = await this._usersService.FindByEmailAsync(oldEmail);

            if (user == null)
            {
                this.Response.StatusCode = StatusCodes.Status404NotFound;
                return View("Error");
            }

			var result = await this._usersService.ChangeEmailAsync(user, newEmail, token);

			return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [Authorize]
		[HttpGet]
		public async Task<IActionResult> IndexAsync([FromQuery] UserQueryParameters filterParameters)
		{
			try
			{              
                PaginatedList<User> users = await this._usersService.SearchByAsync(filterParameters);
                users.UsersFilterParams = filterParameters;

                return View(users);
            }
            catch (Exception e)
            {
                this.Response.StatusCode = StatusCodes.Status500InternalServerError;
                this.ViewData["ErrorMessage"] = e.Message;

                return View("Error");
            }            			
		}


		[Authorize]
		[HttpGet]
		public async Task<IActionResult> DetailsAsync(int id, string? emailMessage)
		{
			try
			{
				var user = await _usersService.GetByIdAsync(id);
				var currentUser = await this._signInManager.UserManager.GetUserAsync(User);
                if (user.Id != currentUser.Id && !await _usersService.IsInRoleAsync(currentUser, "Admin"))
                {
                    this.Response.StatusCode = StatusCodes.Status403Forbidden;
                    this.ViewData["ErrorMessage"] = ErrorMessages.UserProfileErrorMessage;

                    return View("Error");
                }

				this.ViewData["EmailMessage"] = emailMessage;
                return View(user);
			}
			catch (EntityNotFoundException e)
			{
				this.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
				this.ViewData["ErrorMessage"] = e.Message;
				return View("Error");
			}
			catch (Exception e)
			{
				this.Response.StatusCode = StatusCodes.Status500InternalServerError;
				this.ViewData["ErrorMessage"] = e.Message;

				return View("Error");
			}
		}


		[Authorize]
		[HttpGet]
		public async Task<IActionResult> EditAsync([FromRoute] int id)
		{
			try
			{
                var user = await this._usersService.GetByIdAsync(id);

                var currentUserId = this._signInManager.UserManager.GetUserId(User);
				if(user.Id != Int32.Parse(currentUserId))
				{
                    this.Response.StatusCode = StatusCodes.Status403Forbidden;
                    this.ViewData["ErrorMessage"] = ErrorMessages.UserProfileEditErrorMessage;

					return View("Error");
                }

                var userViewModel = new UserViewModel()
				{
					FirstName = user.FirstName,
					LastName = user.LastName,
					Email = user.Email,
					UserName = user.UserName,
					PhoneNumber = user.PhoneNumber,
					AvatarURL = user.AvatarURL ?? default
				};

				return this.View(userViewModel);
			}
			catch (EntityNotFoundException e)
			{
				this.Response.StatusCode = StatusCodes.Status404NotFound;
				this.ViewData["ErrorMessage"] = e.Message;

				return this.View("Error");
			}
			catch (Exception e)
			{
				this.Response.StatusCode = StatusCodes.Status500InternalServerError;
				this.ViewData["ErrorMessage"] = e.Message;

				return View("Error");
			}
		}

		[Authorize]
		[HttpPost]
		public async Task<IActionResult> EditAsync([FromRoute] int id, UserViewModel viewModel)
		{
			if (!this.ModelState.IsValid)
			{
				return this.View(viewModel);
			}

			try
			{
                var user = await this._usersService.GetByIdAsync(id);

                var currentUserId = this._signInManager.UserManager.GetUserId(User);
                if (user.Id != Int32.Parse(currentUserId))
                {
                    this.Response.StatusCode = StatusCodes.Status403Forbidden;
                    this.ViewData["ErrorMessage"] = ErrorMessages.UserProfileEditErrorMessage;

                    return View("Error");
                }
				
                if (viewModel.ImageFile != null)
				{
					AvatarService.ValidateImage(viewModel.ImageFile);
                  
					var oldAvatar = user.AvatarURL;

                    string imageURL = _avatarService.UploadImage(viewModel.ImageFile);

					if (imageURL == null)
					{
						this.ModelState.AddModelError("ImageFile", ErrorMessages.ImageUploadErrorMessage);

						return this.View(viewModel);
					}

                    _ = _avatarService.DeleteImage(oldAvatar);

                    viewModel.AvatarURL = imageURL;
				}

                var userToUpdate = this._userMapper.MapUserToViewModel(viewModel);				
				var result = await this._usersService.UpdateProfileAsync(id, userToUpdate);

				if (result.Succeeded)
				{
					if (user.Email != viewModel.Email)
					{
						var token = await this._usersService.GenerateChangeEmailTokenAsync(user, viewModel.Email);
                        var confirmationLink = Url.Action("ConfirmNewEmail", "Users", new { token, oldEmail = user.Email, newEmail = viewModel.Email}, Request.Scheme);
						var emailResponse = await _emailService.SendEmailAsync(viewModel.Email, confirmationLink);

						return this.RedirectToAction("Details", "Users", new { id, emailMessage = ChangeEmailConfirmationMessage });
					}

                    return this.RedirectToAction("Details", "Users", new { id });
                }

				foreach (var error in result.Errors)
				{
					ModelState.AddModelError("", error.Description);
				}

				return View(viewModel);
			}
			catch (EntityNotFoundException ex)
			{
				this.Response.StatusCode = StatusCodes.Status404NotFound;
				this.ViewData["ErrorMessage"] = ex.Message;

				return this.View("Error");
			}
			catch (InvalidImageException e)
			{
				this.ModelState.AddModelError("ImageFile", e.Message);

				return this.View(viewModel);
			}
			catch (Exception e)
			{
				this.Response.StatusCode = StatusCodes.Status500InternalServerError;
				this.ViewData["ErrorMessage"] = e.Message;

				return View("Error");
			}
		}


		[Authorize]
		[HttpGet]
		public async Task<IActionResult> ChangePasswordAsync([FromRoute] int id)
		{
			try
			{
				var user = await this._usersService.GetByIdAsync(id);

                var currentUserId = this._signInManager.UserManager.GetUserId(User);
                if (user.Id != Int32.Parse(currentUserId))
                {
                    this.Response.StatusCode = StatusCodes.Status403Forbidden;
                    this.ViewData["ErrorMessage"] = ErrorMessages.PasswordChangeErrorMessage;

                    return this.View("Error");
                }

                var changePasswordViewModel = new ChangePasswordViewModel()
				{
					UserName = user.UserName,
					Password = ""
				};

				return this.View(changePasswordViewModel);
			}
			catch (EntityNotFoundException e)
			{
				this.Response.StatusCode = StatusCodes.Status404NotFound;
				this.ViewData["ErrorMessage"] = e.Message;

				return this.View("Error");
			}
			catch (Exception e)
			{
				this.Response.StatusCode = StatusCodes.Status500InternalServerError;
				this.ViewData["ErrorMessage"] = e.Message;

				return View("Error");
			}
		}

		[Authorize]
		[HttpPost]
		public async Task<IActionResult> ChangePasswordAsync([FromRoute] int id, ChangePasswordViewModel viewModel)
		{
			if (!this.ModelState.IsValid)
			{
				return this.View(viewModel);
			}

			try
			{
                var user = await this._usersService.GetByIdAsync(id);

                var currentUserId = this._signInManager.UserManager.GetUserId(User);
                if (user.Id != Int32.Parse(currentUserId))
                {
                    this.Response.StatusCode = StatusCodes.Status403Forbidden;
                    this.ViewData["ErrorMessage"] = ErrorMessages.PasswordChangeErrorMessage;

                    return this.View("Error");
                }

                var result = await this._usersService.ChangePasswordAsync(id, viewModel.Password, viewModel.NewPassword);

				if (result.Succeeded)
				{
					return this.RedirectToAction("Details", "Users", new { id });
				}

				foreach (var error in result.Errors)
				{
					ModelState.AddModelError("Password", error.Description);
				}

				return View(viewModel);
			}
			catch (EntityNotFoundException e)
			{
				this.Response.StatusCode = StatusCodes.Status404NotFound;
				this.ViewData["ErrorMessage"] = e.Message;

				return this.View("Error");
			}
			catch (Exception e)
			{
				this.Response.StatusCode = StatusCodes.Status500InternalServerError;
				this.ViewData["ErrorMessage"] = e.Message;

				return View("Error");
			}
		}

		[Authorize(Roles = "Admin")]
		[HttpGet]
		public async Task<IActionResult> DeleteAsync([FromRoute] int id)
		{
			try
			{
				var user = await this._usersService.GetByIdAsync(id);

                var currentUserId = this._signInManager.UserManager.GetUserId(User);
                if (user.Id == Int32.Parse(currentUserId))
                {
                    this.Response.StatusCode = StatusCodes.Status403Forbidden;
                    this.ViewData["ErrorMessage"] = ErrorMessages.DeleteUserErrorMessage;

                    return this.View("Error");
                }

                return this.View(user);
			}
			catch (EntityNotFoundException e)
			{
				this.Response.StatusCode = StatusCodes.Status404NotFound;
				this.ViewData["ErrorMessage"] = e.Message;

				return this.View("Error");
			}
			catch (Exception e)
			{
				this.Response.StatusCode = StatusCodes.Status500InternalServerError;
				this.ViewData["ErrorMessage"] = e.Message;

				return View("Error");
			}
		}

		[Authorize(Roles = "Admin")]
		[HttpPost, ActionName("Delete")]
		public async Task<IActionResult> DeleteConfirmedAsync(int id)
		{
			try
			{
				var user = await this._usersService.GetByIdAsync(id);
				var currentUserId = this._signInManager.UserManager.GetUserId(User);
                if (user.Id == Int32.Parse(currentUserId))
                {
                    this.Response.StatusCode = StatusCodes.Status403Forbidden;
                    this.ViewData["ErrorMessage"] = ErrorMessages.DeleteUserErrorMessage;

                    return this.View("Error");
                }

                var result = await this._usersService.DeleteAsync(id);

				if (result.Succeeded)
				{
					return this.RedirectToAction("Index", "Users");
				}

				foreach (var error in result.Errors)
				{
					ModelState.AddModelError("", error.Description);
				}

				return View();
			}
			catch(NonZeroBalanceException e)
			{
                this.Response.StatusCode = StatusCodes.Status409Conflict;
                this.ViewData["ErrorMessage"] = ErrorMessages.DeleteUserAccountErrorMessage;

                return this.View("Error");
            }
			catch (EntityNotFoundException e)
			{
				this.Response.StatusCode = StatusCodes.Status404NotFound;
				this.ViewData["ErrorMessage"] = e.Message;

				return this.View("Error");
			}
			catch (Exception e)
			{
				this.Response.StatusCode = StatusCodes.Status500InternalServerError;
				this.ViewData["ErrorMessage"] = e.Message;

				return View("Error");
			}
		}

		[Authorize(Roles = "Admin")]
		[HttpPost]
		public async Task<IActionResult> BlockAsync(int id)
		{
			try
			{
                var user = await this._usersService.GetByIdAsync(id);

                var currentUserId = this._signInManager.UserManager.GetUserId(User);
                if (user.Id == Int32.Parse(currentUserId))
                {
                    this.Response.StatusCode = StatusCodes.Status403Forbidden;
                    this.ViewData["ErrorMessage"] = ErrorMessages.BlockUserErrorMessage;

                    return this.View("Error");
                }

                var result = await this._usersService.BlockAsync(id);

				if (result.Succeeded)
				{
					return this.RedirectToAction("Index", "Users");
				}

				foreach (var error in result.Errors)
				{
					ModelState.AddModelError("", error.Description);
				}

				return View();
			}
			catch (EntityNotFoundException e)
			{
				this.Response.StatusCode = StatusCodes.Status404NotFound;
				this.ViewData["ErrorMessage"] = e.Message;

				return this.View("Error");

			}
			catch (Exception e)
			{
				this.Response.StatusCode = StatusCodes.Status500InternalServerError;
				this.ViewData["ErrorMessage"] = e.Message;

				return View("Error");
			}
		}

		[Authorize(Roles = "Admin")]
		[HttpPost]
		public async Task<IActionResult> UnblockAsync(int id)
		{
			try
			{
                var user = await this._usersService.GetByIdAsync(id);

                var currentUserId = this._signInManager.UserManager.GetUserId(User);
                if (user.Id == Int32.Parse(currentUserId))
                {
                    this.Response.StatusCode = StatusCodes.Status403Forbidden;
                    this.ViewData["ErrorMessage"] = ErrorMessages.UnblockUserErrorMessage;

                    return this.View("Error");
                }

                var result = await this._usersService.UnblockAsync(id);

				if (result.Succeeded)
				{
					return this.RedirectToAction("Index", "Users");
				}

				foreach (var error in result.Errors)
				{
					ModelState.AddModelError("", error.Description);
				}

				return View();
			}
			catch (EntityNotFoundException e)
			{
				this.Response.StatusCode = StatusCodes.Status404NotFound;
				this.ViewData["ErrorMessage"] = e.Message;

				return this.View("Error");
			}
			catch (Exception e)
			{
				this.Response.StatusCode = StatusCodes.Status500InternalServerError;
				this.ViewData["ErrorMessage"] = e.Message;

				return View("Error");
			}
		}
    }
}
