﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Wallet.Core.Dtos.BankCard;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Helpers.Еxceptions;
using Wallet.Service.Interfaces;
using Wallet.Service.Services;
using Wallet.Web.Mappers;

namespace Wallet.Web.Api
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, User")]
    [Route("api/bankcards")]
    [ApiController]
    public class BankCardsApiController : ControllerBase
    {
        private readonly IBankCardService _bankCardService;
        private readonly IUsersService _usersService;
        private readonly BankCardMapper _bankCardMapper;
        public BankCardsApiController(IBankCardService bankCardService, IUsersService usersService, BankCardMapper bankCardMapper)
        {
            _bankCardService = bankCardService;
            _bankCardMapper = bankCardMapper;
            _usersService = usersService;
        }


        [HttpGet]
        public async Task<IActionResult> GetAllAsync([FromQuery] BankCardQueryParameters parameters)
        {
            try
            {
                User user = await this._usersService.GetByUserNameAsync(User.Identity.Name);
                ICollection<BankCard> queryResult = await this._bankCardService.GetAllAsync(parameters, user);
                HashSet<BankCardOutputDto> cards = new HashSet<BankCardOutputDto>();
                cards = this._bankCardMapper.Map(queryResult, cards);

                return Ok(cards);
            }
            catch (InvalidOperationException ex)
            {

                return BadRequest(ex.Message);
            }
            catch (UnauthorizedOperationException ex)
            {
                return Unauthorized(ex.Message);
            }
        }


        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetByIdAsync([FromRoute] int id)
        {
            try
            {
                User user = await this._usersService.GetByUserNameAsync(User.Identity.Name);
                BankCard card = await _bankCardService.GetByIdAsync(id, user);
                BankCardOutputDto cardOutputDto = this._bankCardMapper.Map(card);

                return Ok(cardOutputDto);
            }
            catch (UnauthorizedOperationException ex)
            {
                return Unauthorized(ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] CreateBankCardDto cardDetails)
        {
            try
            {
                User user = await this._usersService.GetByUserNameAsync(User.Identity.Name);
                cardDetails.UserId = user.Id;
                BankCard cardToCreate = _bankCardMapper.Map(cardDetails);
                BankCard createdCard = await _bankCardService.CreateAsync(cardToCreate, user);
                BankCardOutputDto cardOutputDto = this._bankCardMapper.Map(createdCard);

                return Ok(cardOutputDto);
            }
            catch (DbUpdateException)
            {
                return Conflict(BankCardService.CardAlreadyExistsErrorMessage);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (UnauthorizedOperationException ex)
            {
                return Unauthorized(ex.Message);
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute] int id)
        {
            try
            {
                User user = await this._usersService.GetByUserNameAsync(User.Identity.Name);
                BankCard card = await _bankCardService.DeleteAsync(id, user);

                return Ok();
            }
            catch (EntityNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (UnauthorizedOperationException ex)
            {
                return Unauthorized(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] BankCardUpdateDto cardUpdates)
        {
            try
            {
                User user = await this._usersService.GetByUserNameAsync(User.Identity.Name);
                BankCard card = await _bankCardService.UpdateAsync(cardUpdates, user);
                BankCardOutputDto cardOutputDto = this._bankCardMapper.Map(card);

                return Ok(cardOutputDto);
            }
            catch (EntityNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (UnauthorizedOperationException ex)
            {
                return Unauthorized(ex.Message);
            }
        }
    }
}
