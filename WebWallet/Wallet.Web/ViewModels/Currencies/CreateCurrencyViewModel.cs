﻿using System.ComponentModel.DataAnnotations;
using Wallet.Core;

using static Wallet.Core.Models.Currency;

namespace Wallet.Web.ViewModels.Currencies
{
    public class CreateCurrencyViewModel
    {
        [Required]
        [RegularExpression(CurrencyPattern, ErrorMessage = ErrorMessages.InvalidCurrencyIdErrorMessage)]
        [MaxLength(3)]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
