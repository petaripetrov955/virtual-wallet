﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Wallet.Core;
using Wallet.Core.Dtos.Currency;
using Wallet.Core.Models;
using Wallet.Helpers.Еxceptions;
using Wallet.Service.Interfaces;
using Wallet.Web.Mappers;

namespace Wallet.Web.Api
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/currencies")]
    [ApiController]
    public class CurrenciesApiController : ControllerBase
    {
        private ICurrencyService _currencyService;
        private CurrencyMapper _currencyMapper;
        public CurrenciesApiController(ICurrencyService currencyService, CurrencyMapper currencyMapper)
        {
            _currencyService = currencyService;
            _currencyMapper = currencyMapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            try
            {
                var currencies = await _currencyService.GetAllAsync();

                return Ok(currencies);
            }
            catch (InvalidOperationException ex)
            {
                return Conflict(ex.Message);
            }
        }

        [HttpGet]
        [Route("all/{baseCurrency}")]
        public async Task<IActionResult> GetAllWithRatesAsync(string baseCurrency)
        {
            try
            {
                BaseCurrencyWithRatesDto result = await this._currencyService.GetAllWithRatesAsync(baseCurrency);

                return Ok(result);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (CurrencyBeaconApiException ex)
            {
                return StatusCode(502, ex.Message);
            }
            catch (ArgumentException ex)
            {
                return Conflict(ex.Message);
            }
            catch (Exception)
            {
                return StatusCode(502, ErrorMessages.ExternalCurrencyApiErrorMessage);
            }
        }

        [HttpGet]
        [Route("rate")]
        public async Task<IActionResult> GetByIdWithRatesAsync([FromBody] CurrencyWithRateDto input)
        {
            try
            {
                var result = await this._currencyService.GetByIdWithRate(input.BaseCurrencyCode, input.ComparedToCurrencyCode);

                return Ok(result);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (CurrencyBeaconApiException ex)
            {
                return StatusCode(502, ex.Message);
            }
            catch (ArgumentException ex)
            {
                return Conflict(ex.Message);
            }
            catch (Exception)
            {
                return StatusCode(502, ErrorMessages.ExternalCurrencyApiErrorMessage);
            }
        }

        [HttpGet]
        [Route("{currencyId}")]
        public async Task<IActionResult> GetByIdAsync([FromRoute] string currencyId)
        {
            try
            {
                var currency = await _currencyService.GetByIdAsync(currencyId);

                return Ok(currency);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("convert")]
        public async Task<IActionResult> ConvertAsync([FromBody] CurrencyConversionRequestDto currencyConvertDto)
        {
            try
            {
                ExternalCurrencyConversionResponseDto externalApiCurrencyResponse = await _currencyService.ConvertAsync(currencyConvertDto.ReceivingCurrency, currencyConvertDto.SendingCurrency, currencyConvertDto.AmountInReceivingCurrency);
                CurrencyConversionResultDto convertionResult = this._currencyMapper.Map(externalApiCurrencyResponse);

                return Ok(convertionResult);
            }
            catch (ArgumentException ex)
            {
                return Conflict(ex.Message);
            }

            catch (EntityNotFoundException ex)
            {
                return NotFound($"Currency exchange between '{currencyConvertDto.SendingCurrency}' and '{currencyConvertDto.ReceivingCurrency}' is not supported by our system.");
            }
            catch (CurrencyBeaconApiException ex)
            {
                return StatusCode(502, ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> CreateAsync([FromBody] Currency incomingCurrency)
        {
            try
            {
                Currency createdCurrency = await _currencyService.CreateAsync(incomingCurrency.Id, incomingCurrency.Name);
                CurrencyOutputDto currencyOutputDto = this._currencyMapper.Map(createdCurrency);

                return Ok(currencyOutputDto);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (DuplicateEntityException ex)
            {
                return Conflict(ex.Message);
            }
        }
    }
}
