﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Core.CustomAttributes;

namespace Wallet.Core.Dtos
{
    public class TransferCreateDto
    {        

        [Range(1, int.MaxValue, ErrorMessage = "{0} must be provided.")]
        public int AccountId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "{0} must be provided.")]
        public int BankCardId { get; set; }
       
        [Range(0.01, 1000000.00, ErrorMessage = "Value for {0} must be between {1} and {2} (Message from dto)")]
        [DecimalPrecision(2)]
        public decimal Amount { get; set; }

        public bool IsIncoming { get; set; }
        

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is required.")]
        [MaxLength(256, ErrorMessage = "The '{0}' field must be less than {1} characters.")]
        [MinLength(8, ErrorMessage = "The '{0}' field must be at least {1} character.")]
        public string Description { get; set; }

        

    }
}
