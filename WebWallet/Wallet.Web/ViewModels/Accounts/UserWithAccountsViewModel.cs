﻿namespace Wallet.Web.ViewModels.Accounts
{
    public class UserWithAccountsViewModel
    {
        public UserWithAccountsViewModel()
        {
            this.Accounts = new List<AccountDisplayViewModel>();
        }
        public ICollection<AccountDisplayViewModel> Accounts { get; set; }
    }
}
