﻿using Microsoft.EntityFrameworkCore;
using Wallet.Core.Models;

namespace Wallet.Repo.Data.Seed
{
    static class CurrencyModelBuilderExtension
    {
        public static void SeedCurrencies(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Currency>().HasData
                 (
                new List<Currency>
                {
                  new Currency { Id = "USD", Name = "United States Dollar" },
                 new Currency { Id = "EUR", Name = "Euro" },
                  new Currency { Id = "JPY", Name = "Japanese Yen" },
                   new Currency { Id = "GBP", Name = "British Pound Sterling" },
                    new Currency { Id = "CNY", Name = "Chinese Yuan" },
                     new Currency { Id = "CAD", Name = "Canadian Dollar" },
                      new Currency { Id = "CHF", Name = "Swiss Franc" },
                       new Currency { Id = "AUD", Name = "Australian Dollar" },
                        new Currency { Id = "BGN", Name = "Bulgarian Lev" },
                         new Currency { Id = "RUB", Name = "Russian Ruble" }
               });
        }
    }
}