﻿using System.ComponentModel.DataAnnotations;
using Wallet.Core;

namespace Wallet.Web.ViewModels
{
	public class ChangePasswordViewModel : LoginViewModel
	{
		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		[RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$", ErrorMessage = ErrorMessages.PasswordValidationErrorMessage)]
		public string NewPassword { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessages.EmptyStringErrorMessage)]
		[Compare("NewPassword", ErrorMessage = ErrorMessages.ConfirmPasswordErrorMessage)]
        [RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$", ErrorMessage = ErrorMessages.PasswordValidationErrorMessage)]
        public string ConfirmPassword { get; set; }
	}
}
