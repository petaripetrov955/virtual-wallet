﻿using Wallet.Core.Dtos.Account;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;

namespace Wallet.Service.Interfaces
{
    public interface IAccountService
    {
        Task<Account> GetByIdAsync(int accountId, User user);
        Task<Account> GetByIdAsync(int accountId);
        Task<Account> CreateAsync(Account accountToCreate, User user);
        Task<Account> DeleteAsync(int accountId, User user);
        Task<Account> UpdateAsync(AccountUpdateDto accountUpdates, User user);
        Task<PaginatedList<Account>> SearchByAsync(AccountQueryParameters filterParameters, User user);

        void CheckIfAccountOwner(User user, Account account);
    }
}
