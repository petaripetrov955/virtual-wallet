﻿using System.ComponentModel.DataAnnotations;

namespace Wallet.Core.Models
{
    public class Account
    {
        public const string Name_Regex_Pattern = @"^[A-Z][a-zA-Z\s]*$";
        public const int Close_Required_Amount = 0;
        public Account()
        {
            this.IncomingTransactions = new List<Transaction>();
            this.OutgoingTransactions = new List<Transaction>();
            this.Transfers = new List<Transfer>();
        }
        public int Id { get; set; }


        [Required]
        [RegularExpression(Name_Regex_Pattern, ErrorMessage = ErrorMessages.InvalidAccountNameErrorMessage)]
        public string Name { get; set; }

        [Required]
        public int UserId { get; set; }
        public User User { get; set; }

        [Required]
        public string CurrencyId { get; set; }
        public Currency Currency { get; set; }
        public decimal Balance { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<Transaction> IncomingTransactions { get; set; }
        public ICollection<Transaction> OutgoingTransactions { get; set; }
        public ICollection<Transfer> Transfers { get; set; }
    }
}
