﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Core.QueryParameters;

namespace Wallet.Core.Models
{
    public class PaginatedList<T> : List<T>
    {
        public PaginatedList(List<T> items, int totalPages, int pageNumber, int totalNumberItems = -1)
        {
            this.AddRange(items);
            this.TotalPages = totalPages;
            this.PageNumber = pageNumber;
            this.TotalNumberItems = totalNumberItems;
        }

        public int TotalPages { get; set; }
        public int PageNumber { get; set; }
        public bool HasPrevPage
        {
            get
            {
                return this.PageNumber > 1;
            }
        }
        public bool HasNextPage
        {
            get
            {
                return PageNumber < TotalPages;
            }
        }

        public int TotalNumberItems { get; set; }

        public UserQueryParameters? UsersFilterParams { get; set; }
        public OperationQueryParameters? OperationsFilterParams { get; set; }

        public AccountQueryParameters? AccountFilterParameters { get; set; }
        public bool IsAdmin { get; set; }
    }
}
