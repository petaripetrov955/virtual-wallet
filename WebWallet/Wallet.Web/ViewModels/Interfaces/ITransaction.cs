﻿namespace Wallet.Web.ViewModels.Interfaces
{
    public interface ITransaction
    {
        int SenderAccountId { get; set; }
        int RecepientAccountId { get; set; }
        int RecepientUserId { get; set; }
        decimal TargetAmount { get; set; }
        decimal CurrentExchangeRate { get; set; }
        string Description { get; set; }
    }
}
