﻿using Microsoft.Extensions.FileSystemGlobbing.Internal;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using Wallet.Core;
using Wallet.Core.Dtos.BankCard;
using Wallet.Core.Enums;
using Wallet.Core.Models;
using Wallet.Web.ViewModels.BankCards;

namespace Wallet.Web.Mappers
{
    public class BankCardMapper
    {
        public BankCard Map(CreateBankCardDto cardDetails)
        {
            return new BankCard
            {
                HolderName = cardDetails.HolderName,
                CardNumberEnding = cardDetails.CardNumber.Substring(cardDetails.CardNumber.Length - 4),
                CardNumber = cardDetails.CardNumber,
                CardType = cardDetails.CardType,
                CVV = cardDetails.CVV,
                PIN = cardDetails.PIN,
                ValidThrough = cardDetails.ValidThrough,
                CurrencyId = cardDetails.CurrencyId,
                Name = cardDetails.Name,
                UserId = cardDetails.UserId,
            };
        }

        public BankCardOutputDto Map(BankCard card)
        {
            return new BankCardOutputDto
            {
                Name = card.Name,
                ValidThrough = card.ValidThrough.ToString(@"d", CultureInfo.InvariantCulture),
                HolderName = card.HolderName,
                CardType = Enum.GetName(typeof(CardType), card.CardType),
                CurrencyId = card.CurrencyId,
                CardNumber = new string('*', 10) + card.CardNumberEnding
            };
        }

        public CardDisplayViewModel Map(BankCard card, CardDisplayViewModel cardViewModel)
        {
            cardViewModel.Id = card.Id;
            cardViewModel.ValidThrough = card.ValidThrough.ToString(@"d", CultureInfo.InvariantCulture);
            cardViewModel.HolderName = card.HolderName;
            cardViewModel.CardType = card.CardType;
            cardViewModel.CardNumber = $"* * * *  * * * *  * * * * {card.CardNumberEnding}";

            return cardViewModel;
        }

        public UserWithCardsViewModel Map(ICollection<BankCard> bankCards)
        {
            UserWithCardsViewModel viewModel = new UserWithCardsViewModel();

            foreach (var bankCard in bankCards)
            {
                CardDisplayViewModel cardDisplayViewModel = new CardDisplayViewModel();
                CardDisplayViewModel mappedCardViewModel = this.Map(bankCard, cardDisplayViewModel);
                viewModel.Cards.Add(mappedCardViewModel);
            }

            return viewModel;
        }

        public HashSet<BankCardOutputDto> Map(ICollection<BankCard> bankCards, HashSet<BankCardOutputDto> outputCards)
        {
            foreach (var bankCard in bankCards)
            {
                outputCards.Add(this.Map(bankCard));
            }

            return outputCards;
        }

        public HashSet<CardDisplayViewModel> Map(ICollection<BankCard> bankCards, HashSet<CardDisplayViewModel> outputCards)
        {
            foreach (var bankCard in bankCards)
            {
                CardDisplayViewModel viewModelCard = new CardDisplayViewModel();
                outputCards.Add(this.Map(bankCard, viewModelCard));
            }

            return outputCards;
        }

        public UpdateCardViewModel Map(BankCard card, UpdateCardViewModel viewModel)
        {
            viewModel.Id = card.Id;
            viewModel.Name = card.Name;
            return viewModel;
        }
        public BankCard Map(CreateCardViewModel model, int userId)
        {
            return new BankCard
            {
                Name = model.Name,
                CardNumber = model.CardNumber,
                CardNumberEnding = model.CardNumber.Substring(model.CardNumber.Length - 4),
                ValidThrough = model.ValidThrough,
                HolderName = model.HolderName,
                PIN = model.PIN,
                CVV = model.CVV,
                CardType = model.CardType,
                CurrencyId = model.CurrencyId,
                UserId = userId,
            };
        }
    }
}
