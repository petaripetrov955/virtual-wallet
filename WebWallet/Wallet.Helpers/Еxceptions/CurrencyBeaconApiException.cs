﻿namespace Wallet.Helpers.Еxceptions
{
    public class CurrencyBeaconApiException : ApplicationException
    {
        public CurrencyBeaconApiException(string message) : base(message)
        {
        }
    }
}
