﻿using System.ComponentModel.DataAnnotations;

namespace Wallet.Core.Dtos.BankCard
{
    public class BankCardUpdateDto
    {
        [Required]
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? PIN { get; set; }
    }
}

