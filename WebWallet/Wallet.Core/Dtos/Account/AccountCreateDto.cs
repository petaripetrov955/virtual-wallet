﻿using System.ComponentModel.DataAnnotations;

namespace Wallet.Core.Dtos.Account
{
    public class AccountCreateDto
    {
        [Required]
        [RegularExpression(Models.Account.Name_Regex_Pattern, ErrorMessage = ErrorMessages.InvalidAccountNameErrorMessage)]
        public string Name { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public string CurrencyId { get; set; }
    }
}
