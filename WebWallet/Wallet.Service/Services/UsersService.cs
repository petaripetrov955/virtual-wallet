﻿using Microsoft.AspNetCore.Identity;
using System.Linq;
using Wallet.Core;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Helpers.Еxceptions;
using Wallet.Repo.Interfaces;
using Wallet.Service.Interfaces;

namespace Wallet.Service.Services
{
	public class UsersService : IUsersService
	{
		private readonly IUsersRepository _repository;
        private readonly IAccountRepository _accountRepository;
        private readonly IBankCardRepository _bankCardRepository;

        public UsersService(IUsersRepository repository, IAccountRepository accountRepository, IBankCardRepository bankCardRepository)
		{
			this._repository = repository;
			this._accountRepository = accountRepository;
			this._bankCardRepository = bankCardRepository;
        }

		public async Task<List<User>> GetAllAsync()
		{
			return await this._repository.GetUsersAsync();
		}

		public async Task<List<User>> GetUsersWithAllTransactionsAsync()
		{
			return await this._repository.GetUsersWithAllTransactionsAsync();
		}

		public async Task<int> CountAllAsync()
		{
			return await this._repository.CountAllAsync();
		}

		public async Task<User> GetByIdAsync(int id)
		{
			User user = await this._repository.GetByIdAsync(id);
			return user ?? throw new EntityNotFoundException(string.Format(ErrorMessages.UserIdNotFoundErrorMessage, id));
		}
		public async Task<User> GetByIdWithBankCardsAsync(int id)
		{
			User user = await this._repository.GetByIdWithBankCardsAsync(id);
			return user ?? throw new EntityNotFoundException(string.Format(ErrorMessages.UserIdNotFoundErrorMessage, id));
		}

        public async Task<User> GetByUsernameWithBankCardsAsync(string username)
        {
            User user = await this._repository.GetByUsernameWithBankCardsAsync(username);
            return user ?? throw new EntityNotFoundException(string.Format(ErrorMessages.UserNameNotFoundErrorMessage, username));
        }
        public async Task<User> GetByUsernameWithAccountsAsync(string username)
        {
            User user = await this._repository.GetByUsernameWithAccountsAsync(username);
            return user ?? throw new EntityNotFoundException(string.Format(ErrorMessages.UserNameNotFoundErrorMessage, username));
        }

        public async Task<User> GetByIdWithAccountsAsync(int id)
		{
			User user = await this._repository.GetByIdWithAccountsAsync(id);
			return user ?? throw new EntityNotFoundException(string.Format(ErrorMessages.UserIdNotFoundErrorMessage, id));
		}

		public async Task<User> GetByIdWithAllTransactionsAsync(int id)
		{
			User user = await this._repository.GetByIdWithAllTransactionsAsync(id);
			return user ?? throw new EntityNotFoundException(string.Format(ErrorMessages.UserIdNotFoundErrorMessage, id));
		}

		public async Task<User> GetByUserNameAsync(string userName)
		{
			var user = await this._repository.GetByUserNameAsync(userName);
			return user ?? throw new EntityNotFoundException(string.Format(ErrorMessages.UserNameNotFoundErrorMessage, userName));
		}

        public async Task<IList<string>> GetRolesAsync(User user)
        {
            return await this._repository.GetRolesAsync(user);
        }

        public async Task<bool> IsInRoleAsync(User user, string role)
        {
            return await _repository.IsInRoleAsync(user, role);
        }

        public async Task<IdentityResult> RegisterAsync(User user, string password)
		{
			if ((await this._repository.GetByUserNameAsync(user.UserName)) != null)
			{
				throw new DuplicateEntityException(string.Format(ErrorMessages.DuplicateUserNameErrorMessage, user.UserName));
			}

			if ((await this._repository.GetByPhoneNumberAsync(user.PhoneNumber)) != null)
			{
				throw new DuplicateEntityException(string.Format(ErrorMessages.DuplicatePhoneNumberErrorMessage, user.PhoneNumber));
			}

			var result = await this._repository.RegisterAsync(user, password);

			if (result.Succeeded)
			{
				await this._repository.AddToRoleAsync(user, Constants.Roles.UserRoleName);
			}

			return result;
		}

		public async Task<string> GenerateEmailConfirmationTokenAsync(User user)
		{
			return await this._repository.GenerateEmailConfirmationTokenAsync(user);
		}

        public async Task<string> GenerateChangeEmailTokenAsync(User user, string newEmail)
        {
            return await _repository.GenerateChangeEmailTokenAsync(user, newEmail);
        }

        public async Task<IdentityResult> ConfirmEmailAsync(User user, string token)
		{
			return await this._repository.ConfirmEmailAsync(user, token);
		}
        public async Task<IdentityResult> ChangeEmailAsync(User user, string newEmail, string token)
        {
            return await _repository.ChangeEmailAsync(user, newEmail, token);
        }

        public async Task<User> FindByEmailAsync(string email)
		{
			return await this._repository.FindByEmailAsync(email);
		}

		public async Task<IdentityResult> UpdateProfileAsync(int id, User user)
		{
			User userToUpdate = await this._repository.GetByIdAsync(id);

			var result = await this._repository.UpdateAsync(id, user);

			return result;
		}

		public async Task<IdentityResult> ChangePasswordAsync(int id, string currentPassword, string newPassword)
		{
			var result = await this._repository.ChangePasswordAsync(id, currentPassword, newPassword);

			return result;
		}

		public async Task<IdentityResult> DeleteAsync(int id)
		{
			var user = await this._repository.GetByIdWithAccountsAsync(id);
            var usersBankCards = await this._repository.GetByIdWithBankCardsAsync(id);
            var result = user.Accounts.Where(a => a.Balance > 0).FirstOrDefault();
			if (result != null)
			{
				throw new NonZeroBalanceException(ErrorMessages.DeleteUserAccountErrorMessage);
            }

			foreach(var account in user.Accounts)
			{
				_ = await this._accountRepository.DeleteAsync(account);
            }
			
			foreach (var bankCard in usersBankCards.BankCards)
			{
				_ = await this._bankCardRepository.DeleteAsync(bankCard);
			}

            return await this._repository.DeleteAsync(id);
		}

		public async Task<IdentityResult> BlockAsync(int id)
		{
			User userToBlock = await this._repository.GetByIdAsync(id);

			return await this._repository.BlockAsync(userToBlock);
		}
		public async Task<IdentityResult> UnblockAsync(int id)
		{
			User userToUnblock = await this._repository.GetByIdAsync(id);

			return await this._repository.UnblockAsync(userToUnblock);
		}
		public async Task<PaginatedList<User>> SearchByAsync(UserQueryParameters filterParameters)
		{
			return await this._repository.SearchByAsync(filterParameters);
		}

		public void ValidateUserStatus(User user)
		{
            if (user.IsBlocked)
            {
                throw new UnauthorizedOperationException(ErrorMessages.BlockedUserErrorMessage);
            }

            if (!user.EmailConfirmed)
            {
                throw new EmailNotConfirmedException();
            }
        }

        public bool CheckIfUserHasAccounts(User user)
        {
            if (!user.Accounts.Any())
            {
                return false;
            }

            return true;
        }
    }
}
