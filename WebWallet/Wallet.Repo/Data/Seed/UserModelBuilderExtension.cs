﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Wallet.Core;
using Wallet.Core.Models;

namespace Wallet.Repo.Data.Seed
{
	public static class UserModelBuilderExtension
	{
		private static Role adminRole = new Role(Constants.Roles.AdminRoleId, Constants.Roles.AdminRoleName);
		private static Role userRole = new Role(Constants.Roles.UserRoleId, Constants.Roles.UserRoleName);

		public static void SeedRoles(this ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Role>().HasData(new List<Role>() 
			{
				userRole,
				adminRole
			});
		}

		public static void SeedUsers(this ModelBuilder modelBuilder)
		{
			var passHasher = new PasswordHasher<User>();

			SeedUser(modelBuilder, new User()
			{
				Id = 1,
				FirstName = "System",
				LastName = "Admin",
				Email = "admin@example.org",
				UserName = "admin",
				Password = "123",
				PhoneNumber = "0123456789",
				EmailConfirmed = true,
				AvatarURL = "https://res.cloudinary.com/dcdil2ul5/image/upload/70a5afce-b55e-43b1-a0d5-205b3ef7f623"				
			}, adminRole); ;

			SeedUser(modelBuilder, new User()
			{
				Id = 2,
				FirstName = "Test",
				LastName = "User",
				Email = "user@example.org",
				UserName = "user",
				Password = "123",
				PhoneNumber = "1234567890",
				EmailConfirmed = false,
				AvatarURL = "https://res.cloudinary.com/dcdil2ul5/image/upload/513ee6e3-9bea-4321-ae1e-a389a6c5398f"
			}, userRole);

			SeedUser(modelBuilder, new User()
			{
				Id = 3,
				FirstName = "Georgi",
				LastName = "Georgiev",
				Email = "g.georgiev@email.bg",
				UserName = "georgi",
				Password = "123",
				PhoneNumber = "0000000000",
				EmailConfirmed = true,
				AvatarURL = "https://res.cloudinary.com/dcdil2ul5/image/upload/a55b9809-fa30-4736-a4c4-8939cd437274"
			}, userRole);


			SeedUser(modelBuilder, new User()
			{
				Id = 4,
				FirstName = "Dimiter",
				LastName = "Dimitrov",
				Email = "d.dimitrov@email.bg",
				UserName = "dimiter",
				Password = "123",
				PhoneNumber = "2345678901",
				EmailConfirmed = true,
				AvatarURL = "https://res.cloudinary.com/dcdil2ul5/image/upload/3ad25394-620e-4f99-9253-c431c0300151"
			}, userRole);


			SeedUser(modelBuilder, new User()
			{
				Id = 5,
				FirstName = "Alexander",
				LastName = "Alexandrov",
				Email = "a.alexandrov@email.bg",
				UserName = "alex",
				Password = "123",
				PhoneNumber = "3456789012",
				EmailConfirmed = true,
				IsBlocked = true,
				AvatarURL = "https://res.cloudinary.com/dcdil2ul5/image/upload/72a80b0c-b977-4465-bfdd-dfca6e611eb8"
			}, userRole);

			SeedUser(modelBuilder, new User()
			{
				Id = 6,
				FirstName = "Martin",
				LastName = "Martinov",
				Email = "m.m@email.bg",
				UserName = "martin",
				Password = "123",
				PhoneNumber = "4567890123",
				EmailConfirmed = true,
				AvatarURL = "https://res.cloudinary.com/dcdil2ul5/image/upload/375988f3-d071-4d2b-bfbd-405559fb2864"
			}, userRole);

			SeedUser(modelBuilder, new User()
			{
				Id = 7,
				FirstName = "Iva",
				LastName = "Ivanova",
				Email = "i.ivanova@email.bg",
				UserName = "iivanova",
				Password = "123",
				PhoneNumber = "5678901234",
				EmailConfirmed = true,
				AvatarURL = "https://res.cloudinary.com/dcdil2ul5/image/upload/9fff9730-b80c-490e-8ffc-0f6dd2593701"
			}, userRole);

			SeedUser(modelBuilder, new User()
			{
				Id = 8,
				FirstName = "Svetoslava",
				LastName = "Antonova",
				Email = "s.an@email.bg",
				UserName = "svetla",
				Password = "123",
				PhoneNumber = "6789012345",
				EmailConfirmed = true,
				AvatarURL = "https://res.cloudinary.com/dcdil2ul5/image/upload/4d4b0750-97e4-4e1b-8147-44b6e41d2234"
			}, userRole);

			SeedUser(modelBuilder, new User()
			{
				Id = 9,
				FirstName = "Antonia",
				LastName = "Zagorova",
				Email = "a.z@email.bg",
				UserName = "toni",
				Password = "123",
				PhoneNumber = "7890123456",
				EmailConfirmed = true,
				AvatarURL = "https://res.cloudinary.com/dcdil2ul5/image/upload/ba72a66e-b630-4c11-8d7c-5ef21e28b5ff"
			}, userRole);

			SeedUser(modelBuilder, new User()
			{
				Id = 10,
				FirstName = "Vasil",
				LastName = "Vasilev",
				Email = "v.v@email.bg",
				UserName = "vasko",
				Password = "123",
				PhoneNumber = "8901234567",
				EmailConfirmed = true,
				AvatarURL = "https://res.cloudinary.com/dcdil2ul5/image/upload/3f798ef4-894e-4d41-9982-6231fbd2213f"
			}, userRole);

			SeedUser(modelBuilder, new User()
			{
				Id = 11,
				FirstName = "Kaloyan",
				LastName = "Krumov",
				Email = "k.k@email.bg",
				UserName = "kkrumov",
				Password = "123",
				PhoneNumber = "9012345678",
				EmailConfirmed = true,
				AvatarURL = "https://res.cloudinary.com/dcdil2ul5/image/upload/736dd323-721b-4f59-ade0-123ce07ea5de"
			}, userRole);
		}

		private static void SeedUser(
			this ModelBuilder modelBuilder,
			User user,
			Role role)
		{
			user.NormalizedUserName = user.UserName.ToUpper();
			user.NormalizedEmail = user.Email.ToUpper();
			user.SecurityStamp = Guid.NewGuid().ToString();

			modelBuilder.Entity<User>().HasData(user);
			modelBuilder.Entity<IdentityUserRole<int>>().HasData(
				new IdentityUserRole<int>()
				{
					RoleId = role.Id,
					UserId = user.Id
				}
			);
		}
	}
}
