﻿
namespace Wallet.Core
{
    public static class Constants
    {
        public static class Roles
        {
            public const int AdminRoleId = 1;
            public const string AdminRoleName = "Admin";

            public const int UserRoleId = 2;
            public const string UserRoleName = "User";
        }

        public static class Operations
        {

            public const string Exchange_In_Accounts = "Exchange in Accounts";
            public const string Payment_Between_Users = "Payment between Users";
            public const string Received_Payment = "Received Payment";
            public const string Money_Added = "Money added";
            public const string Pay_To_User = "Pay to User";
            public const string Withdraw_To_BankCard = "Withdrawal to Bank Card";
            public const string Own_Wallets_Exchange = "Internal Exchange between own Wallets:";
            public const string Own_Wallets_Exchange_From_To = "Internal Exchange between own Wallets: {0} -> {1}";
            public const string Exchange_In_Own_Accounts = "Exchange in own Accounts";
            public const string Payment_To_User = "Payment to User";
            public const string Add_Money = "Add Money";
        }

        public static class Labels
        {
            public const string PlusSign = "+";
            public const string MinusSign = "-";
            public const string User = "User:";
            public const string BankCard = "Bank Card:";
            public const string Account = "Account:";
            public const string Error = "Error";
            public const string Index = "Index";
        }

        public static class Formats
        {
            public const string Culture = "en-GB";
            public const string CreatedAtDateTime = "dddd, dd MMMM yyyy HH:mm:ss";
            public const int Decimal_Point_Precision = 4;
        }

        public static class SortingOrder
        {
            public const string Descending = "desc";
            public const string Ascending = "asc";
        }

        public static class Token
        {
            public const string JWT_Config = "JwtConfig";
            public const string Key = "Key";
            public const string Audience = "Audience";
            public const string Issuer = "Issuer";
            public const string Token_Validity_In_Minutes_Label = "TokenValidityInMinutes";
            public const string Refresh_Token_Validity_In_Minutes_Label = "RefreshTokenValidityInMinutes";
        }
    }
}
