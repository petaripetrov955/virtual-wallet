﻿//using Microsoft.AspNetCore.Authentication.JwtBearer;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using System.Net.Mail;
//using Wallet.Core;
//using Wallet.Core.Dtos;
//using Wallet.Core.Models;
//using Wallet.Core.QueryParameters;
//using Wallet.Helpers.Еxceptions;
//using Wallet.Service.Interfaces;
//using Wallet.Web.Mappers;
//using Wallet.Web.QueryParameters;

//namespace Wallet.Web.Api
//{
//    [Route("api/transfers")]
//    [ApiController]
//    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
//    public class TransfersApiController : ControllerBase
//    {
//        private readonly ITransfersService _transfersService;
//        private readonly IUsersService _usersService;
//        private readonly IBankCardService _bankCardService;
//        private readonly IAccountService _accountService;
//        private readonly TransferMapper _transferMapper;

//        public TransfersApiController(ITransfersService transfersService, IUsersService usersService, IBankCardService bankCardService, IAccountService accountService, TransferMapper transferMapper)
//        {
//            this._transfersService = transfersService;
//            this._usersService = usersService;
//            this._bankCardService = bankCardService;
//            this._accountService = accountService;
//            this._transferMapper = transferMapper;

//        }

//        [HttpGet]
//        public async Task<IActionResult> GetAllTransactionsAsync()
//        {
//            try
//            {
//                User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
//                bool IsAdmin = ((List<string>)await _usersService.GetRolesAsync(user)).Contains("Admin");
//                List<Transfer> transfers;
//                if (IsAdmin)
//                {
//                    transfers = await _transfersService.GetAllTransfersAsync();

//                }
//                else
//                {
//                    transfers = await _transfersService.GetTransfersByUserIdAsync(user.Id);
//                }

//                if (!transfers.Any())
//                {
//                    return StatusCode(StatusCodes.Status204NoContent);
//                }
//                List<TransferDispalyDto> transfersShown = transfers.Select(trans => _transferMapper.TransferDislayMap(trans)).ToList();
//                return StatusCode(StatusCodes.Status200OK, transfersShown);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
//            }

//        }

//        [HttpGet]
//        [Route("{id}")]
//        public async Task<IActionResult> GetAsync([FromRoute] int id)
//        {
//            try
//            {
//                string username = Request.HttpContext.User.Identity.Name;
//                User user = await _usersService.GetByUserNameAsync(username);
//                bool IsAdmin = ((List<string>)await _usersService.GetRolesAsync(user)).Contains("Admin");
//                Transfer transfer = await _transfersService.GetTransferByIdAsync(id, user);
//                if (!IsAdmin && transfer.UserId != user.Id)
//                {
//                    return StatusCode(StatusCodes.Status401Unauthorized, "You are not authorized to access this Transfer!");
//                }
//                TransferDispalyDto transferShown = _transferMapper.TransferDislayMap(transfer);
//                return Ok(transferShown);
//            }
//            catch (EntityNotFoundException ex)
//            {
//                return NotFound(ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
//            }
//        }

//        [HttpPost]
//        public async Task<IActionResult> CreateAsync([FromBody] TransferCreateDto transferDto)
//        {
//            try
//            {
//                string username = Request.HttpContext.User.Identity.Name;
//                User user = await _usersService.GetByUserNameAsync(username);
//                if (user.IsBlocked)// && transferDto.IsIncoming)
//                {
//                    return StatusCode(StatusCodes.Status401Unauthorized, ErrorMessages.OperationBlockedUserMessage);
//                }

//                BankCard bankCard = await _bankCardService.GetByIdAsync(transferDto.BankCardId, user); //exception catched when user not owner
//                Account account = await _accountService.GetByIdAsync(transferDto.AccountId);
//                if (account.UserId != user.Id)
//                {
//                    return StatusCode(StatusCodes.Status401Unauthorized, "You are not authorized to access this Account!");
//                }
//                if (bankCard.CurrencyId != account.CurrencyId)
//                {
//                    return StatusCode(StatusCodes.Status406NotAcceptable, "Select a Wallet and a Bankcard with identical Currencies.");
//                }

//                string currencyId = account.CurrencyId;
//                Transfer transferToCreate = _transferMapper.TransferCreateMap(transferDto, currencyId, user.Id);
//                bool IsValidOperation = _transfersService.CheckBalance(transferToCreate, account);
//                if (!IsValidOperation)
//                {
//                    return StatusCode(StatusCodes.Status406NotAcceptable, ErrorMessages.TransferNotValidMessage);
//                }
//                Transfer createdTransfer = await _transfersService.CreateTransferAsync(transferToCreate, user);
//                TransferDispalyDto transferShown = _transferMapper.TransferDislayMap(createdTransfer);
//                return StatusCode(StatusCodes.Status201Created, transferShown);


//            }
//            catch (EntityNotFoundException ex)
//            {
//                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
//            }
//            catch (UnauthorizedOperationException)
//            {
//                return StatusCode(StatusCodes.Status401Unauthorized, "You are not authorized to access this Bank Card!");
//            }
//            catch (CurrencyBeaconApiException ex)
//            {
//                return StatusCode(StatusCodes.Status503ServiceUnavailable, ex.Message);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
//            }
//        }

//        [HttpGet]
//        [Route("search")]
//        public async Task<IActionResult> GetFilterBy([FromQuery] TransferQueryParameters filterParams)
//        {

//            try
//            {
//                string username = Request.HttpContext.User.Identity.Name;
//                User user = await _usersService.GetByUserNameAsync(username);
//                bool IsAdmin = ((List<string>)await _usersService.GetRolesAsync(user)).Contains("Admin");
//                List<Transfer> result = await _transfersService.FilterByAsync(user, IsAdmin, filterParams);
//                if (!result.Any())
//                {
//                    return StatusCode(StatusCodes.Status204NoContent);
//                }
//                List<TransferDispalyDto> resultShown = result.Select(trans => _transferMapper.TransferDislayMap(trans)).ToList();

//                return StatusCode(StatusCodes.Status200OK, resultShown);
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
//            }

//        }


//    }
//}
