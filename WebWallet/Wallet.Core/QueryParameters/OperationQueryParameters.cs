﻿namespace Wallet.Core.QueryParameters
{
    public class OperationQueryParameters
    {
        public string? Type { get; set; }

        public string? From { get; set; }
        public int? OriginAmountMin { get; set; }
        public int? OriginAmountMax { get; set; }
        public string? To { get; set; }
        public int? TargetAmountMin { get; set; }
        public int? TargetAmountMax { get; set; }

        public string? Keyword { get; set; }

        public int? AccountId { get; set; }
        public string? AccountName { get; set; }

        public string? SortBy { get; set; }
        public string? SortOrder { get; set; }

        public int PageSize { get; set; } = 5;
        public int PageNumber { get; set; } = 1;
    }
}
