﻿using Microsoft.AspNetCore.Identity;
using Moq;
using Wallet.Core;
using Wallet.Core.Models;
using Wallet.Helpers.Еxceptions;
using Wallet.Repo.Interfaces;
using Wallet.Service.Services;

namespace Wallet.Tests
{
    #pragma warning disable CS8618
    [TestClass]
    public class UsersServiceTests
    {

        private Mock<IAccountRepository> accountRepositoryMock;
        private Mock<IBankCardRepository> cardRepositoryMock;
        private Mock<IUsersRepository> repositoryMock;

        private UsersService testUsersService;

        [TestInitialize]
        public void InitTest()
        {
            accountRepositoryMock = new Mock<IAccountRepository>();
            cardRepositoryMock = new Mock<IBankCardRepository>();
            repositoryMock = new Mock<IUsersRepository>();

            testUsersService = new UsersService(repositoryMock.Object, accountRepositoryMock.Object, cardRepositoryMock.Object);
        }

        [TestMethod]
        public async Task GetById_Should_ReturnCorrectUser_When_ParamsAreValid()
        {
            // Arrange        
            User expectedUser = TestHelper.GetTestUser();

            repositoryMock
                .Setup(repo => repo.GetByIdAsync(expectedUser.Id))
                .Returns(Task.FromResult<User>(expectedUser));

            // Act
            var actualUser = await testUsersService.GetByIdAsync(expectedUser.Id);

            // Assert
            Assert.AreEqual(expectedUser.Id, actualUser.Id);
            Assert.AreEqual(expectedUser.FirstName, actualUser.FirstName);
            Assert.AreEqual(expectedUser.LastName, actualUser.LastName);
            Assert.AreEqual(expectedUser.UserName, actualUser.UserName);
            Assert.AreEqual(expectedUser.Email, actualUser.Email);
            Assert.AreEqual(expectedUser.IsBlocked, actualUser.IsBlocked);
            Assert.AreEqual(expectedUser.IsDeleted, actualUser.IsDeleted);
            Assert.AreEqual(expectedUser.PhoneNumber, actualUser.PhoneNumber);
            Assert.AreEqual(expectedUser.AvatarURL, actualUser.AvatarURL);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public async Task GetById_Should_ThrowException_When_UserNotFound()
        {
            // Arrange

            repositoryMock
                .Setup(repo => repo.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<User>(null));

            // Act

            await testUsersService.GetByIdAsync(1);
        }

        [TestMethod]
        public async Task GetByUserName_Should_ReturnCorrectUser_When_ParamsAreValid()
        {
            // Arrange        
            User expectedUser = TestHelper.GetTestUser();

            repositoryMock
                .Setup(repo => repo.GetByUserNameAsync(expectedUser.UserName))
                .Returns(Task.FromResult<User>(expectedUser));

            // Act
            var actualUser = await testUsersService.GetByUserNameAsync(expectedUser.UserName);

            // Assert
            Assert.AreEqual(expectedUser.Id, actualUser.Id);
            Assert.AreEqual(expectedUser.FirstName, actualUser.FirstName);
            Assert.AreEqual(expectedUser.LastName, actualUser.LastName);
            Assert.AreEqual(expectedUser.UserName, actualUser.UserName);
            Assert.AreEqual(expectedUser.Email, actualUser.Email);
            Assert.AreEqual(expectedUser.IsBlocked, actualUser.IsBlocked);
            Assert.AreEqual(expectedUser.IsDeleted, actualUser.IsDeleted);
            Assert.AreEqual(expectedUser.PhoneNumber, actualUser.PhoneNumber);
            Assert.AreEqual(expectedUser.AvatarURL, actualUser.AvatarURL);
        }


        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public async Task GetByUserName_Should_ThrowException_When_UserNotFound()
        {
            // Arrange

            repositoryMock
                .Setup(repo => repo.GetByUserNameAsync("testuser"))
                .Returns(Task.FromResult<User>(null));

            // Act

            await testUsersService.GetByUserNameAsync("testuser");
        }

        [TestMethod]
        public async Task Register_Should_ReturnCorrectUser_When_ParamsAreValid()
        {
            // Arrange        
            User expectedUser = TestHelper.GetTestUser();

            repositoryMock
                .Setup(repo => repo.GetByUserNameAsync("testuser"))
                .Returns(Task.FromResult<User>(null));

            repositoryMock
               .Setup(repo => repo.GetByPhoneNumberAsync("1234567890"))
               .Returns(Task.FromResult<User>(null));

            repositoryMock
               .Setup(repo => repo.RegisterAsync(expectedUser, "123"))
               .Returns(Task.FromResult(IdentityResult.Success));

            // Act
            var result = await testUsersService.RegisterAsync(expectedUser, "123");

            // Assert
            Assert.IsTrue(result.Succeeded);
        }


        [TestMethod]
        [ExpectedException(typeof(DuplicateEntityException))]
        public async Task Register_Should_ThrowException_When_UserNameExist()
        {
            var testUser = TestHelper.GetTestUser();
            // Arrange

            repositoryMock
                .Setup(repo => repo.GetByUserNameAsync(testUser.UserName))
                .ReturnsAsync(testUser);

            await testUsersService.RegisterAsync(testUser, "123");
        }

        [TestMethod]
        [ExpectedException(typeof(DuplicateEntityException))]
        public async Task Register_Should_ThrowException_When_PhoneNumberExist()
        {
            var testUser = TestHelper.GetTestUser();
            // Arrange

            repositoryMock
                .Setup(repo => repo.GetByUserNameAsync(testUser.UserName))
                .Returns(Task.FromResult<User>(null));

            repositoryMock
                .Setup(repo => repo.GetByPhoneNumberAsync(testUser.PhoneNumber))
                .ReturnsAsync(testUser);

            // Act

            await testUsersService.RegisterAsync(testUser, "123");
        }

        [TestMethod]
        public async Task UpdateProfile_Should_ReturnSuccess_When_ParamsAreValid()
        {
            var testUser = TestHelper.GetTestUser();

            repositoryMock
              .Setup(repo => repo.GetByIdAsync(testUser.Id))
              .ReturnsAsync(testUser);

            repositoryMock
                .Setup(repo => repo.UpdateAsync(It.IsAny<int>(), It.IsAny<User>()))
                .Returns(Task.FromResult(IdentityResult.Success));

            // Act
            var result = await testUsersService.UpdateProfileAsync(testUser.Id, testUser);

            // Assert
            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task UpdateProfile_Should_ReturnFailure_When_UserDoesNotExist()
        {
            var testUser = TestHelper.GetTestUser();

            repositoryMock
              .Setup(repo => repo.GetByIdAsync(testUser.Id))
              .Returns(Task.FromResult<User>(null));

            repositoryMock
                .Setup(repo => repo.UpdateAsync(It.IsAny<int>(), It.IsAny<User>()))
                .Returns(Task.FromResult(IdentityResult.Failed()));

            // Act
            var result = await testUsersService.UpdateProfileAsync(123, testUser);

            // Assert
            Assert.IsFalse(result.Succeeded);
        }

        [TestMethod]
        public async Task BlockUser_Should_ReturnSuccess_When_ParamsAreValid()
        {
            var testUser = TestHelper.GetTestUser();

            repositoryMock
              .Setup(repo => repo.GetByIdAsync(testUser.Id))
              .ReturnsAsync(testUser);

            repositoryMock
                .Setup(repo => repo.BlockAsync(It.IsAny<User>()))
                .Returns(Task.FromResult(IdentityResult.Success));

            // Act
            var result = await testUsersService.BlockAsync(testUser.Id);

            // Assert
            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task BlockUser_Should_ReturnFailure_When_UserDoesNotExist()
        {
            var testUser = TestHelper.GetTestUser();

            repositoryMock
              .Setup(repo => repo.GetByIdAsync(testUser.Id))
              .Returns(Task.FromResult<User>(null));

            repositoryMock
                .Setup(repo => repo.BlockAsync(It.IsAny<User>()))
                .Returns(Task.FromResult(IdentityResult.Failed()));

            // Act
            var result = await testUsersService.BlockAsync(123);

            // Assert
            Assert.IsFalse(result.Succeeded);
        }

        [TestMethod]
        public async Task UnblockUser_Should_ReturnSuccess_When_ParamsAreValid()
        {
            var testUser = TestHelper.GetTestUser();

            repositoryMock
              .Setup(repo => repo.GetByIdAsync(testUser.Id))
              .ReturnsAsync(testUser);

            repositoryMock
                .Setup(repo => repo.UnblockAsync(It.IsAny<User>()))
                .Returns(Task.FromResult(IdentityResult.Success));

            // Act
            var result = await testUsersService.UnblockAsync(testUser.Id);

            // Assert
            Assert.IsTrue(result.Succeeded);
        }

        [TestMethod]
        public async Task UnblockUser_Should_ReturnFailure_When_UserDoesNotExist()
        {
            var testUser = TestHelper.GetTestUser();

            repositoryMock
              .Setup(repo => repo.GetByIdAsync(testUser.Id))
              .Returns(Task.FromResult<User>(null));

            repositoryMock
                .Setup(repo => repo.UnblockAsync(It.IsAny<User>()))
                .Returns(Task.FromResult(IdentityResult.Failed()));

            // Act
            var result = await testUsersService.UnblockAsync(123);

            // Assert
            Assert.IsFalse(result.Succeeded);
        }

        [TestMethod]
        public async Task CountAllAsync_Should_Return_NumberOfAllUsers()
        {
            // Arrange        
            var testUsers = TestHelper.GetTestUsers();

            repositoryMock
                .Setup(repo => repo.CountAllAsync())
                .Returns(Task.FromResult<int>(testUsers.Count));

            var expected = testUsers.Count;

            //Act
            var actual = await testUsersService.CountAllAsync();

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(NonZeroBalanceException))]
        public async Task Delete_Should_ThrowException_When_AccountHasMoney()
        {
            // Arrange
            var testUser = TestHelper.GetTestUser();

            repositoryMock
              .Setup(repo => repo.GetByIdWithAccountsAsync(testUser.Id))
              .ReturnsAsync(testUser);

            repositoryMock
              .Setup(repo => repo.GetByIdWithBankCardsAsync(testUser.Id))
              .ReturnsAsync(testUser);

            repositoryMock
               .Setup(repo => repo.DeleteAsync(testUser.Id))
               .Throws(new NonZeroBalanceException(ErrorMessages.DeleteUserAccountErrorMessage));

            await testUsersService.DeleteAsync(testUser.Id);

        }

    }
}
