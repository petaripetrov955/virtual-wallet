﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Wallet.Core.Models;

namespace Wallet.Repo.Data.EntityConfigurations
{
    internal class AccountEntityConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.HasMany(a => a.IncomingTransactions)
                   .WithOne(transaction => transaction.RecepientAccount)
                   .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(a => a.OutgoingTransactions)
                  .WithOne(transaction => transaction.SenderAccount)
                  .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(a => a.Transfers)
                  .WithOne(transfer => transfer.Account)
                  .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
