﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Core.CustomAttributes;

namespace Wallet.Core.Dtos
{
    public class TransactionExternalCreateDto
    {
        
        public int SenderAccountId { get; set; }

        public int RecepientUserId { get; set; }
        

        [Range(0.01, 1000000.00, ErrorMessage = "Value for {0} must be between {1} and {2} (Message from Transaction-Class)")]
        [DecimalPrecision(2)]
        public decimal TargetAmount { get; set; }

        public string TargetCurrencyId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description is required (Message from Transaction-Class)")]
        [MaxLength(256, ErrorMessage = "The '{0}' field must be less than {1} characters. (Message from Transaction-Class)")]
        [MinLength(8, ErrorMessage = "The '{0}' field must be at least {1} character. (Message from Transaction-Class)")]
        public string Description { get; set; }      
        

        
    }
}
