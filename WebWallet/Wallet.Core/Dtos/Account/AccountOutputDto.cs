﻿namespace Wallet.Core.Dtos.Account
{
    public class AccountOutputDto
    {
        public string Name { get; set; }
        public string CurrencyId { get; set; }
        public decimal Balance { get; set; }
    }
}
