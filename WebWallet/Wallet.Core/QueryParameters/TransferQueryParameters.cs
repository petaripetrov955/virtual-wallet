﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wallet.Core.QueryParameters
{
    public class TransferQueryParameters
    {
        public string? UserName {  get; set; } 
        public int? UserId { get; set; }
        public string? AccountName { get; set; }
        public int? AccountId { get; set; }
        public string? BankCardName { get; set; }
        public int? BankCardId { get; set; }
        public string? CurrencyName { get; set; }
        public string? CurrencyId { get; set; }
        public bool? IsIncoming { get; set; }
        public string? Description { get; set; }

        public string? SortBy { get; set; }
        public string? SortOrder { get; set; }

        public string? Keyword { get; set; }

        public int PageSize { get; set; } = 4; // = userService.GetPageSizeByUserId(id)
        public int PageNumber { get; set; } = 1;

    }
}
