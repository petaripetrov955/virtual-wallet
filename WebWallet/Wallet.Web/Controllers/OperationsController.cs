﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Wallet.Core;
using Wallet.Core.Dtos;
using Wallet.Core.Dtos.Currency;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Service.Interfaces;
using Wallet.Core.Enums;
using Wallet.Web.Filters;
using Wallet.Web.HelpersWeb;
using Wallet.Web.Mappers;
using Wallet.Web.QueryParameters;
using Wallet.Web.ViewModels;
using Wallet.Web.ViewModels.Interfaces;

using Account = Wallet.Core.Models.Account;

namespace Wallet.Web.Controllers
{
    [Authorize]
    [CustomExceptionFilterAttribute]
    public class OperationsController : Controller
    {
        private const string Controller_Name = "Operations";

        private readonly ITransfersService _transfersService;
        private readonly ITransactionsService _transactionsService;
        private readonly IUsersService _usersService;
        private readonly IAccountService _accountService;
        private readonly ICurrencyService _currencyService;
        private readonly OperationMapper _operationMapper;
        private readonly TransferMapper _transferMapper;
        private readonly TransactionMapper _transactionMapper;
        public OperationsController(ITransfersService transfersService, ITransactionsService transactionsService, IUsersService usersService, OperationMapper operationMapper, TransferMapper transferMapper, IAccountService accountService, ICurrencyService currencyService, TransactionMapper transactionMapper)
        {
            _transfersService = transfersService;
            _transactionsService = transactionsService;
            _usersService = usersService;
            _operationMapper = operationMapper;
            _transferMapper = transferMapper;
            _transactionMapper = transactionMapper;
            _accountService = accountService;
            _currencyService = currencyService;
        }

        [HttpGet]
        public async Task<IActionResult> Index([FromQuery] OperationQueryParameters filterParams)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            bool isAdmin = await _usersService.IsInRoleAsync(user, Constants.Roles.AdminRoleName);
            List<Transfer> transfers;
            List<Transaction> transactions;
            if (filterParams.AccountId != null)
            {
                int accountId = (int)filterParams.AccountId;
                Account account = await _accountService.GetByIdAsync(accountId, user);
                filterParams.AccountName = account.Name;
                TransferQueryParameters tarnsferParam = new TransferQueryParameters()
                {
                    AccountId = filterParams.AccountId
                };

                transfers = await _transfersService.FilterByAsync(user, isAdmin, tarnsferParam);
                TransactionQueryParameters transactionParam = new TransactionQueryParameters()
                {
                    SenderOrRecepientAccountId = filterParams.AccountId
                };

                transactions = await _transactionsService.FilterByAsync(user, isAdmin, transactionParam);
            }
            else
            {
                transfers = await _transfersService.FilterByAsync(user, isAdmin);
                transactions = await _transactionsService.FilterByAsync(user, isAdmin);
            }

            List<OperationsViewModel> operations = _operationMapper.OperationsMap(transfers, transactions, user, isAdmin);
            PaginatedList<OperationsViewModel> operationsModel = await OperationsSearch.FilterBy(operations, filterParams);
            operationsModel.OperationsFilterParams = filterParams;
            operationsModel.IsAdmin = isAdmin ? true : operationsModel.IsAdmin;

            return View(operationsModel);
        }

        [HttpGet]
        public async Task<IActionResult> Details(int id, string type, bool isCreated = false)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            bool isAdmin = await _usersService.IsInRoleAsync(user, Constants.Roles.AdminRoleName);
            OperationDetailsViewModel viewModel = new();
            OperationType operationType = type is null ? OperationType.None : Enum.Parse<OperationType>(type);
            if (operationType == OperationType.Transfer)
            {
                Transfer transfer = await _transfersService.GetTransferByIdAsync(id, user);
                Account account = await _accountService.GetByIdAsync(transfer.AccountId, user);
                viewModel = _operationMapper.OperationDetailsTransferMap(transfer, user, isAdmin);
                viewModel.IsCreated = isCreated;
                viewModel.IsIncoming = transfer.IsIncoming;
                viewModel.CurrentBalance = account.Balance;

                return View(viewModel);
            }
            else if (operationType == OperationType.Transaction)
            {
                Transaction transaction = await _transactionsService.GetTransactionByIdAsync(id, user);
                User senderUser = await this._usersService.GetByIdAsync(transaction.SenderUserId);
                Account senderAccount = await _accountService.GetByIdAsync(transaction.SenderAccountId, senderUser);
                User recepientUser = await this._usersService.GetByIdAsync(transaction.RecepientUserId);
                Account recepientAccount = await _accountService.GetByIdAsync(transaction.RecepientAccountId, recepientUser);
                viewModel = _operationMapper.OperationDetailsTransactionMap(transaction, user, isAdmin);
                viewModel.IsCreated = isCreated;
                viewModel.IsInternal = transaction.IsInternal;
                viewModel.CurrentBalance = senderAccount.Balance;
                viewModel.TargetCurrentBalance = recepientAccount.Balance;

                return View(viewModel);
            }
            else
            {
                return RedirectToAction(nameof(Index), Constants.Labels.Error, new { id = 404, message = String.Format(ErrorMessages.InvalidOperation, type) });
            }
        }

        [HttpGet]
        public async Task<IActionResult> CreateTransferIncoming()
        {
            User user = await _usersService.GetByUsernameWithBankCardsAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            var viewModel = await CreateTransferModel(user, true);

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmTransferIncoming(CreateTransferViewModel viewModel)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            viewModel = ConfirmTransfer(viewModel, Constants.Operations.Add_Money,
                              $"{Constants.Labels.BankCard} {viewModel.BankCardName}",
                                $"{Constants.Labels.Account} {viewModel.AccountName}");

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> CreateTransferIncoming(CreateTransferViewModel viewModel)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            if (!ModelState.IsValid)
            {
                return RedirectToAction(nameof(this.ConfirmTransferIncoming), Controller_Name, viewModel);
            }

            Transfer transferToCreate = _transferMapper.TransferCreateMap(viewModel);
            Transfer createdTransfer = await _transfersService.CreateTransferAsync(transferToCreate, user);

            return this.RedirectToAction(nameof(Details), Controller_Name, new { id = createdTransfer.Id, type = nameof(Transfer), isCreated = true });
        }

        [HttpGet]
        public async Task<IActionResult> CreateTransferNotIncoming()
        {
            User user = await _usersService.GetByUsernameWithBankCardsAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            var viewModel = await CreateTransferModel(user, false);

            return View(viewModel);
        }

        private async Task<CreateTransferViewModel> CreateTransferModel(User user, bool isIncoming)
        {
            CreateTransferViewModel viewModel = new();
            viewModel.UserId = user.Id;
            ICollection<BankCard> bankCards = user.BankCards;
            if (!bankCards.Any())
            {
                viewModel.HasBankCards = false;
            }

            user = await _usersService.GetByIdWithAccountsAsync(user.Id);
            viewModel.HasAccounts = this._usersService.CheckIfUserHasAccounts(user);
            viewModel.IsIncoming = isIncoming;

            return _operationMapper.TransferViewModelCollectionsMap(bankCards, user.Accounts, viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmTransferNotIncoming(CreateTransferViewModel viewModel)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            viewModel = ConfirmTransfer(viewModel, Constants.Operations.Withdraw_To_BankCard,
                                 $"{Constants.Labels.Account} {viewModel.AccountName}",
                                $"{Constants.Labels.BankCard} {viewModel.BankCardName}");

            return View(viewModel);
        }

        private CreateTransferViewModel ConfirmTransfer(CreateTransferViewModel viewModel, string operationType, string fundsFrom, string fundsTo)
        {
            viewModel.Type = operationType;
            viewModel.From = fundsFrom;
            viewModel.To = fundsTo;
            if (!this.ModelState.IsValid)
            {
                return viewModel;
            }

            viewModel.IsConfirmed = true;

            return viewModel;
        }

        [HttpPost]
        public async Task<IActionResult> CreateTransferNotIncoming(CreateTransferViewModel viewModel)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            if (!ModelState.IsValid)
            {
                return RedirectToAction(nameof(this.ConfirmTransferNotIncoming), Controller_Name, viewModel);
            }

            Transfer transferToCreate = _transferMapper.TransferCreateMap(viewModel);
            Transfer createdTransfer = await _transfersService.CreateTransferAsync(transferToCreate, user);

            return this.RedirectToAction(nameof(Details), Controller_Name, new { id = createdTransfer.Id, type = nameof(Transfer), isCreated = true });
        }

        [HttpGet]
        public async Task<IActionResult> CreateSimilarTransfer(int id)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            Transfer transfer = await _transfersService.GetTransferByIdAsync(id, user);
            CreateSimilarTransferViewModel viewModel = _operationMapper.SimilarTransferCreateMap(transfer);
            viewModel.CurrentBalance = transfer.Account.Balance;

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmSimilarTransfer(CreateSimilarTransferViewModel viewModel)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            if (!this.ModelState.IsValid)
            {
                return this.View(viewModel);
            }

            viewModel.IsConfirmed = true;

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> CreateSimilarTransfer(CreateSimilarTransferViewModel viewModel)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            if (!this.ModelState.IsValid)
            {
                return RedirectToAction(nameof(this.ConfirmSimilarTransfer), Controller_Name, viewModel);
            }

            Transfer transferToCreate = _transferMapper.TransferCreateMap(viewModel);
            Transfer createdTransfer = await _transfersService.CreateTransferAsync(transferToCreate, user);

            return this.RedirectToAction(nameof(Details), Controller_Name, new { id = createdTransfer.Id, type = nameof(Transfer), isCreated = true });
        }

        [HttpGet]
        public async Task<IActionResult> CreateSimilarTransaction(int id)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            Transaction transaction = await _transactionsService.GetTransactionByIdAsync(id, user);
            if (user.Id != transaction.SenderUserId)
            {
                return RedirectToAction(Constants.Labels.Index, Constants.Labels.Error, new { id = 401 });
            }

            if (transaction.SenderAccount.IsDeleted || transaction.RecepientAccount.IsDeleted)
            {
                return RedirectToAction(Constants.Labels.Index, Constants.Labels.Error, new { id = 404 });
            }

            CreateSimilarTransactionViewModel viewModel = _operationMapper.SimilarTransactionCreateMap(transaction);
            viewModel.CurrentBalance = transaction.SenderAccount.Balance;
            viewModel.CurrentExchangeRate = await SetExchangeRate(transaction.OriginCurrencyId, transaction.TargetCurrencyId);
            if (transaction.IsInternal)
            {
                viewModel.CurrentTargetBalance = transaction.RecepientAccount.Balance;
            }

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmSimilarTransaction(CreateSimilarTransactionViewModel viewModel)
        {
            User sender = await _usersService.GetByUserNameAsync(User.Identity.Name);
            User recepient = await _usersService.GetByIdAsync(viewModel.RecepientUserId);
            this._usersService.ValidateUserStatus(sender);
            this._usersService.ValidateUserStatus(recepient);
            Account senderAccount = await _accountService.GetByIdAsync(viewModel.SenderAccountId, sender);
            Account recepientAccount = await _accountService.GetByIdAsync(viewModel.RecepientAccountId, recepient);
            viewModel.OriginAmount = viewModel.CalculatedOrigin;
            if (!this.ModelState.IsValid)
            {
                return this.View(viewModel);
            }

            Transaction transactionToCreate;
            if (viewModel.IsInternal)
            {
                if (senderAccount.UserId != sender.Id || recepientAccount.UserId != sender.Id)
                {
                    return RedirectToAction(Constants.Labels.Index, Constants.Labels.Error, new { id = 406, message = ErrorMessages.AccountIdUserIdMismatchMessage });
                }

                TransactionInternalCreateDto internalCreateDto = this._transactionMapper.TransactionInternalCreateDtoMap(viewModel);
                ExternalCurrencyConversionResponseDto exchangeDto = await _currencyService.ConvertAsync(viewModel.TargetCurrencyId, viewModel.OriginCurrencyId, viewModel.TargetAmount);
                transactionToCreate = _transactionMapper.TransactionInternalCreateMap(internalCreateDto, exchangeDto, sender.Id);
            }
            else
            {
                if (senderAccount.UserId != sender.Id || recepientAccount.UserId != viewModel.RecepientUserId)
                {
                    return RedirectToAction(Constants.Labels.Index, Constants.Labels.Error, new { id = 406, message = ErrorMessages.AccountIdUserIdMismatchMessage });
                }

                TransactionExternalCreateDto externalCreateDto = this._transactionMapper.TransactionExternalCreateDtoMap(viewModel);
                transactionToCreate = await CreateTransactionBasedOnCurrencyExchange(viewModel, externalCreateDto, viewModel.SenderAccountId, viewModel.TargetCurrencyId);
            }

            viewModel.IsConfirmed = true;

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> CreateSimilarTransaction(CreateSimilarTransactionViewModel viewModel)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            if (!this.ModelState.IsValid)
            {
                return RedirectToAction(nameof(ConfirmSimilarTransaction), Controller_Name, viewModel);
            }

            Transaction transactionToCreate;
            Transaction createdTransaction;
            if (viewModel.IsInternal)
            {
                TransactionInternalCreateDto internalCreateDto = this._transactionMapper.TransactionInternalCreateDtoMap(viewModel);
                ExternalCurrencyConversionResponseDto exchangeDto = await _currencyService.ConvertAsync(viewModel.TargetCurrencyId, viewModel.OriginCurrencyId, viewModel.TargetAmount);
                transactionToCreate = _transactionMapper.TransactionInternalCreateMap(internalCreateDto, exchangeDto, user.Id);
            }
            else
            {
                TransactionExternalCreateDto externalCreateDto = this._transactionMapper.TransactionExternalCreateDtoMap(viewModel);
                transactionToCreate = await CreateTransactionBasedOnCurrencyExchange(viewModel, externalCreateDto, viewModel.SenderAccountId, viewModel.TargetCurrencyId);
            }

            createdTransaction = await _transactionsService.CreateTransactionAsync(transactionToCreate, user);

            return RedirectToAction(nameof(Details), Controller_Name, new { id = createdTransaction.Id, type = nameof(Transaction), isCreated = true });
        }

        [HttpGet]
        public async Task<RedirectToActionResult> CreateOperationDistributor(int id)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            switch (id)
            {
                case 1:
                    return RedirectToAction(nameof(CreateTransferIncoming), Controller_Name);
                case 2:
                    return RedirectToAction(nameof(CreateTransferNotIncoming), Controller_Name);
                case 3:
                    return RedirectToAction(nameof(CreateExchange), Controller_Name);
                case 4:
                    return RedirectToAction("", "Users");
                default:
                    return RedirectToAction(nameof(Index), Controller_Name);
            }
        }

        [HttpGet]
        public IActionResult ConfirmYourUser()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> CreateExchange()
        {
            User user = await _usersService.GetByUsernameWithAccountsAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            ExchangeViewModel viewModel = new ExchangeViewModel();
            viewModel.SenderUserId = user.Id;
            viewModel.RecepientUserId = user.Id;
            viewModel.HasAccounts = this._usersService.CheckIfUserHasAccounts(user);
            viewModel.IsInternal = true;
            viewModel = _operationMapper.TransactionExchangeViewModelCreate(user.Accounts, viewModel);

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> ProceedExchange(ExchangeViewModel viewModel)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            Account senderAccount = await _accountService.GetByIdAsync(viewModel.SenderAccountId, user);
            Account recepientAccount = await _accountService.GetByIdAsync(viewModel.RecepientAccountId, user);
            viewModel.Type = Constants.Operations.Exchange_In_Own_Accounts;
            viewModel.From = $"{Constants.Labels.Account} {senderAccount.Name}";
            viewModel.OriginCurrencyId = senderAccount.CurrencyId;
            viewModel.CurrentOriginBalance = senderAccount.Balance;
            viewModel.To = $"{Constants.Labels.Account} {recepientAccount.Name}";
            viewModel.TargetCurrencyId = recepientAccount.CurrencyId;
            viewModel.CurrentTargetBalance = recepientAccount.Balance;
            viewModel.CurrentExchangeRate = await SetExchangeRate(senderAccount.CurrencyId, recepientAccount.CurrencyId);
            viewModel.Description = String.Format(Constants.Operations.Own_Wallets_Exchange_From_To, senderAccount.CurrencyId, recepientAccount.CurrencyId);
            viewModel.TargetAmount = Transaction.Default_Target_Amount;

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmExchange(ExchangeViewModel viewModel)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            viewModel.OriginAmount = viewModel.CalculatedOrigin;
            Account senderAccount = await _accountService.GetByIdAsync(viewModel.SenderAccountId, user);
            Account recepientAccount = await _accountService.GetByIdAsync(viewModel.RecepientAccountId, user);
            if (!this.ModelState.IsValid)
            {
                return View(viewModel);
            }

            viewModel.CurrentExchangeRate = await SetExchangeRate(senderAccount.CurrencyId, recepientAccount.CurrencyId);
            viewModel.IsConfirmed = true;

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> CreateExchange(ExchangeViewModel viewModel)
        {
            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            if (!this.ModelState.IsValid)
            {
                viewModel.IsConfirmed = false;
                return RedirectToAction(nameof(ConfirmExchange), Controller_Name, viewModel);
            }

            TransactionInternalCreateDto internalCreateDto = this._transactionMapper.TransactionInternalCreateDtoMap(viewModel);
            ExternalCurrencyConversionResponseDto exchangeDto = await _currencyService.ConvertAsync(viewModel.TargetCurrencyId, viewModel.OriginCurrencyId, viewModel.TargetAmount);
            Transaction transactionToCreate = _transactionMapper.TransactionInternalCreateMap(internalCreateDto, exchangeDto, user.Id);
            Transaction createdTransaction = await _transactionsService.CreateTransactionAsync(transactionToCreate, user);

            return RedirectToAction(nameof(Details), Controller_Name, new { id = createdTransaction.Id, type = nameof(Transaction), isCreated = true });
        }

        [HttpGet]
        public async Task<IActionResult> CreatePaymentToUser(int id)
        {
            User senderUser = await this._usersService.GetByUsernameWithAccountsAsync(User.Identity.Name);
            User recepientUser = await _usersService.GetByIdWithAccountsAsync(id);
            this._usersService.ValidateUserStatus(senderUser);
            this._usersService.ValidateUserStatus(recepientUser);
            this._transactionsService.ThrowErrorIfSenderMatchesRecepient(senderUser, recepientUser);
            PaymentViewModel viewModel = new PaymentViewModel();
            viewModel.RecepientUserName = recepientUser.UserName;
            viewModel.HasSenderAccounts = this._usersService.CheckIfUserHasAccounts(senderUser);
            viewModel.HasRecepientAccounts =  this._usersService.CheckIfUserHasAccounts(recepientUser);
            viewModel.SenderUserId = senderUser.Id;
            viewModel.RecepientUserId = id;
            viewModel = _operationMapper.TransactionPaymentViewModelCreate(senderUser.Accounts, recepientUser.Accounts, viewModel);

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> ProceedPayment(PaymentViewModel viewModel)
        {
            User senderUser = await _usersService.GetByUserNameAsync(User.Identity.Name);
            User recepientUser = await _usersService.GetByIdWithAccountsAsync(viewModel.RecepientUserId);
            this._usersService.ValidateUserStatus(senderUser);
            this._usersService.ValidateUserStatus(recepientUser);
            Account senderAccount = await _accountService.GetByIdAsync(viewModel.SenderAccountId, senderUser);
            Account recepientAccount = await _accountService.GetByIdAsync(viewModel.RecepientAccountId, recepientUser);
            viewModel.Type = $"{Constants.Operations.Payment_To_User} {viewModel.RecepientUserName}";
            viewModel.From = $"{Constants.Labels.Account} {senderAccount.Name}";
            viewModel.OriginCurrencyId = senderAccount.CurrencyId;
            viewModel.CurrentOriginBalance = senderAccount.Balance;
            viewModel.To = $"{Constants.Labels.User} {viewModel.RecepientUserName} ({recepientAccount.CurrencyId})";
            viewModel.TargetCurrencyId = recepientAccount.CurrencyId;
            viewModel.CurrentExchangeRate = await SetExchangeRate(senderAccount.CurrencyId, recepientAccount.CurrencyId);
            viewModel.TargetAmount = Transaction.Default_Target_Amount;

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmPayment(PaymentViewModel viewModel)
        {
            User senderUser = await _usersService.GetByUserNameAsync(User.Identity.Name);
            User recepientUser = await _usersService.GetByIdWithAccountsAsync(viewModel.RecepientUserId);
            this._usersService.ValidateUserStatus(senderUser);
            this._usersService.ValidateUserStatus(recepientUser);
            _ = await _accountService.GetByIdAsync(viewModel.SenderAccountId, senderUser);
            _ = await _accountService.GetByIdAsync(viewModel.RecepientAccountId, recepientUser);
            viewModel.OriginAmount = viewModel.CalculatedOrigin;
            if (!this.ModelState.IsValid)
            {
                return View(viewModel);
            }

            viewModel.IsConfirmed = true;

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> CreatePaymentToUser(PaymentViewModel viewModel)
        {
            User senderUser = await _usersService.GetByUserNameAsync(User.Identity.Name);
            if (!this.ModelState.IsValid)
            {
                return RedirectToAction(nameof(ConfirmPayment), Controller_Name, viewModel);
            }

            Account recepientAccount = await _accountService.GetByIdAsync(viewModel.RecepientAccountId);
            TransactionExternalCreateDto createDto = this._transactionMapper.TransactionExternalCreateDtoMap(viewModel);
            Transaction transactionToCreate = await CreateTransactionBasedOnCurrencyExchange(viewModel, createDto, senderUser.Id, recepientAccount.CurrencyId);
            Transaction createdTransaction = await _transactionsService.CreateTransactionAsync(transactionToCreate, senderUser);

            return RedirectToAction(nameof(Details), Controller_Name, new { id = createdTransaction.Id, type = nameof(Transaction), isCreated = true });
        }

        [HttpGet]
        public async Task<IActionResult> TransferFromAccountCreate(int id, bool IsIncoming)
        {
            User user = await _usersService.GetByUsernameWithBankCardsAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            Account account = await _accountService.GetByIdAsync(id, user);
            ICollection<BankCard> bankCards = user.BankCards;
            TransferFromAccountViewModel viewModel = new TransferFromAccountViewModel();
            BankCard card = bankCards.Where(b => b.CurrencyId == account.CurrencyId).FirstOrDefault();
            if (card is null)
            {
                viewModel.IsIncoming = IsIncoming;
                viewModel.HasBankCard = false;
            }

            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmTransferFromAccount(TransferFromAccountViewModel viewModel)
        {

            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            this._usersService.ValidateUserStatus(user);
            if (!this.ModelState.IsValid)
            {
                return View(viewModel);
            }

            viewModel.IsConfirmed = true;

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> TransferFromAccountCreate(TransferFromAccountViewModel viewModel)
        {

            User user = await _usersService.GetByUserNameAsync(User.Identity.Name);
            if (!this.ModelState.IsValid)
            {
                return RedirectToAction(nameof(ConfirmTransferFromAccount), Controller_Name, viewModel);
            }

            Transfer transferToCreate = _transferMapper.TransferCreateMap(viewModel);
            Transfer createdTransfer = await _transfersService.CreateTransferAsync(transferToCreate, user);

            return this.RedirectToAction(nameof(Details), Controller_Name, new { id = createdTransfer.Id, type = nameof(Transfer), isCreated = true });
        }

        private async Task<decimal> SetExchangeRate(string senderAccountCurrency, string recepientAccountCurrency)
        {
            BaseCurrencyWithRatesDto rate = await this._currencyService.GetByIdWithRate(senderAccountCurrency, recepientAccountCurrency);
            return Math.Round(rate.Rates.First().Value, Constants.Formats.Decimal_Point_Precision);
        }

        private async Task<Transaction> CreateTransactionBasedOnCurrencyExchange(IExternalTransaction viewModel, TransactionExternalCreateDto createDto, int senderUserId, string recepientAccountCurrency)
        {
            Transaction transactionToCreate;
            if (viewModel.OriginCurrencyId == viewModel.TargetCurrencyId)
            {
                transactionToCreate = _transactionMapper.TransactionExternalNoExchangeCreateMap(createDto, viewModel.RecepientAccountId, senderUserId);
            }
            else
            {
                ExternalCurrencyConversionResponseDto exchangeDto = await _currencyService.ConvertAsync(recepientAccountCurrency, viewModel.OriginCurrencyId, viewModel.TargetAmount);
                transactionToCreate = _transactionMapper.TransactionExternalWithExchangeCreateMap(createDto, exchangeDto, senderUserId, viewModel.RecepientAccountId);
            }

            return transactionToCreate;
        }
    }
}
