﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Wallet.Core.Dtos.Account;
using Wallet.Core.Models;
using Wallet.Core.QueryParameters;
using Wallet.Service.Interfaces;
using Wallet.Web.Filters;
using Wallet.Web.Mappers;
using Wallet.Web.ViewModels.Accounts;

namespace Wallet.Web.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    public class AccountsController : Controller
    {
        private readonly IUsersService _usersService;
        private readonly IAccountService _accountService;
        private readonly AccountMapper _accountMapper;

        public AccountsController(IUsersService usersService, IAccountService accountService, AccountMapper accountMapper)
        {
            this._usersService = usersService;
            this._accountService = accountService;
            this._accountMapper = accountMapper;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Index([FromQuery] AccountQueryParameters filterParameters)
        {

            User user = await this._usersService.GetByUserNameAsync(User.Identity.Name);
            PaginatedList<Account> accounts = await this._accountService.SearchByAsync(filterParameters, user);
            accounts.AccountFilterParameters = filterParameters;

            return View(accounts);
        }

        [HttpPost]
        [Route("Accounts/Delete")]
        public async Task<IActionResult> Delete(int accountId)
        {
            User user = await this._usersService.GetByUsernameWithAccountsAsync(User.Identity.Name);
            await this._accountService.DeleteAsync(accountId, user);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Route("Accounts/Create")]
        public async Task<IActionResult> Create(CreateAccountViewModel viewModel)
        {
            User user = await this._usersService.GetByUsernameWithAccountsAsync(User.Identity.Name);
            Account accountToCreate = this._accountMapper.Map(viewModel);
            await this._accountService.CreateAsync(accountToCreate, user);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Route("Accounts/Update")]
        public async Task<IActionResult> Update(AccountUpdateDto viewModel)
        {
            User user = await this._usersService.GetByUsernameWithAccountsAsync(User.Identity.Name);
            await this._accountService.UpdateAsync(viewModel, user);

            return RedirectToAction(nameof(Index));
        }
    }
}
