﻿using Wallet.Core.Dtos;
using Wallet.Core.Models;

namespace Wallet.Service.Interfaces
{
    public interface IAuthenticationService
    {
        Task<string> CreateTokenAsync(User user);
        Task<string> GenerateRefreshTokenAsync(User user);
        Task<User> ValidateUserAsync(UserLoginDto loginDto);
        Task<TokenModel> RefreshTokenAsync(TokenModel tokenModel);
    }
}
