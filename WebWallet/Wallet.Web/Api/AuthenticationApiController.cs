﻿using Microsoft.AspNetCore.Mvc;
using Wallet.Core.Dtos;
using Wallet.Core.Models;
using Wallet.Service.Interfaces;

namespace Wallet.Web.Api
{
    [Route("api/authenticate")]
    [ApiController]
    public class AuthenticationApiController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthenticationApiController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> LoginAsync([FromBody] UserLoginDto loginUserDto)
        {
            User user = await _authenticationService.ValidateUserAsync(loginUserDto);
            string accessToken = await _authenticationService.CreateTokenAsync(user);
            string refreshToken = await this._authenticationService.GenerateRefreshTokenAsync(user);
            TokenModel tokenResult = new TokenModel
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken
            };

            return Ok(tokenResult);
        }

        [HttpPost]
        [Route("refresh-token")]
        public async Task<IActionResult> RefreshTokenAsync([FromBody] TokenModel token)
        {
            TokenModel refreshedToken = await this._authenticationService.RefreshTokenAsync(token);

            return Ok(refreshedToken);
        }
    }
}
