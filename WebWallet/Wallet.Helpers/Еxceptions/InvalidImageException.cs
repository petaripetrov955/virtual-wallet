﻿
namespace Wallet.Helpers.Еxceptions
{
	public class InvalidImageException : ApplicationException
	{
		public InvalidImageException(string message) : base(message)
		{

		}
	}
}
